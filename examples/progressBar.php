<?php

use FlyingAnvil\Libfa\Utils\Loop;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;

require_once __DIR__ . '/../vendor/autoload.php';

$options = [
    'Default'               => ProgressBarOptions::createDefault(),
    'PresetSimpleBottomTop' => ProgressBarOptions::createPresetSimpleBottomTop(),
    'PresetSimpleTopBottom' => ProgressBarOptions::createPresetSimpleTopBottom(),
    'PresetSimpleBlocks'    => ProgressBarOptions::createPresetSimpleBlocks(),
    'Custom'                => ProgressBarOptions::createCustom(
        '¡',
        '»',
        '›',
        ' ',
        52
    ),
];

$progressBar = new ProgressBar();
$loop        = new Loop();

foreach ($options as $name => $option) {
    echo $name, PHP_EOL;

    $progressBar->setOptions($option);

    $loop->setCallable(static function (Loop $loop, ProgressBar $progressBar) {
        $progressBar->writeProgress(
            $loop->getPassedIterations(),
            $loop->getLimit(),
        );
    }, $loop, $progressBar);

    $loop->setLimit(100);
    $loop->setInterval(.05);
    $loop->run();

    $progressBar->writeProgress($loop->getPassedIterations(), $loop->getLimit());

    echo PHP_EOL, 'Took ', $loop->getLoopDuration() * 1e-9, 's', PHP_EOL, PHP_EOL;
}

