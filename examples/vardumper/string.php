<?php

use FlyingAnvil\Libfa\Debug\VarDumper;

require_once __DIR__ . '/../../vendor/autoload.php';

VarDumper::dump('text');
VarDumper::dump(false);
VarDumper::dump(123);
VarDumper::dump(3.141592658972);
VarDumper::dump([
    1, 2, 3,
    'key' => 'value',
    'nested' => [
        1.1, 2.2, 3.3,
        'key' => 'value',
    ]
]);

VarDumper::dump(new DateTime());
VarDumper::dump(\FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput::create('much text'));

echo PHP_EOL;
