<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Image\Drawing\Canvas;
use FlyingAnvil\Libfa\Utils\Loop;

require_once __DIR__ . '/../vendor/autoload.php';

const RESOLUTION       = 64;
const TARGET_FRAMERATE = 20;

const SPEED_OFFSET  = 1;
const SPEED_SCALE_Y = .5;
const SPEED_SCALE_X = .01;

$offsetX = $offsetY = 0;
$scaleX  = .25;
$scaleY  = 10;
$lastFrame = 0;

readline_callback_handler_install('', static function() { });
$in = fopen('php://stdin', 'rb');
stream_set_blocking($in, false);

$loop = new Loop();
$loop->setCompensateRuntime(true);
$loop->setInterval(1 / TARGET_FRAMERATE);
$loop->setCallable(static function($in) use ($loop, &$lastFrame, &$offsetX, &$offsetY, &$scaleX, &$scaleY) {
    if (feof($in)) {
        $loop->stop();
        return;
    }

    $input = fgets($in, 4);
    if (strtolower($input) === 'q') {
        $loop->stop();
        echo PHP_EOL;
        return;
    }

    $inputMapping = [
        "\x1B\x5B\x41" => 'navUp',
        "\x1B\x5B\x42" => 'navDown',
        "\x1B\x5B\x44" => 'navLeft',
        "\x1B\x5B\x43" => 'navRight',

        "\x77" => 'W',
        "\x61" => 'A',
        "\x73" => 'S',
        "\x64" => 'D',
    ];

    $key = $inputMapping[$input] ?? null;

    switch ($key) {
        case 'navUp':
            $offsetY += SPEED_OFFSET;
            break;
        case 'navDown':
            $offsetY -= SPEED_OFFSET;
            break;
        case 'navLeft':
            $offsetX += SPEED_OFFSET;
            break;
        case 'navRight':
            $offsetX -= SPEED_OFFSET;
            break;

        case 'W':
            $scaleY += SPEED_SCALE_Y;
            break;
        case 'S':
            $scaleY -= SPEED_SCALE_Y;
            break;
        case 'A':
            $scaleX += SPEED_SCALE_X;
            break;
        case 'D':
            $scaleX -= SPEED_SCALE_X;
            break;
    }

    // Start Frame
    $frameStart = hrtime(true);
    $time       = $frameStart * 1e-9;

    $canvas = Canvas::create(RESOLUTION, RESOLUTION);
    $canvas->fillPattern(static function(int $x, int $y) use ($offsetX, $offsetY) {
        $u = $x - (RESOLUTION * .5) + (RESOLUTION * .5) + 1; # + $offsetX;
        $v = $y - (RESOLUTION * .5) + (RESOLUTION * .5) + 1; # + $offsetY;

        if ($u <= 0 || $v <= 0) {
            return null;
        }

        return Color::create(
            min(max(($u / RESOLUTION) * 255, 0), 255),
            min(max(($v / RESOLUTION) * 255, 0), 255),
            0,
        );
    });

    for ($x = 0; $x < RESOLUTION; $x++) {
        $newScaleY = $scaleY + sin($time) * 20 - 10;
        $y = (sin(($x + $offsetX + $time * 5) * $scaleX) * $newScaleY) + RESOLUTION * .5 - $offsetY;

        if ($y >= RESOLUTION || $y < 0) {
            continue;
        }

        $canvas->setPixel($x, $y, Color::create(172,28,182));
    }

    ob_start();
    $canvas->drawSmall();
    $drawn = ob_get_clean();

    system('clear');
//    echo "\e[H\e[J"; // Only scrolls old output out of the way
    echo $drawn;
    $frameEnd  = hrtime(true);

    echo PHP_EOL;
    $updateTime = ($frameEnd - $frameStart) * 1e-6;
    $frameTime  = ($frameEnd - $lastFrame) * 1e-6;
    $frameRate  = 1 / ($frameTime * 1e-3);
    echo sprintf(
        ' scaleX: %s | scaleY: %s | offsetX: %s | offsetY: %s',
        number_format($scaleX, 2),
        number_format($scaleY, 1),
        number_format($offsetX, 1),
        number_format($offsetY, 1),
    ), PHP_EOL;

    echo sprintf(
        ' updateTime: %sms | frameTime: %sms | frameRate: %s | targetFrameRate: %d',
        str_pad(number_format($updateTime, 1), 4, ' ', STR_PAD_LEFT),
        str_pad(number_format($frameRate, 1), 4, ' ', STR_PAD_LEFT),
        number_format($frameRate, 1),
        TARGET_FRAMERATE,
    ), PHP_EOL;

    echo PHP_EOL, ' q: quit | WASD: scale | arrowKeys: shift ', PHP_EOL;
    $lastFrame = $frameEnd;
}, $in);

$loop->run();
