<?php

declare(strict_types=1);

use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\Random\SM64RandomNumberGenerator;

require_once __DIR__ . '/../../vendor/autoload.php';

$rng = new SM64RandomNumberGenerator();

$initialValue = $rng->generate();
$cycles = 0;
while (true) {
    $cycles++;
    $currentValue = $rng->generate();

    if ($currentValue === $initialValue) {
        break;
    }
}

echo 'Cycles: ', $cycles, PHP_EOL, PHP_EOL;

$rng->reset();
$regularSequence = [];
for ($i = 0; $i < 0xFFFF; $i++) {
    $regularSequence[] = $rng->generate();
}

echo 'Regular Sequence: ', Range::create(
    min($regularSequence),
    max($regularSequence),
), PHP_EOL;

$rng->reset();
$sequence01 = [];
for ($i = 0; $i < 0xFFFF; $i++) {
    $sequence01[] = $rng->generate01();
}

echo '01 Sequence:      ', Range::create(
    min($sequence01),
    max($sequence01),
), PHP_EOL, PHP_EOL;

$rng->reset();
$odds = 0;
$even = 0;
for ($i = 0; $i < $cycles; $i++) {
    $value = $rng->generate();

    if ($value % 2 === 0) {
        $even++;
        continue;
    }

    $odds++;
}

echo 'Odds: ', $odds, PHP_EOL;
echo 'Even: ', $even, PHP_EOL;
