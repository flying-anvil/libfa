<?php

use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Random\DoomRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\PMD1DungeonRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\PMD1PrimaryRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\SM64RandomNumberGenerator;
use FlyingAnvil\Libfa\Utils\Formatter\TableFormatter;

require_once __DIR__ . '/../../vendor/autoload.php';

$rngs = [
    new SM64RandomNumberGenerator(),
    new SM64RandomNumberGenerator(false),
    new PMD1PrimaryRandomNumberGenerator(),
    new PMD1DungeonRandomNumberGenerator(),
    new DoomRandomNumberGenerator(),
];

$tableFormatter = new TableFormatter();
$tableFormatter->setPadding();

foreach ($rngs as $rng) {
    $name = substr($rng::class, strrpos($rng::class, '\\') + 1);
    $result = Table::createEmpty([$name, 'Value']);

    // TODO: Redo logic for cycle-detection, some algorithms produce the same number,
    // TODO: but in a different order, meaning it is not repeating yet
    $initialValue = $rng->generate();
    $cycles = 0;
    while (true) {
        $cycles++;
        $currentValue = $rng->generate();

        if (!$rng instanceof PMD1PrimaryRandomNumberGenerator && !$rng instanceof PMD1DungeonRandomNumberGenerator) {
            if ($currentValue === $initialValue) {
                break;
            }
        }

        if ($cycles >= 0xF_FFFF) {
            break;
        }
    }

    $result->addRows(['Cycles', $cycles]);
    $result->addRows(['', null]);

    $rng->reset();
    $regularSequenceMin = PHP_INT_MAX;
    $regularSequenceMax = PHP_INT_MIN;
    for ($i = 0; $i < $cycles; $i++) {
        $value = $rng->generate();

        $regularSequenceMin = min($regularSequenceMin, $value);
        $regularSequenceMax = max($regularSequenceMax, $value);
    }

    $result->addRows(['Regular Sequence', Range::create(
        $regularSequenceMin,
        $regularSequenceMax,
    )]);

    $rng->reset();
    $sequenceMinInt = PHP_INT_MAX;
    $sequenceMaxInt = PHP_INT_MIN;
    for ($i = 0; $i < $cycles; $i++) {
        $value = $rng->generateRangeInt(0x00, 0xFF);

        $sequenceMinInt = min($sequenceMinInt, $value);
        $sequenceMaxInt = max($sequenceMaxInt, $value);
    }

    $result->addRows(['Regular Sequence Int', Range::create(
        $sequenceMinInt,
        $sequenceMaxInt,
    )]);
    $result->addRows(['', null]);

    $rng->reset();
    $sequence01min = PHP_INT_MAX;
    $sequence01max = PHP_INT_MIN;
    for ($i = 0; $i < $cycles; $i++) {
        $value = $rng->generate01();

        $sequence01min = min($sequence01min, $value);
        $sequence01max = max($sequence01max, $value);
    }

    $result->addRows(['01 Sequence', Range::create(
        $sequence01min,
        $sequence01max,
    )]);
    $result->addRows(['', null]);

    $rng->reset();
    $odds = 0;
    $even = 0;
    for ($i = 0; $i < $cycles; $i++) {
        $value = $rng->generate();

        if ($value % 2 === 0) {
            $even++;
            continue;
        }

        $odds++;
    }

    $result->addRows(['Odds', $odds]);
    $result->addRows(['Even', $even]);

    echo $tableFormatter->generateFormattedRowsString($result), PHP_EOL;
}
