<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalette;
use FlyingAnvil\Libfa\Image\Drawing\IndexedCanvas;

require_once __DIR__ . '/../vendor/autoload.php';

$palette = IndexedColorPalette::createEmpty();
$palette->addColor(
    Color::create(0, 0, 0),
    Color::create(50, 50, 50),
    Color::create(100, 100, 100),
    Color::create(150, 150, 150),
    Color::create(200, 200, 200),
    Color::create(250, 250, 250),
);

$canvas = IndexedCanvas::create(3, 3);
$canvas->setPixel(1, 1, 2);
$canvas->fillRandomFromPalette($palette);

echo $canvas, PHP_EOL;
$canvas->draw($palette);
echo PHP_EOL;
$canvas->drawSmall($palette);
