<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\Libfa\Utils\Formatter\TableFormatter;

require_once __DIR__ . '/../vendor/autoload.php';

$default         = DecorableOutput::create('sample text');
$bold            = DecorableOutput::create('sample text')->setBold();
$faint           = DecorableOutput::create('sample text')->setFaint();
$italic          = DecorableOutput::create('sample text')->setItalic();
$underline       = DecorableOutput::create('sample text')->setUnderline();
$blinkSlow       = DecorableOutput::create('sample text')->setSlowBlink();
$blinkRapid      = DecorableOutput::create('sample text')->setRapidBlink();
$swapColors      = DecorableOutput::create('sample text')->setSwapColors();
$conceal         = DecorableOutput::create('sample text')->setConceal();
$crossedOut      = DecorableOutput::create('sample text')->setCrossedOut();
$foregroundColor = DecorableOutput::create('sample text')->setForegroundColor(Color::createRandom());
$backGroundColor = DecorableOutput::create('sample text')->setBackgroundColor(Color::createRandom());
$overline        = DecorableOutput::create('sample text')->setOverline();

$goneCrazy = DecorableOutput::create('sample text')
    ->setBold()
    ->setFaint()
    ->setItalic()
    ->setUnderline()
    ->setSlowBlink()
    ->setRapidBlink()
    ->setSwapColors()
    ->setCrossedOut()
    ->setForegroundColor(Color::createRandom())
    ->setBackgroundColor(Color::createRandom())
    ->setOverline()
;

$table = Table::createEmpty(['example', 'description']);
$table->addRows(
    [$default, 'default'],
    [$bold, 'bold'],
    [$faint, 'faint'],
    [$italic, 'italic'],
    [$underline, 'underline'],
    [$blinkSlow, 'slow blink'],
    [$blinkRapid, 'rapid blink'],
    [$swapColors, 'swapped colors'],
    [$conceal, 'conceal'],
    [$crossedOut, 'crossed out'],
    [$foregroundColor, 'foreground color'],
    [$backGroundColor, 'background color'],
    [$overline, 'overline'],
    ['', ''],
    [$goneCrazy, 'all of \'dem'],
);

$printer = new TableFormatter();
$printer->setPadding();
echo $printer->getTableAsString($table), PHP_EOL;
