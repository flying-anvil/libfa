<?php

use FlyingAnvil\Libfa\Benchmark\Benchmark;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\PMD1PrimaryRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\SM64RandomNumberGenerator;
use FlyingAnvil\Libfa\Random\NormalDistributedRandomNumberGenerator;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;

require_once __DIR__ . '/../../vendor/autoload.php';

$progressBar = new ProgressBar();
$progressBar->setOptions(ProgressBarOptions::createPresetSimpleBlocks());

$benchmark  = new Benchmark(5000, 1000, $progressBar);
$generators = [
    [
        'description' => 'default',
        'generator'   => new SM64RandomNumberGenerator(),
    ],
    [
        'description' => 'default',
        'generator'   => new BuiltinRandomNumberGenerator(),
    ],
    [
        'description' => 'SM64',
        'generator'   => new NormalDistributedRandomNumberGenerator(new SM64RandomNumberGenerator()),
    ],
    [
        'description' => 'builtin',
        'generator'   => new NormalDistributedRandomNumberGenerator(new BuiltinRandomNumberGenerator()),
    ],
    [
        'description' => 'default',
        'generator'   => new PMD1PrimaryRandomNumberGenerator(),
    ],
];

foreach ($generators as $definition) {
    $generator   = $definition['generator'];
    $className   = get_class($generator);
    $description = sprintf(
        '%s: %s',
        substr($className, strrpos($className, '\\') + 1),
        $definition['description'],
    );

    echo $benchmark->benchmark(static function () use ($generator) {
        return $generator->generate();
    }, $description)->buildTableReport(), PHP_EOL;
}
