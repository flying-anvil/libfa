<?php

use FlyingAnvil\Libfa\Benchmark\Benchmark;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;

require_once __DIR__ . '/../../vendor/autoload.php';

$progressBar = new ProgressBar();
$progressBar->setOptions(ProgressBarOptions::createPresetSimpleBlocks());

$benchmark = new Benchmark(10000, 1000, $progressBar);

echo $benchmark->benchmark(static function () {
    return sqrt(432857);
}, 'Squaring by sqrt')->buildTableReport(), PHP_EOL;

echo $benchmark->benchmark(static function () {
    /** @noinspection PowerOperatorCanBeUsedInspection */
    return pow(432857, .5);
}, 'Squaring by pow')->buildTableReport(), PHP_EOL;

echo $benchmark->benchmark(static function () {
    return 432857 ** .5;
}, 'Squaring by ** .5')->buildTableReport(), PHP_EOL;
