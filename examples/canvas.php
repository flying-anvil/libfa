<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\Image\Drawing\Canvas;

require_once __DIR__ . '/../vendor/autoload.php';

// small cube with dot in middle
$canvas = Canvas::create(3, 3);
$canvas->fill(Color::createRandom());
$canvas->setPixel(1, 1, Color::create(0, 0, 0));
$canvas->setPixel(1, 1, null);
$canvas->draw();
echo PHP_EOL;
$canvas->drawSmall();

echo PHP_EOL, PHP_EOL;

$canvas = Canvas::create(10, 10);
$canvas->fill(Color::createRandom());
$canvas->drawRect(1, 1, 8, 8, Color::createRandom());
$canvas->drawRect(4, 4, 5, 5, null, null, true);
$canvas->draw();
echo PHP_EOL;
$canvas->drawSmall();

echo PHP_EOL, PHP_EOL;

//$start = hrtime(true);
$canvas = Canvas::create(16, 16);
$canvas->fillRandom(
    Range::create(75, 100),
    Range::create(0, 0),
    Range::create(0, 0),
);
//$end = hrtime(true);

$canvas->draw();
echo PHP_EOL;
$canvas->drawSmall();

//echo 'generation: ' . (($end - $start) * 1e-9), PHP_EOL;
//echo 'drawing:    ' . ((hrtime(true) - $end) * 1e-9), PHP_EOL;

//echo json_encode($canvas, JSON_PRETTY_PRINT);
