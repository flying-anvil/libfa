<?php

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalettes;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\Libfa\Utils\Loop;
use FlyingAnvil\Libfa\Wrapper\File;

require_once __DIR__ . '/../vendor/autoload.php';

$writer  = new ConsoleWriter();
$loop    = new Loop();
$palette = IndexedColorPalettes::loadFromPalFile(File::load(__DIR__ . '/../samples/swamp.pal'), 16);

$loop->setCallable(static function(Loop $loop, ConsoleWriter $writer, IndexedColorPalettes $palette, int $paletteSize) {
    $passedIterations = $loop->getPassedIterations();
    $currentIteration = $loop->getCurrentIteration();
    $limit            = $loop->getLimit();

    $message = DecorableOutput::create(sprintf(
        'Iteration: %s',
        str_pad(
            $currentIteration,
            strlen((string)min($paletteSize, $limit > 0 ? $limit : $paletteSize)),
            ' ',
            STR_PAD_LEFT
        ),
    ))->setForegroundColor($palette->getColorByIndex($passedIterations))
        ->setBackgroundColor($palette->getColorByIndex($paletteSize - $currentIteration))
        ->setBold();

    $writer->writeln($message);

    // 5ms
    usleep(5000);

    if ($currentIteration === $paletteSize) {
        $loop->stop();
    }
}, $loop, $writer, $palette, $palette->countColors());

//$writer->startOutputBuffer();

$loop->setInterval(.1);
$loop->setCompensateRuntime();

$loop->run();
