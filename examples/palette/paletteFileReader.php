<?php

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalettes;
use FlyingAnvil\Libfa\Wrapper\File;

require_once __DIR__ . '/../../vendor/autoload.php';

if ($argc < 2) {
    echo 'Missing arguments! Expected <filePath> [<colorsPerRow>]', PHP_EOL;
    echo 'Try samples/swamp.pal', PHP_EOL;
    exit(1);
}

$palFile      = $argv[1];
$colorsPerRow = $argv[2] ?? 0;

if (!is_numeric($colorsPerRow)) {
    echo 'Argument [<colorsPerRow>] must be a number', PHP_EOL;
    exit(2);
}

$colorsPerRow = (int)$colorsPerRow;

$file = File::load($palFile);
if (!$file->isReadable()) {
    echo sprintf(
        'File "%s" is not readable. Does it exist?',
        $file->getFilePath(),
    ), PHP_EOL;
    exit(3);
}

$pal = IndexedColorPalettes::loadFromPalFile($file, $colorsPerRow);
//$pal = IndexedColorPalette::loadFromPalFile($file);
echo $pal, PHP_EOL;

//$target = File::load('/home/gramini/shared/libfa/swamp.copy.pal');
//$pal->saveAsFile($target);
