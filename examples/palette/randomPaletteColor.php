<?php

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\AbstractColorPalette;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\X11ColorPalette;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\Libfa\Utils\Loop;

require_once __DIR__ . '/../vendor/autoload.php';

echo 'Using X11ColorPalette', PHP_EOL, PHP_EOL;

$palette = X11ColorPalette::getPalette();
$writer  = new ConsoleWriter();
$loop    = new Loop();
$loop->setCallable(static function(Loop $loop, ConsoleWriter $writer, AbstractColorPalette $palette) {
    $currentIteration = $loop->getCurrentIteration();

    $iterationMessage = DecorableOutput::create('Iteration:');
    $iterationMessage->setForegroundColor($palette->getRandomColor());
    $iterationMessage->setUnderline();

    $countMessage = DecorableOutput::create(' ' . str_pad($currentIteration, 2, ' ', STR_PAD_LEFT) . ' ');
    $countMessage->setUnderline();

    $writer->write($iterationMessage);
    $writer->writeln($countMessage);
}, $loop, $writer, $palette);

$loop->setInterval(0.25);
$loop->setLimit(50);
$loop->setCompensateRuntime();
$loop->run();
