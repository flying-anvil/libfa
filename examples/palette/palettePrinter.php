<?php

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\HtmlColorPalette;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\X11ColorPalette;
use FlyingAnvil\Libfa\Debug\PalettePrinter;

require_once __DIR__ . '/../../vendor/autoload.php';

$palette = HtmlColorPalette::getPalette();
// $palette = X11ColorPalette::getPalette();

PalettePrinter::printPalette($palette);
echo PHP_EOL;
PalettePrinter::printPaletteForeground($palette);
echo PHP_EOL;
PalettePrinter::printPaletteBackground($palette);
echo PHP_EOL;
PalettePrinter::printPaletteTable($palette);
echo PHP_EOL;
PalettePrinter::printPaletteBlock($palette);
echo PHP_EOL;
PalettePrinter::printPaletteBlock($palette, 2);
echo PHP_EOL;
