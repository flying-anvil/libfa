<?php

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\GeneratedColorPalette;
use FlyingAnvil\Libfa\DataObject\Math\Vector3;
use FlyingAnvil\Libfa\Random\RandomNumberGenerator;
use FlyingAnvil\Libfa\Random\SM64RandomNumberGenerator;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;

require_once __DIR__ . '/../../vendor/autoload.php';

echo DecorableOutput::create('Constant')->setUnderline()->setBold(), PHP_EOL;
$count = 24;
printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(1.0, 1.0, 1.0),
        Vector3::create(0.0, .33, .66),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(1.0, 1.0, 1.0),
        Vector3::create(0.0, 0.1, 0.2),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(1.0, 1.0, 1.0),
        Vector3::create(0.3, 0.2, 0.2),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(1.0, 1.0, 0.5),
        Vector3::create(0.8, 0.9, 0.3),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(1.0, 0.7, 0.4),
        Vector3::create(0.0, .15, 0.2),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(0.5, 0.5, 0.5),
        Vector3::create(2.0, 1.0, 1.0),
        Vector3::create(0.5, 0.2, .25),
    ),
);

printPaletteInformation(
    GeneratedColorPalette::generate(
        $count,
        Vector3::create(0.8, 0.5, 0.4),
        Vector3::create(0.2, 0.4, 0.2),
        Vector3::create(2.0, 1.0, 1.0),
        Vector3::create(0.0, .25, .25),
    ),
);

echo PHP_EOL, DecorableOutput::create('Random')->setUnderline()->setBold(), PHP_EOL;

$rng = null;
//$rng = new SM64RandomNumberGenerator();
for ($i = 0; $i < 10; $i++) {
    $palette = GeneratedColorPalette::generateRandom($count, rng: $rng);
    printPaletteInformation($palette);
//    generateRandomPalette($count, $rng);
}

function printPaletteInformation(GeneratedColorPalette $palette): void
{
    echo sprintf(
        '%s  %s  %s  %s  ',
        $palette->getBias()->toString(2),
        $palette->getScale()->toString(2),
        $palette->getCosineOscillations()->toString(2),
        $palette->getCosinePhase()->toString(2),
    );

    echo $palette;
    echo PHP_EOL, PHP_EOL;
}

function generatePalette(int $count, Vector3 $a, Vector3 $b, Vector3 $c, Vector3 $d): void
{
    echo sprintf(
        '%s  %s  %s  %s  ',
        $a->toString(2), $b->toString(2), $c->toString(2), $d->toString(2),
    );

    $generatedPalette = GeneratedColorPalette::generate($count, $a, $b, $c, $d);
    echo $generatedPalette;
    echo PHP_EOL;
}

function generateRandomPalette(int $count, ?RandomNumberGenerator $rng = null): void
{
    $generatedPalette = GeneratedColorPalette::generateRandom($count, rng: $rng, clampSaturation: false);
    echo $generatedPalette;
    echo PHP_EOL, PHP_EOL;
}
