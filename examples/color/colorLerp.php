<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;

require_once __DIR__ . '/../../vendor/autoload.php';

$gradients = [
    [
        'from' => Color::create(255, 0,  0),
        'to'   => Color::create(  0, 0,255),
    ],
    [
        'from' => Color::create(50, 100,  50),
        'to'   => Color::create(  40, 10,20),
    ],
    [
        'from' => Color::createFromHexString('CC693F'),
        'to'   => Color::createFromHexString('69CC3F'),
    ],
    [
        'from' => Color::createRandom(),
        'to'   => Color::createRandom(),
    ],
    [
        'from' => Color::createRandom(),
        'to'   => Color::createRandom(),
    ],
];

$steps = 20;

foreach ($gradients as $gradient) {
    $from = $gradient['from'];
    $to   = $gradient['to'];

    for ($i = 0; $i < $steps; $i++) {
        $percentage = $i / ($steps - 1);
        $block = ColorBlockOutput::create(Color::lerp($from, $to, $percentage));

        echo $block;
    }

    echo PHP_EOL, PHP_EOL;
}
