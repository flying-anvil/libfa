<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Color\ColorRamp;
use FlyingAnvil\Libfa\DataObject\Color\ColorRampStep;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;

require_once __DIR__ . '/../../vendor/autoload.php';

$ramps = [
    [
        'name' => 'R > G > B',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 0, 0)),
            ColorRampStep::create(0.5, Color::create(0, 255, 0)),
            ColorRampStep::create(1.0, Color::create(0, 0, 255)),
        ),
    ],
    [
        'name' => 'R > G > B (G shifted left)',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 0, 0)),
            ColorRampStep::create(0.2, Color::create(0, 255, 0)),
            ColorRampStep::create(1.0, Color::create(0, 0, 255)),
        ),
    ],
    [
        'name' => 'R > G > B (G shifted right)',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 0, 0)),
            ColorRampStep::create(0.8, Color::create(0, 255, 0)),
            ColorRampStep::create(1.0, Color::create(0, 0, 255)),
        ),
    ],
    [
        'name' => 'R > G > B > R',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0/3, Color::create(255, 0, 0)),
            ColorRampStep::create(1/3, Color::create(0, 255, 0)),
            ColorRampStep::create(2/3, Color::create(0, 0, 255)),
            ColorRampStep::create(3/3, Color::create(255, 0, 0)),
        ),
    ],
    [
        'name' => 'Color Wheel',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0/7, Color::create(255,   0,   0)),
            ColorRampStep::create(1/7, Color::create(255, 255,   0)),
            ColorRampStep::create(2/7, Color::create(  0, 255,   0)),
            ColorRampStep::create(3/7, Color::create(  0, 255, 255)),
            ColorRampStep::create(4/7, Color::create(  0,   0, 255)),
            ColorRampStep::create(5/7, Color::create(  0,   0, 255)),
            ColorRampStep::create(6/7, Color::create(255,   0, 255)),
            ColorRampStep::create(7/7, Color::create(255,   0,   0)),
        ),
    ],
    [
        'name' => 'Black & White',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 255, 255)),
            ColorRampStep::create(0.5, Color::create(0, 0, 0)),
            ColorRampStep::create(1.0, Color::create(255, 255, 255)),
        ),
    ],
    [
        'name' => 'Black & White (flat plateau)',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 255, 255)),
            ColorRampStep::create(0.4, Color::create(0, 0, 0)),
            ColorRampStep::create(0.6, Color::create(0, 0, 0)),
            ColorRampStep::create(1.0, Color::create(255, 255, 255)),
        ),
    ],
    [
        'name' => 'Black & White (flat plateau outside)',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.0, Color::create(255, 255, 255)),
            ColorRampStep::create(0.2, Color::create(255, 255, 255)),
            ColorRampStep::create(0.4, Color::create(0, 0, 0)),
            ColorRampStep::create(0.6, Color::create(0, 0, 0)),
            ColorRampStep::create(0.8, Color::create(255, 255, 255)),
            ColorRampStep::create(1.0, Color::create(255, 255, 255)),
        ),
    ],
    [
        'name' => 'Out of bounds',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(-0.4, Color::create(255, 255, 255)),
            ColorRampStep::create(0.5, Color::create(0, 0, 0)),
            ColorRampStep::create(1.4, Color::create(255, 255, 255)),
        ),
    ],
    [
        'name' => 'Sawtooth',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.000, Color::create(255, 255, 255)),
            ColorRampStep::create(0.200, Color::create( 15,   0,  20)),
            ColorRampStep::create(0.201, Color::create(255, 255, 255)),
            ColorRampStep::create(0.400, Color::create( 15,   0,  20)),
            ColorRampStep::create(0.401, Color::create(255, 255, 255)),
            ColorRampStep::create(0.600, Color::create( 15,   0,  20)),
            ColorRampStep::create(0.601, Color::create(255, 255, 255)),
            ColorRampStep::create(0.800, Color::create( 15,   0,  20)),
            ColorRampStep::create(0.801, Color::create(255, 255, 255)),
            ColorRampStep::create(1.000, Color::create( 15,   0,  20)),
        ),
    ],
    [
        'name' => 'Missing bounds',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0.45, Color::create(250, 119,  66)),
            ColorRampStep::create(0.55, Color::createFromHexString('493682')),
        ),
    ],
    [
        'name' => 'Random',
        'ramp' => ColorRamp::create(
            ColorRampStep::create(0/3, Color::createRandom()),
            ColorRampStep::create(1/3, Color::createRandom()),
            ColorRampStep::create(2/3, Color::createRandom()),
            ColorRampStep::create(3/3, Color::createRandom()),
        ),
    ],
    [
        'name' => 'Even more random',
        'ramp' => (static function (): ColorRamp {
            $rng = new BuiltinRandomNumberGenerator();

            $countSteps = random_int(3, 10);
            $steps = [];

            for ($i = 0; $i < $countSteps; $i++) {
                $steps[] = ColorRampStep::create(
                    $rng->generate01(),
                    Color::createRandom(),
                );
            }

            return ColorRamp::create(...$steps);
        })(),
    ],
];

$steps = 50;

foreach ($ramps as $rampDefinition) {
    $ramp = $rampDefinition['ramp'];
    $name = $rampDefinition['name'];

    for ($i = 0; $i < $steps; $i++) {
        $percentage = $i / ($steps - 1);
        $block = ColorBlockOutput::create($ramp->evaluate($percentage));

        echo $block;
    }

    echo sprintf(
        ' %s',
        $name,
    );

    echo PHP_EOL, PHP_EOL;
}
