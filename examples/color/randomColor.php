<?php

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\Libfa\Utils\Loop;

require_once __DIR__ . '/../../vendor/autoload.php';

$writer = new ConsoleWriter();
$loop   = new Loop();
$loop->setCallable(static function(Loop $loop, ConsoleWriter $writer) {
    $currentIteration = $loop->getCurrentIteration();

    $iterationMessage = DecorableOutput::create('Iteration:');
    $iterationMessage->setForegroundColor(Color::createRandom());
    $iterationMessage->setBackgroundColor(Color::createRandom());
    $iterationMessage->setUnderline();

    $countMessage = DecorableOutput::create(' ' . str_pad($currentIteration, 6, ' ', STR_PAD_LEFT) . ' ');
    $countMessage->setUnderline();

    $writer->write($iterationMessage);
    $writer->writeln($countMessage);
}, $loop, $writer);

$loop->setInterval(0.25);
$loop->setLimit(50);
$loop->setCompensateRuntime();
$loop->run();
