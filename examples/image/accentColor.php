<?php

use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Image\Drawing\Canvas;
use FlyingAnvil\Libfa\Wrapper\Image;

require_once __DIR__ . '/../../vendor/autoload.php';

if ($argc < 2) {
    fwrite(STDERR, 'Must specify image file (URLs should also work, try https://gitlab.com/uploads/-/system/user/avatar/4258666/avatar.png)' . PHP_EOL);
    exit(1);
}

$file = $argv[1];

if (!file_exists($file) && !str_starts_with($file, 'http')) {
    fwrite(STDERR, sprintf(
        'File "%s" does not exist',
            $file,
    ) . PHP_EOL);
    exit(1);
}

$extension = FileExtension::createFromFilePath($file);

$image = Image::loadFromFile($file, $extension->toString());
$image->open();

$colors = $image->debugAccentColorAlgorithms();
$colors['Accent Color'] = $image->getAccentColor();

if ($image->getResolutionX() > 80) {
    $image->resize(80);
}

$image->toCanvas()->drawSmall();
echo PHP_EOL;

foreach ($colors as $method => $color) {
    echo $method, ' (', $color, ')', PHP_EOL;

    $enlargingCanvas = Canvas::create(4, 4);
    $enlargingCanvas->fill($color);
    $enlargingCanvas->drawSmall();

    echo PHP_EOL;
}
