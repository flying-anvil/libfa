# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## [Unreleased]

---

## [0.2.2] - 2021-03-30

### Fixed
- `FileExtension` wrong import of interface `StringValue`
- `File` fixed failsafe of `iterateCsvLines` (was misplaced inside `iterateLines`)

[0.2.2]: https://gitlab.com/flying-anvil/libfa/compare/0.2.1...0.2.2

---

## [0.2.1] - 2021-03-28

### Added
- Integrated `common-interfaces`

[0.2.1]: https://gitlab.com/flying-anvil/libfa/compare/0.2.0...0.2.1

---

## [0.2.0] - 2021-03-27

### Changed
- Upgrade to PHP 8.0
- Polished examples

### Added
- Image wrapper
- Image accent color

[0.2.0]: https://gitlab.com/flying-anvil/libfa/compare/0.1.0...0.2.0

---

## [0.1.0] - 2021-03-27

Last version compatible with PHP 7

[0.1.0]: https://gitlab.com/flying-anvil/libfa/-/tags/0.1.0
