# LibFa

Some common stuff of all different kinds.

It's still WIP, so this readme is very slim. Examples and screenshots ar on the todo.

You can find some examples in the `examples` directory, try them.

## Features

| Feature                                 | Description                                                                |
|-----------------------------------------|----------------------------------------------------------------------------|
| Benchmarking + Report                   | Benchmark simple functions and generate a detailed report about it         |
| DataObjects                             | Ready-to-Use data objects like Color, Point, Vector, Frequency or Amplitude (way more to follow) |
| Custom image functionality              | Canvas to draw on in PHP, Palettes, Tiles                                  |
| Math and math functions                 | Common things like lerp or clamp; Math Functions such as Sine or Linear-, ExponentialFunction, LinearRegression |
| Random Number Generation                | Different Implementations of RNG, even a standard distributed one          |
| Wrapper                                 | OOP File and Image, Loop, Sleeper                                          |
| Console Tools                           | Decorable output (color, bold, italic ans so on)                           |
| - Formatting Tools                      | CLI Key-Value formatting, Table formatting                                 |
| - Progress Bar                          | Configurable CLI progress bar (planned to be more extensible)              |
| Useful image tools (more to come)       | Get an accent color from an image                                          |
|                                         |                                                                            |
