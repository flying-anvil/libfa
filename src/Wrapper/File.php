<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Wrapper;

use FlyingAnvil\Libfa\DataObject\Exception\FileException;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use Generator;
use IteratorAggregate;
use Stringable;

class File implements IteratorAggregate
{
    /** @var resource */
    private $handle;
    private bool $isOpen = false;

    protected function __construct(
        protected string $filePath,
        protected string $wrapper = '',
    ) {}

    public static function wrap($stream): self
    {
        $instance = new self('none', '');
        $instance->handle = $stream;
        $instance->isOpen = true;

        return $instance;
    }

    public static function load(string $filePath): self
    {
        return new self($filePath);
    }

    public static function loadWrapper(string $wrapper, string $filePath): self
    {
        return new self($filePath, $wrapper);
    }

    public static function loadMemory(): self
    {
        return new self('php://memory');
    }

    public static function loadTemp(?int $maxMemory = null): self
    {
        $rawHandle = 'php://temp';

        if ($maxMemory !== null) {
            $rawHandle .= '/maxmemory:' . $maxMemory;
        }

        return new self($rawHandle);
    }

    public static function loadStdin(): self
    {
        return new self('php://stdin');
    }

    public static function loadStdout(): self
    {
        return new self('php://stdout');
    }

    public static function loadStderr(): self
    {
        return new self('php://stderr');
    }

    # region Meta Function Wrapper

    public function create(): bool
    {
        return touch($this->filePath);
    }

    public function exists(): bool
    {
        return file_exists($this->filePath);
    }

    public function isReadable(): bool
    {
        return is_readable($this->filePath);
    }

    public function isWritable(): bool
    {
        return is_writable($this->filePath);
    }

    public function getFileSize(): int
    {
        return filesize($this->filePath);
    }

    public function isOpen(): bool
    {
        return $this->isOpen;
    }

    public function isEof(): bool
    {
        $this->checkOpen();

        return feof($this->handle);
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getWrapper(): string
    {
        return $this->wrapper;
    }

    public function getExtension(): FileExtension
    {
        return FileExtension::createFromFilePath($this->filePath);
    }

    public function copy(string $target): void
    {
        copy($this->filePath, $target);
    }

    public function move(string $target): void
    {
        rename($this->filePath, $target);
    }

    public function delete(): void
    {
        unlink($this->filePath);
    }

    public function seek(int $offset, int $whence = SEEK_SET): bool
    {
        $this->checkOpen();
        return fseek($this->handle, $offset, $whence) === 0;
    }

    public function tell(): int
    {
        $this->checkOpen();
        return ftell($this->handle);
    }

    public function rewind(): bool
    {
        $this->checkOpen();
        return rewind($this->handle);
    }

    # endregion

    public function open(string $mode = 'rb', $useIncludePath = null, $context = null): bool
    {
        $params = [];
        if ($useIncludePath !== null) {
            $params[] = $useIncludePath;
        }

        if ($context !== null) {
            $params[] = $context;
        }

        $this->handle = fopen($this->wrapper . $this->filePath, $mode, ...$params);
        $this->isOpen = true;

        if ($this->handle === false) {
            // TODO: Error
            $this->handle = null;
            $this->isOpen = false;
        }

        return $this->isOpen;
    }

    public function close(): self
    {
        if ($this->isOpen) {
            fclose($this->handle);
            $this->isOpen = false;
        }

        return $this;
    }

    # region Read/Write

    /**
     * @param int|null $length
     * @return bool|string
     * @throws FileException
     */
    public function readLine(int $length = -1)
    {
        $this->checkOpen();

        $params = [];
        if ($length >= 0) {
            $params[] = $length;
        }

        return fgets($this->handle, ...$params);
    }

    public function read(int $length): string|bool
    {
        $this->checkOpen();

        return fread($this->handle, $length);
    }

    public function readAll(): string|bool
    {
        if (!$this->isOpen) {
            return $this->getFileContents();
        }

        return stream_get_contents($this->handle);
    }

    /**
     * Does only work for existing files.
     * To work with memory or temporary streams, use readAll
     *
     * @return string|bool
     */
    public function getFileContents(): string|bool
    {
        return file_get_contents($this->wrapper . $this->filePath);
    }

    public function readChar()
    {
        $this->checkOpen();

        return fgetc($this->handle);
    }

    public function readNullTerminatedSequence(int $maxLength = PHP_INT_MAX): string
    {
        $string = '';
        $length = 0;

        while ($length <= $maxLength && !$this->isEof()) {
            $newChar = $this->readChar();

            if ($newChar === "\0") {
                break;
            }

            $string .= $newChar;
        }

        return $string;
    }

    /**
     * @return array|false
     * @noinspection PhpDocSignatureInspection
     * @throws FileException
     */
    public function readCsvLine($length = 0, $delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        $this->checkOpen();
        return fgetcsv($this->handle, $length, $delimiter, $enclosure, $escape);
    }

    public function write(string $content, int $length = null)
    {
        $this->checkOpen();

        $params = [];
        if ($length !== null) {
            $params[] = $length;
        }

        return fwrite($this->handle, $content, ...$params);
    }

    public function writeAll(string|Stringable $contents): int|false
    {
        return file_put_contents($this->wrapper . $this->filePath, (string)$contents);
    }

    public function writeCsvLine(array $data, $delimiter = ',', $enclosure = '"', $escape = '\\'): void
    {
        $this->checkOpen();
        fputcsv($this->handle, $data, $delimiter, $enclosure, $escape);
    }

    /**
     * @param int $length
     * @return Generator<string>
     * @throws FileException
     */
    public function iterateLines(int $length = -1): Generator
    {
        while (($line = $this->readLine($length)) !== false) {
            yield $line;
        }
    }

    /**
     * @param int $chunkSize
     * @return Generator<string>
     * @throws FileException
     */
    public function iterateChunks(int $chunkSize = 4096): Generator
    {
        while (!$this->isEof()) {
            yield $this->read($chunkSize);
        }
    }

    /**
     * @param int|null $length
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return Generator<string>
     * @throws FileException
     */
    public function iterateCsvLines(int $length = 0, $delimiter = ',', $enclosure = '"', $escape = '\\'): Generator
    {
        while (($data = $this->readCsvLine($length, $delimiter, $enclosure, $escape)) !== false) {
            if (count($data) === 1 && $data[0] === null) {
                continue;
            }

            yield $data;
        }
    }

    public function iterateChars(): Generator
    {
        while (($char = $this->readChar()) !== false) {
            yield $char;
        }
    }

    # endregion

    # region Pack util
        # region Named

    public function writeSignedByte(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('c', $data));
    }

    public function writeUnsignedByte(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('C', $data));
    }

    public function writeUnsignedShortBigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('n', $data));
    }

    public function writeUnsignedShortLittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('v', $data));
    }

    public function writeUnsignedLongBigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('N', $data));
    }

    public function writeUnsignedLongLittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('V', $data));
    }

    public function writeUnsignedLongLongBigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('J', $data));
    }

    public function writeUnsignedLongLongLittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('P', $data));
    }

    # endregion

        # region Sized

    public function writeInt8(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('c', $data));
    }

    public function writeUInt8(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('C', $data));
    }

    public function writeUInt16BigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('n', $data));
    }

    public function writeUInt16LittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('v', $data));
    }

    public function writeUInt32BigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('N', $data));
    }

    public function writeUInt32LittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('V', $data));
    }

    public function writeUInt64BigEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('J', $data));
    }

    public function writeUInt64LittleEndian(int $data): void
    {
        $this->checkOpen();

        $this->write(pack('P', $data));
    }

        # endregion
    # endregion

    # region Unpack util
        # region Named

    public function readSignedByte(): int
    {
        $this->checkOpen();

        return unpack('c', $this->readChar())[1];
    }

    public function readUnsignedByte(): int
    {
        $this->checkOpen();

        return unpack('C', $this->readChar())[1];
    }

    public function readUnsignedShortBigEndian(): int
    {
        $this->checkOpen();

        return unpack('n', $this->read(2))[1];
    }

    public function readUnsignedShortLittleEndian(): int
    {
        $this->checkOpen();

        return unpack('v', $this->read(2))[1];
    }

    public function readUnsignedLongBigEndian(): int
    {
        $this->checkOpen();

        return unpack('N', $this->read(4))[1];
    }

    public function readUnsignedLongLittleEndian(): int
    {
        $this->checkOpen();

        return unpack('V', $this->read(4))[1];
    }

    public function readUnsignedLongLongBigEndian(): int
    {
        $this->checkOpen();

        return unpack('J', $this->read(8))[1];
    }

    public function readUnsignedLongLongLittleEndian(): int
    {
        $this->checkOpen();

        return unpack('P', $this->read(8))[1];
    }

        # endregion

        # region Sized

    public function readInt8(): int
    {
        $this->checkOpen();

        return unpack('c', $this->readChar())[1];
    }

    public function readUInt8(): int
    {
        $this->checkOpen();

        return unpack('C', $this->readChar())[1];
    }

    public function readUInt16BigEndian(): int
    {
        $this->checkOpen();

        return unpack('n', $this->read(2))[1];
    }

    public function readUInt16LittleEndian(): int
    {
        $this->checkOpen();

        return unpack('v', $this->read(2))[1];
    }

    public function readUInt32BigEndian(): int
    {
        $this->checkOpen();

        return unpack('N', $this->read(4))[1];
    }

    public function readUInt32LittleEndian(): int
    {
        $this->checkOpen();

        return unpack('V', $this->read(4))[1];
    }

    public function readUInt64BigEndian(): int
    {
        $this->checkOpen();

        return unpack('J', $this->read(8))[1];
    }

    public function readUInt64LittleEndian(): int
    {
        $this->checkOpen();

        return unpack('P', $this->read(8))[1];
    }

    # endregion
    # endregion

    public function getIterator(): Generator
    {
        return $this->iterateLines();
    }

    private function checkOpen(): void
    {
        if (!$this->isOpen) {
            throw new FileException('Cannot access a closed file, open first');
        }
    }

    public function __destruct()
    {
        $this->close();
    }
}
