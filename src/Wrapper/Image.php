<?php

/** @noinspection PhpComposerExtensionStubsInspection */

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Wrapper;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Exception\DependencyException;
use FlyingAnvil\Libfa\Image\Drawing\Canvas;

class Image
{
    /** @var resource */
    private $handle;
    private bool $isOpen = false;

    private int $resolutionX;
    private int $resolutionY;

    private function __construct(
        protected string $filePath = '',
        protected ?string $fileType = '',
    ) {
        if (!extension_loaded('gd')) {
            throw new DependencyException('Extension "gd" is missing');
        }
    }

    /**
     * @param string $filePath
     * @param string $type Determines which imagecreatefrom* function will be used
     * @return static
     * @throws DependencyException
     */
    public static function loadFromFile(string $filePath, ?string $type = null): self
    {
        return new self($filePath, $type);
    }

    public static function loadNew(int $resolutionX, int $resolutionY): self
    {
        return new self();
    }

    public function open(): void
    {
        if ($this->filePath) {
            $type = $this->fileType ?? pathinfo($this->filePath, PATHINFO_EXTENSION);

            $this->handle = match ($type) {
                'bmp' => imagecreatefrombmp($this->filePath),
                'gif' => imagecreatefromgif($this->filePath),
                'jpg' => imagecreatefromjpeg($this->filePath),
                'jpeg' => imagecreatefromjpeg($this->filePath),
                'png' => imagecreatefrompng($this->filePath),
                'webp' => imagecreatefromwebp($this->filePath),

                default => throw new \BadMethodCallException(sprintf(
                    'Type "%s" is not valid',
                    $type,
                )),
            };

            $this->resolutionX = imagesx($this->handle);
            $this->resolutionY = imagesy($this->handle);

            $this->isOpen = true;
            return;
        }

        $this->isOpen = true;
        $this->handle = imagecreatetruecolor($this->resolutionX, $this->resolutionY);
    }

    public function openIfClosed(): void
    {
        if (!$this->isOpen) {
            $this->open();
        }
    }

    public function write(string $format = 'png', ?string $filePath = null): bool
    {
        $target = $filePath ?: $this->filePath;

        return match ($format) {
            'bmp'  => imagebmp($this->handle, $target),
            'gif'  => imagegif($this->handle, $target),
            'jpg'  => imagejpeg($this->handle, $target),
            'jpeg' => imagejpeg($this->handle, $target),
            'png'  => imagepng($this->handle, $target),
            'webp' => imagewebp($this->handle, $target),

            default => throw new \BadMethodCallException(sprintf(
                'Type "%s" is not valid',
                $this->fileType,
            )),
        };
    }

    public function copy(): self
    {
        $copy = imagecreatetruecolor($this->resolutionX, $this->resolutionY);
        imagecopy($copy, $this->handle, 0, 0, 0, 0, $this->resolutionX, $this->resolutionY);

        $new = new self();
        $new->resolutionX = $this->resolutionX;
        $new->resolutionY = $this->resolutionY;
        $new->handle = $copy;
        $new->isOpen = true;

        return $new;
    }

    public function resize(int $newWidth, int $newHeight = -1, int $mode = IMG_BILINEAR_FIXED): void
    {
        $this->handle = imagescale($this->handle, $newWidth, $newHeight, $mode);

        $this->resolutionX = imagesx($this->handle);
        $this->resolutionY = imagesy($this->handle);
    }

    public function getPixel(int $posX, int $posY): Color
    {
        $channels = imagecolorsforindex($this->handle, imagecolorat($this->handle, $posX, $posY));
        return Color::create(
            $channels['red'],
            $channels['green'],
            $channels['blue'],
            $channels['alpha'],
        );
    }

    public function setPixel(int $posX, int $posY, Color $color): void
    {
        $index = imagecolorallocate(
            $this->handle,
            $color->getRed(),
            $color->getGreen(),
            $color->getBlue(),
        );

        imagesetpixel($this->handle, $posX, $posY, $index);
    }

    public function pixelIndexToCoordinates(int $index): array
    {
        $x = $index % $this->resolutionX;
        $y = (int)($index / $this->resolutionX);

        return [$x, $y];
    }

    public function pixelCoordinatesToIndex(int $posX, int $posY): int
    {
        return $posX + ($posY * $this->resolutionX);
    }

    public function isOpen(): bool
    {
        return $this->isOpen;
    }

    public function getAccentColor(): Color
    {
//        return $this->getHighestSaturationColor();
//        return $this->getHsvBasedColorAdditive(0, 1, 1);
        return $this->getHsvBasedColorMultiplicative(0, .666, .333);
    }

    /**
     * @param callable[] $custom
     * @return Color[]
     */
    public function debugAccentColorAlgorithms(array $custom = []): array
    {
        $result = [
            'Highest Saturation'                        => $this->getHighestSaturationColor(),
            'Highest Value'                             => $this->getHighestValueColor(),
            'Hsv Based Color Additive (0, 1, 1)'        => $this->getHsvBasedColorAdditive(0, 1, 1),
            'Hsv Based Color Additive (0, .5, 1)'       => $this->getHsvBasedColorAdditive(0, .5, 1),
            'Hsv Based Color Additive (0, 1, .5)'       => $this->getHsvBasedColorAdditive(0, 1, .5),
            'Hsv Based Color Multiplicative (0, 1, 1)'  => $this->getHsvBasedColorMultiplicative(0, 1, 1),
            'Hsv Based Color Multiplicative (0, .5, 1)' => $this->getHsvBasedColorMultiplicative(0, .5, 1),
            'Hsv Based Color Multiplicative (0, 1, .5)' => $this->getHsvBasedColorMultiplicative(0, 1, .5),
        ];

        foreach ($custom as $name => $algo) {
            $result[$name] = $this->pickColorByCriteria($algo);
        }

        return $result;
    }

    public function getHighestSaturationColor(): Color
    {
        return $this->pickColorByCriteria(function (Color $color) {
            return $color->toHsvColor()->getSaturation();
        });
    }

    public function getHighestValueColor(): Color
    {
        return $this->pickColorByCriteria(function (Color $color) {
            return $color->toHsvColor()->getValue();
        });
    }

    public function getHsvBasedColorAdditive(float $hueWeight, float $saturationWeight, float $valueWeight): Color
    {
        return $this->pickColorByCriteria(
            function (Color $color) use ($hueWeight, $saturationWeight, $valueWeight) {
                $hsv = $color->toHsvColor();

                return ($hsv->getHue() * $hueWeight)
                    + ($hsv->getSaturation() * $saturationWeight)
                    + ($hsv->getValue() * $valueWeight);
            }
        );
    }

    public function getHsvBasedColorMultiplicative(float $hueWeight, float $saturationWeight, float $valueWeight): Color
    {
        return $this->pickColorByCriteria(
            function (Color $color) use ($hueWeight, $saturationWeight, $valueWeight) {
                $hsv = $color->toHsvColor();

                return ($hsv->getHue() * $hueWeight)
                    + ($hsv->getSaturation() * $saturationWeight)
                    + ($hsv->getValue() * $valueWeight);
            }
        );
    }

    /**
     * @param callable<Color> $filter
     * @return Color
     */
    public function pickColorByCriteria(callable $filter): Color
    {
        $highestFactor = PHP_INT_MIN;
        $bestColor     = null;

        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = imagecolorat($this->handle, $x, $y);
                [
                    'red'    => $red,
                    'green'  => $green,
                    'blue'   => $blue,
                    'alpha'  => $alpha,
                ] = imagecolorsforindex($this->handle, $index);

                // Ignore full transparent
                if ($alpha === 127) {
                    continue;
                }

                $color = Color::create(
                    $red,
                    $green,
                    $blue,
                );

                $factor = $filter($color);
                if ($factor > $highestFactor) {
                    $highestFactor = $factor;
                    $bestColor     = $color;
                }
            }
        }

        return $bestColor;
    }

    public function toCanvas(): Canvas
    {
        $canvas = Canvas::create($this->resolutionX, $this->resolutionY);

        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = imagecolorat($this->handle, $x, $y);
                [
                    'red'   => $red,
                    'green' => $green,
                    'blue'  => $blue,
                    'alpha' => $alpha,
                ] = imagecolorsforindex($this->handle, $index);

                // Ignore full transparent
                if ($alpha === 127) {
                    continue;
                }

                $color = Color::create(
                    $red,
                    $green,
                    $blue,
                );

                $canvas->setPixel($x, $y, $color);
            }
        }

        return $canvas;
    }

    public function getResolutionX(): int
    {
        return $this->resolutionX;
    }

    public function getResolutionY(): int
    {
        return $this->resolutionY;
    }
}
