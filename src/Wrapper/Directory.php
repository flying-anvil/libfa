<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Wrapper;

use FlyingAnvil\Libfa\DataObject\Exception\DirectoryException;

class Directory
{
    protected function __construct(
        protected string $directoryPath,
    ) {}

    public static function load(string $directoryPath): self
    {
        return new self($directoryPath);
    }

    public static function loadExisting(string $directoryPath): self
    {
        if (!is_dir($directoryPath)) {
            throw new DirectoryException(sprintf(
                'Directory "%s" does not exist',
                $directoryPath,
            ));
        }

        return new self($directoryPath);
    }

    public static function loadNew(string $directoryPath): self
    {
        if (is_dir($directoryPath)) {
            throw new DirectoryException(sprintf(
                'Directory "%s" already exists',
                $directoryPath,
            ));
        }

        return new self($directoryPath);
    }

    public function exists(): bool
    {
        return is_dir($this->directoryPath);
    }

    public function create(int $permissions = 0775, bool $recursive = true): bool
    {
        if (!is_dir($this->directoryPath) && !mkdir($concurrentDirectory = $this->directoryPath, $permissions, $recursive) && !is_dir($concurrentDirectory)) {
            throw new DirectoryException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        return true;
    }

    /**
     * @return bool If the directory is present
     * @throws DirectoryException If the directory could not be created
     */
    public function createIfAbsent(int $permissions = 0775, bool $recursive = true): bool
    {
        if (!$this->exists()) {
            return $this->create($permissions, $recursive);
        }

        return true;
    }


    public function remove(): bool
    {
        return rmdir($this->directoryPath);
    }

    /**
     * @return bool If the directory is absent
     */
    public function removeIfPresent(): bool
    {
        if ($this->exists()) {
            return rmdir($this->directoryPath);
        }

        return true;
    }

    public function getDirectoryPath(): string
    {
        return $this->directoryPath;
    }
}
