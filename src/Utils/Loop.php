<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils;

use BadMethodCallException;
use FlyingAnvil\Libfa\Utils\Sleeper;
use OutOfRangeException;
use Throwable;

class Loop
{
    public const DEFAULT_INTERVAL = 5.0;

    private const NANOSECOND_TO_SECOND = 1e-9;

    /** @var callable */
    private $callable;

    /** @var callable */
    private $errorHandler;

    private array $params = [];
    private float $interval = self::DEFAULT_INTERVAL;
    private int $limit = 0;
    private bool $isRunning = false;
    private int $passedIterations = 0;
    private bool $compensateRuntime = false;
    private int $loopStart = 0;
    private int $loopEnd = 0;
    private int $lastIterationStart = 0;
    private int $lastIterationEnd = 0;

    public function __construct(protected ?Sleeper $sleeper = null)
    {
        $this->sleeper = $sleeper ?: new Sleeper();
    }

    public function setCallable(callable $callable, ...$params): void
    {
        $this->callable = $callable;
        $this->params   = $params;
    }

    public function setErrorHandler(callable $errorHandler): void
    {
        $this->errorHandler = $errorHandler;
    }

    public function setInterval(float $interval = self::DEFAULT_INTERVAL): void
    {
        if ($interval < 0) {
            throw new OutOfRangeException('Interval must be not less than 0');
        }

        $this->interval = $interval;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * false:
     * Fixed sleep duration after each interval.
     * Loop sleeps for # seconds.
     * <br><br>
     * true:
     * Dynamic sleep duration based on the last interval execution time.
     * Loop runs every # seconds.
     *
     * @param bool $compensateRuntime
     */
    public function setCompensateRuntime(bool $compensateRuntime = true): void
    {
        $this->compensateRuntime = $compensateRuntime;
    }

    public function run(): void
    {
        $this->reset();

        if (!$this->callable) {
            throw new BadMethodCallException('Callable must be set before starting the lop. Use "setCallable"');
        }

        $this->isRunning = true;
        $this->loopStart = hrtime(true);
        while ($this->isRunning) {

            try {
                $this->lastIterationStart = hrtime(true);
                ($this->callable)(...$this->params);
                $this->lastIterationEnd = hrtime(true);
            } catch (Throwable $throwable) {
                if (!$this->errorHandler) {
                    throw $throwable;
                }

                ($this->errorHandler)($throwable);
            }

            $this->passedIterations++;

            if ($this->limit > 0 && $this->passedIterations >= $this->limit) {
                $this->stop();
            }

            // In case the loop has been stopped
            if ($this->isRunning) {
                $this->sleeper->sleep($this->getSleepDuration());
            }
        }
    }

    public function stop(): void
    {
        $this->loopEnd   = hrtime(true);
        $this->isRunning = false;
    }

    public function getInterval(): float
    {
        return $this->interval;
    }

    public function isRunning(): bool
    {
        return $this->isRunning;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getPassedIterations(): int
    {
        return $this->passedIterations;
    }

    public function getCurrentIteration(): int
    {
        return $this->passedIterations + 1;
    }

    /**
     * @return int timestamp in nanoseconds
     */
    public function getLastIterationStart(): int
    {
        return $this->lastIterationStart;
    }

    /**
     * @return int timestamp in nanoseconds
     */
    public function getLastIterationEnd(): int
    {
        return $this->lastIterationEnd;
    }

    /**
     * @return int in nanoseconds
     */
    public function getLastIterationDuration(): int
    {
        return $this->lastIterationEnd - $this->lastIterationStart;
    }

    /**
     * @return int timestamp in nanoseconds
     */
    public function getLoopStart(): int
    {
        return $this->loopStart;
    }

    /**
     * @return int timestamp in nanoseconds
     */
    public function getLoopEnd(): int
    {
        return $this->loopEnd;
    }

    /**
     * @return int in nanoseconds
     */
    public function getLoopDuration(): int
    {
        if ($this->loopEnd > 0) {
            return $this->loopEnd - $this->loopStart;
        }

        return hrtime(true) - $this->loopStart;
    }

    private function reset(): void
    {
        $this->passedIterations   = 0;
        $this->lastIterationStart = 0;
        $this->lastIterationEnd   = 0;
        $this->loopStart          = 0;
        $this->loopEnd            = 0;
    }

    private function getSleepDuration(): float
    {
        if (!$this->compensateRuntime) {
            return $this->interval;
        }

        $duration        = $this->getLastIterationDuration();
        $durationSeconds = $duration * self::NANOSECOND_TO_SECOND;

        return $this->interval - $durationSeconds;
    }
}
