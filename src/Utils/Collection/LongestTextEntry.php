<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Collection;

use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutputElement;

class LongestTextEntry
{
    public function getLongestEntryLength(iterable $stringables): int
    {
        $max = 0;
        foreach ($stringables as $stringable) {
            $max = max($max, strlen((string)$stringable));
        }

        return $max;
    }

    public function getLongestEntryLengthMb(iterable $stringables): int
    {
        $max = 0;
        foreach ($stringables as $stringable) {
            $max = max($max, $this->getLengthMb($stringable));
        }

        return $max;
    }

    public function getShortestEntryLength(iterable $stringables): int
    {
        $min = 0;
        foreach ($stringables as $stringable) {
            $min = min($min, strlen((string)$stringable));
        }

        return $min;
    }

    public function getShortestEntryLengthMb(iterable $stringables): int
    {
        $min = 0;
        foreach ($stringables as $stringable) {
            $min = min($min, $this->getLengthMb($stringable));
        }

        return $min;
    }

    public function getLengthMb(mixed $stringable): int
    {
        if ($stringable instanceof DecorableOutputElement) {
            return $stringable->getPrintableLength();
        }

        // Remove ANSI escape sequences    \e  [38;2;123  m
        $string = preg_replace('#\\x1b\[[^A-Za-z]*[A-Za-z]#', '', (string)$stringable);

        // Remove non-printable characters (like NULL-bytes, BELL and so on)
        $string = mb_ereg_replace('[^[:print:]]', '', $string);

        return mb_strlen($string);
    }
}
