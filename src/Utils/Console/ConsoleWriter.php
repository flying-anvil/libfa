<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console;

use FlyingAnvil\Libfa\Conversion\StringValue;
use Stringable;
use FlyingAnvil\Libfa\DataObject\Collection\GenericCollection;
use FlyingAnvil\Libfa\DataObject\Color\Color;

class ConsoleWriter
{
    private const WRITABLE_TYPES = [
        GenericCollection::TYPE_BOOL,
        GenericCollection::TYPE_INT,
        GenericCollection::TYPE_FLOAT,
        GenericCollection::TYPE_DOUBLE,
        GenericCollection::TYPE_STRING,
        GenericCollection::TYPE_OBJECT,
        GenericCollection::TYPE_NULL,
    ];

    // TODO: Use default values
    /** @var Color */
    private $color;

    /** @var Color */
    private $backgroundColor;

    /** @var bool */
    private $useBuffer = false;

    /** @var string */
    private $buffer = '';

    /**
     * @param resource $outputStream
     */
    public function __construct(
        private $outputStream = STDOUT,
    ) {}

    /**
     * Prints $output.
     */
    public function write(string|int|float|Stringable $output): void
    {
        if ($this->useBuffer) {
            $this->buffer .= (string)$output;

            return;
        }

        fwrite($this->outputStream, (string)$output);
    }

    /**
     * Prints $output and adds a new line at the very end.
     */
    public function writeln(string|int|float|Stringable $output): void
    {
        $this->write($output . PHP_EOL);
    }

    public function setColor(Color $color): self
    {
        $this->color = $color;
        return $this;
    }

    public function setBackgroundColor(Color $color): self
    {
        $this->backgroundColor = $color;
        return $this;
    }

    public function isWritable(mixed $value): bool
    {
        $type = gettype($value);

        if (!in_array($type, self::WRITABLE_TYPES, true)) {
            return false;
        }

        return ($type === GenericCollection::TYPE_OBJECT
            && ($value instanceof Stringable || method_exists($value, '__toString')));
    }

    public function startOutputBuffer(): void
    {
        $this->useBuffer = true;
    }

    public function endOutputBuffer(): void
    {
        $this->useBuffer = false;
    }

    public function getOutputBuffer(): string
    {
        return $this->buffer;
    }

    public function flushOutputBuffer(): void
    {
        $this->buffer = '';
    }

    # region Ansi
    public function ansiClearLineRight(): void
    {
        $this->write("\e[0K");
    }

    public function ansiClearLineLeft(): void
    {
        $this->write("\e[1K");
    }

    public function ansiClearLine(): void
    {
        $this->write("\e9001D\e[1K");
    }
    # endregion
}
