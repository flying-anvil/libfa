<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console\OutputElement;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\Color\Color;

class SmallColorBlockOutput implements DecorableOutputElement, StringValue
{
    protected function __construct(
        protected ?Color $topColor,
        protected ?Color $bottomColor,
    ) {}

    public static function create(?Color $topColor, ?Color $bottomColor): self
    {
        return new self($topColor, $bottomColor);
    }

    public function getPrintableLength(): int
    {
        return 1;
    }

    public function toString(): string
    {
        if ($this->bottomColor === null && $this->topColor === null) {
            return ' ';
        }

        $topColor    = '';
        $bottomColor = '';
        $keyForegroundColor = 4;

        if ($this->bottomColor === null) {
            // Swap foreground and background colors on the cli
            $bottomColor = "\e[7m";
            $keyForegroundColor = 3;
        }

        if ($this->topColor !== null) {
            $topColor = sprintf(
                "\e[%d8;2;%d;%d;%dm",
                $keyForegroundColor, // respect swapped colors
                $this->topColor->getRed(),
                $this->topColor->getGreen(),
                $this->topColor->getBlue(),
            );
        }

        if ($this->bottomColor !== null) {
            $bottomColor = sprintf(
                "\e[38;2;%d;%d;%dm",
                $this->bottomColor->getRed(),
                $this->bottomColor->getGreen(),
                $this->bottomColor->getBlue(),
            );
        }

        // TODO: Make reset (\e[0m) configurable
        return sprintf(
            "%s%s▄\e[0m",
            $topColor,
            $bottomColor,
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
