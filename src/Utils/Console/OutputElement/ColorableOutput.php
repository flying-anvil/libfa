<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console\OutputElement;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\Color\Color;

class ColorableOutput implements DecorableOutputElement, StringValue
{
    private const RESET = "\e[0m";
    private const PATTERN_FOREGROUND = "\033[38;2;%d;%d;%dm";
    private const PATTERN_BACKGRUOND = "\033[48;2;%d;%d;%dm";

    protected function __construct(
        protected string $message,
        protected ?Color $foregroundColor = null,
        protected ?Color $backgroundColor = null)
    {}

    public static function create(string $message, ?Color $foregroundColor = null, ?Color $backgroundColor = null): self
    {
        return new self($message, $foregroundColor, $backgroundColor);
    }

    public function getPrintableLength(): int
    {
        return mb_strlen($this->message);
    }

    public function toString(): string
    {
        $output = '';
        $reset  = false;

        if ($this->foregroundColor !== null) {
            $output .= sprintf(
                self::PATTERN_FOREGROUND,
                $this->foregroundColor->getRed(),
                $this->foregroundColor->getGreen(),
                $this->foregroundColor->getBlue(),
            );

            $reset = true;
        }

        if ($this->backgroundColor !== null) {
            $output .= sprintf(
                self::PATTERN_BACKGRUOND,
                $this->backgroundColor->getRed(),
                $this->backgroundColor->getGreen(),
                $this->backgroundColor->getBlue(),
            );

            $reset = true;
        }

        $output .= $this->message;

        if ($reset) {
            $output .= self::RESET;
        }

        return $output;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
