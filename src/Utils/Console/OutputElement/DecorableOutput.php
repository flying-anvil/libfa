<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console\OutputElement;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use RangeException;

/**
 * @see https://stackoverflow.com/a/33206814
 */
class DecorableOutput implements DecorableOutputElement, StringValue
{
    private const RESET = "\e[0m";
    private const SEQUENCE_RESET = 0;
    private const SEQUENCE_BOLD = 1;
    private const SEQUENCE_FAINT = 2;
    private const SEQUENCE_ITALIC = 3;
    private const SEQUENCE_UNDERLINE = 4;
    private const SEQUENCE_BLINK_SLOW = 5;
    private const SEQUENCE_BLINK_RAPID = 6;
    private const SEQUENCE_SWAP_COLORS = 7;
    private const SEQUENCE_CONCEAL = 8;
    private const SEQUENCE_CROSSED_OUT = 9;
    private const PREFIX_FONT = 1;
    private const PREFIX_FOREGROUND_COLOR = '38;2;';
    private const PREFIX_BACKGROUND_COLOR = '48;2;';
    private const SEQUENCE_OVERLINE = 53;

    private const RESET_BOLD = 22;
    private const RESET_FAINT = 22;
    private const RESET_ITALIC = 23;
    private const RESET_UNDERLINE = 24;
    private const RESET_BLINK_SLOW = 25;
    private const RESET_BLINK_RAPID = 25;
    private const RESET_SWAP_COLORS = 27;
    private const RESET_CONCEAL = 28;
    private const RESET_CROSSED_OUT = 29;
    private const RESET_FOREGROUND_COLOR = 39;
    private const RESET_BACKGROUND_COLOR = 49;
    private const RESET_OVERLINE = 55;

    private bool $resetBefore = true;
    private bool $resetAfter  = true;
    private array $sequence = [];
    private array $reset = [];

    private ?Color $foregroundColor;
    private ?Color $backgroundColor;

    private function __construct(protected string $message) {}

    public static function create(string $message): self
    {
        return new self($message);
    }

    public function getPrintableLength(): int
    {
        return mb_strlen($this->message);
    }

    public function getUndecoratedText(): string
    {
        return $this->message;
    }

    public function toString(): string
    {
        $escape = $this->resetBefore ? self::SEQUENCE_RESET : '';
        $reset  = '';

        if ($this->sequence !== [] || $escape !== '') {
            $escape = "\e[" . implode(';', array_filter($this->sequence)) . 'm';
        }

        if ($this->resetAfter && $this->reset !== []) {
            $reset = "\e[" . implode(';', array_filter($this->reset)) . 'm';
        }

        return $escape . $this->message . $reset;
    }

    public static function getResetString(): string
    {
        return self::RESET;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function setResetBefore(bool $resetBefore = true): self
    {
        $this->resetBefore = $resetBefore;
        return $this;
    }

    public function setResetAfter(bool $resetAfter = true): self
    {
        $this->resetAfter = $resetAfter;
        return $this;
    }


    public function setReset(bool $resetBefore = true, bool $resetAfter = true): self
    {
        $this->resetBefore = $resetBefore;
        $this->resetAfter  = $resetAfter;
        return $this;
    }

    public function setBold(bool $bold = true): self
    {
        $this->sequence['bold'] = $bold ? self::SEQUENCE_BOLD : false;
        $this->reset['bold'] = $bold ? self::RESET_BOLD : false;
        return $this;
    }

    public function setFaint(bool $faint = true): self
    {
        $this->sequence['faint'] = $faint ? self::SEQUENCE_FAINT : false;
        $this->reset['faint'] = $faint ? self::RESET_FAINT : false;
        return $this;
    }

    public function setItalic(bool $italic = true): DecorableOutput
    {
        $this->sequence['italic'] = $italic ? self::SEQUENCE_ITALIC : false;
        $this->reset['italic'] = $italic ? self::RESET_ITALIC : false;
        return $this;
    }

    public function setUnderline(bool $underline = true): self
    {
        $this->sequence['underline'] = $underline ? self::SEQUENCE_UNDERLINE : false;
        $this->reset['underline'] = $underline ? self::RESET_UNDERLINE : false;
        return $this;
    }

    public function setSlowBlink(bool $slowBlink = true): self
    {
        $this->sequence['slowBlink'] = $slowBlink ? self::SEQUENCE_BLINK_SLOW : false;
        $this->reset['slowBlink'] = $slowBlink ? self::RESET_BLINK_SLOW : false;
        return $this;
    }

    public function setRapidBlink(bool $rapidBlink = true): self
    {
        $this->sequence['rapidBlink'] = $rapidBlink ? self::SEQUENCE_BLINK_RAPID : false;
        $this->reset['rapidBlink'] = $rapidBlink ? self::RESET_BLINK_RAPID : false;
        return $this;
    }

    public function setSwapColors(bool $swapColors = true): self
    {
        $this->sequence['swapColors'] = $swapColors ? self::SEQUENCE_SWAP_COLORS : false;
        $this->reset['swapColors'] = $swapColors ? self::RESET_SWAP_COLORS : false;
        return $this;
    }

    public function setConceal(bool $conceal = true): self
    {
        $this->sequence['conceal'] = $conceal ? self::SEQUENCE_CONCEAL : false;
        $this->reset['conceal'] = $conceal ? self::RESET_CONCEAL : false;
        return $this;
    }

    public function setCrossedOut(bool $crossedOut = true): self
    {
        $this->sequence['crossedOut'] = $crossedOut ? self::SEQUENCE_CROSSED_OUT : false;
        $this->reset['crossedOut'] = $crossedOut ? self::RESET_CROSSED_OUT : false;
        return $this;
    }

    public function setAlternateFont(int $alternateFont): self
    {
        if ($alternateFont < 0 || $alternateFont > 10) {
            throw new RangeException('Must be in range of [0,10], but is ' . $alternateFont);
        }

        $this->sequence['font'] = $alternateFont > 0 ? (self::PREFIX_FONT . $alternateFont) : false;
        $this->reset['font'] = $alternateFont > 0 ? (self::PREFIX_FONT . 0) : false;
        return $this;
    }

    public function setForegroundColor(?Color $foregroundColor): self
    {
        $this->foregroundColor     = $foregroundColor;
        $this->sequence['fgColor'] = $foregroundColor !== null
            ? sprintf(
                '%s%d;%d;%d',
                self::PREFIX_FOREGROUND_COLOR,
                $foregroundColor->getRed(),
                $foregroundColor->getGreen(),
                $foregroundColor->getBlue(),
            )
            : false;
        $this->reset['fgColor'] = $foregroundColor !== null ? self::RESET_FOREGROUND_COLOR : false;

        return $this;
    }

    public function setBackgroundColor(Color $backgroundColor): self
    {
        $this->backgroundColor     = $backgroundColor;
        $this->sequence['bgColor'] = $backgroundColor !== null
            ? sprintf(
                '%s%d;%d;%d',
                self::PREFIX_BACKGROUND_COLOR,
                $backgroundColor->getRed(),
                $backgroundColor->getGreen(),
                $backgroundColor->getBlue(),
            )
            : false;
        $this->reset['bgColor'] = $backgroundColor !== null ? self::RESET_BACKGROUND_COLOR : false;

        return $this;
    }

    public function setOverline(bool $overline = true): self
    {
        $this->sequence['overline'] = $overline ? self::SEQUENCE_OVERLINE : false;
        $this->reset['overline'] = $overline ? self::RESET_OVERLINE : false;
        return $this;
    }

    public function setCustomEscape(int ...$sequences): void
    {
        $this->sequence['custom'] = implode(';', $sequences);
    }
}
