<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console\OutputElement;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\Color\Color;

class ColorBlockOutput implements DecorableOutputElement, StringValue
{
    protected function __construct(protected Color $color)
    {
        $this->color = $color;
    }

    public static function create(Color $color): self
    {
        return new self($color);
    }

    public function getPrintableLength(): int
    {
        return 2;
    }

    public function toString(): string
    {
        return sprintf(
            "\033[48;2;%d;%d;%dm  \033[0m",
            $this->color->getRed(),
            $this->color->getGreen(),
            $this->color->getBlue(),
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
