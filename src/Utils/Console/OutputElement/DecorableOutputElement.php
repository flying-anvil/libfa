<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Console\OutputElement;

use Stringable;

interface DecorableOutputElement extends Stringable
{
    /**
     * @return int Length of the printable character of the output. This excludes things like color codes
     */
    public function getPrintableLength(): int;
}
