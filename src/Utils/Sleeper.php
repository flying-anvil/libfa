<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils;

class Sleeper
{
    private const SECONDS_TO_MICRO = 1e6;

    public function sleep(float $duration): void
    {
        if ($duration < 0) {
            return;
        }

        usleep((int)($duration * self::SECONDS_TO_MICRO));
    }

    public function sleepSeconds(int $duration): int
    {
        if ($duration < 0) {
            return 0;
        }

        return sleep($duration * self::SECONDS_TO_MICRO);
    }
}
