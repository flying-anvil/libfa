<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Progress;

class DualProgressBar
{
    private function __construct(
        private int $length,
    ) {}

    public function printProgress(int $topCurrent, int $topMax, int $bottomCurrent, int $bottomMax): void
    {
        $topProgress = $topCurrent / $topMax;
        $bottomProgress = $bottomCurrent / $bottomMax;

        echo "\r\e[4;53m"; // Underline + Overline
        for ($i = 1; $i <= $this->length; $i++) {
            $fraction = $i / $this->length;
            $topBlock = $topProgress >= $fraction;
            $bottomBlock = $bottomProgress >= $fraction;

            if ($topBlock && $bottomBlock) {
                echo '█';
                continue;
            }

            if ($topBlock) {
                echo '▀';
                continue;
            }

            if ($bottomBlock) {
                echo '▄';
                continue;
            }

            echo ' ';
        }

        echo "\e[0m";

        // Here's a tetromino for motivation: ▄█▄
    }
}
