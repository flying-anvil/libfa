<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Progress\Options;

use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;

class ProgressBarOptions
{
    private int $width     = 102;
    private string $delimiter = '|';
    private string $caret     = '>';
    private string $fill      = '=';
    private string $empty     = ' ';

    private function __construct()
    {
    }

    public static function createDefault(): self
    {
        return new self();
    }

    public static function createCustom(
        string $delimiter,
        string $caret,
        string $fill,
        string $empty,
        int $width = 102,
    ): self {
        $instance = new self();
        $instance->width     = $width;
        $instance->delimiter = $delimiter;
        $instance->caret     = $caret;
        $instance->fill      = $fill;
        $instance->empty     = $empty;
        return $instance;
    }

    public static function createPresetSimpleBottomTop(): self
    {
        $instance = new self();
        $instance->width     = 102;
        $instance->delimiter = '|';
        $instance->caret     = '\\';
        $instance->fill      = (string)DecorableOutput::create(' ')->setOverline();
        $instance->empty     = '_';
        return $instance;
    }

    public static function createPresetSimpleTopBottom(): self
    {
        $instance = new self();
        $instance->width     = 102;
        $instance->delimiter = '|';
        $instance->caret     = '/';
        $instance->fill      = '_';
        $instance->empty     = (string)DecorableOutput::create(' ')->setOverline();
        return $instance;
    }

    public static function createPresetSimpleBlocks(): self
    {
        $instance = new self();
        $instance->width     = 102;
        $instance->delimiter = '';
        $instance->caret     = '▒';
        $instance->fill      = '█';
        $instance->empty     = '░';
        return $instance;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    public function getCaret(): string
    {
        return $this->caret;
    }

    public function getFill(): string
    {
        return $this->fill;
    }

    public function getEmpty(): string
    {
        return $this->empty;
    }
}
