<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Progress;

use FlyingAnvil\Libfa\Utils\Collection\LongestTextEntry;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;

class ProgressBar
{
    private ProgressBarOptions $options;

    public function __construct(
        protected ?ConsoleWriter $writer = null,
        protected ?LongestTextEntry $longestFinder = null)
    {
        $this->options = ProgressBarOptions::createDefault();

        $this->writer        = $writer ?: new ConsoleWriter();
        $this->longestFinder = $longestFinder ?: new LongestTextEntry();
    }

    public function setOptions(ProgressBarOptions $options): self
    {
        $this->options = $options;
        return $this;
    }

    public function writeProgress(float $current, float $maximum): void
    {
        $relative = $current / $maximum;
        $maxWidth = $this->options->getWidth();

        $delimiter = $this->options->getDelimiter();
        $caret     = $this->options->getCaret();
        $fill      = $this->options->getFill();
        $empty     = $this->options->getEmpty();

        $delimiterLength = $this->longestFinder->getLengthMb($delimiter) * 2;
        $fillLength      = $this->longestFinder->getLengthMb($fill);
        $emptyLength     = $this->longestFinder->getLengthMb($empty);

        $maxFillLength = $maxWidth - $delimiterLength;
        $fillSpace     = (int)min($relative * $maxFillLength, $maxFillLength);
        $emptySpace    = (int)($maxWidth - $fillSpace - $delimiterLength);

        $filled    = str_repeat($fill, $fillSpace);
        $emptied   = $emptySpace === 0 ? '' : str_repeat($empty, $emptySpace);

        if ($fillSpace < $maxFillLength && $fillSpace > 0) {
            $filled = str_repeat($fill, $fillSpace - 1) . $caret;
        }

        $line = "\r" . $delimiter . $filled . $emptied . $delimiter;

        $this->writer->write($line);
    }
}
