<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Formatter;

use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Utils\Collection\LongestTextEntry;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use Generator;
use JetBrains\PhpStorm\Deprecated;

class TableFormatter
{
    private string $paddingLeft = '';
    private string $paddingRight = '';

    public function __construct(
        protected ?LongestTextEntry $longestFinder = null,
        protected ?ConsoleWriter $writer = null,
    ) {
        $this->longestFinder = $longestFinder ?? new LongestTextEntry();
        $this->writer        = $writer ?? new ConsoleWriter();
    }

    public function setPadding(string $left = ' ', string $right = ' '): void
    {
        $this->paddingLeft  = $left;
        $this->paddingRight = $right;
    }

    public function getTableAsString(Table $table): string
    {
        return implode(PHP_EOL, $this->getTableAsStringArray($table));
    }

    public function getTableAsStringArray(Table $table): array
    {
        return iterator_to_array($this->generateFormattedRows($table));
    }

    #[Deprecated('Directly writing is discouraged')]
    public function printTable(Table $table): void
    {
        $this->writer->writeln($this->getTableAsString($table));
    }

    public function generateFormattedRows(Table $table): Generator
    {
        $longests  = $this->getLongests($table);
        $top       = $this->generateSeparator($longests, '╭', '┬', '╮');
        $separator = $this->generateSeparator($longests, '├', '┼', '┤');
        $bottom    = $this->generateSeparator($longests, '╰', '┴', '╯');

        $formattedHeaders = $this->generateHeaders($longests, '│', '│', '│');

        yield $top;
        yield $formattedHeaders;
        yield $separator;

        foreach ($table as $row) {
            if (!is_iterable($row)) {
                // TODO: Maybe Exception
                continue;
            }

            yield $this->generateRow(array_values($longests), $row, '│', '│', '│');
        }

        yield $bottom;
    }

    public function generateFormattedRowsString(Table $table): string
    {
        return implode(PHP_EOL, iterator_to_array($this->generateFormattedRows($table))) . PHP_EOL;
    }

    private function getLongests(Table $table): array
    {
        $longests = [];

        foreach ($table->getColumns() as $index => $column) {
            $longests[$index] = $this->longestFinder->getLongestEntryLengthMb($column);
        }

        foreach ($table->getHeaders() as $header) {
            $dummy = str_repeat(' ', $longests[$header]);
            $longests[$header] = $this->longestFinder->getLongestEntryLengthMb([$dummy, $header]);
        }

        return $longests;
    }

    private function generateSeparator(array $longests, string $leftChar, string $middleChar, string $rightChar): string
    {
        $separator = [];
        $paddingWidth = $this->longestFinder->getLengthMb($this->paddingLeft) + $this->longestFinder->getLengthMb($this->paddingRight);

        foreach ($longests as $longest) {
            $separator[] = str_repeat('─', $longest + $paddingWidth);
        }

        return $leftChar . implode($middleChar, $separator) . $rightChar;
    }

    private function generateHeaders(array $longests, string $leftChar, string $middleChar, string $rightChar): string
    {
        $row = [];

        foreach ($longests as $name => $length) {
            $row[] = $this->mbStrPad($name, $length);
        }

        return $leftChar . implode($middleChar, $row) . $rightChar;
    }

    private function generateRow(array $indexedLongests, array $row, string $leftChar, string $middleChar, string $rightChar): string
    {
        $formattedRow = [];

        foreach ($row as $index => $content) {
            $formattedRow[] = $this->mbStrPad($content, $indexedLongests[$index]);
        }

        return $leftChar . implode($middleChar, $formattedRow) . $rightChar;
    }

    private function mbStrPad($input, int $padLength): string
    {
        $length  = $this->longestFinder->getLengthMb($input);
        $missing = $padLength - $length;

        return $this->paddingLeft . $input . str_repeat(' ', max(0, $missing)) . $this->paddingRight;
    }
}
