<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Utils\Formatter;

use FlyingAnvil\Libfa\Utils\Collection\LongestTextEntry;

class KeyValueFormatter
{
    private LongestTextEntry $longestFinder;

    public function __construct()
    {
        $this->longestFinder = new LongestTextEntry();
    }

    public function formatKeyValueTable(iterable $data): string
    {
        $keys   = [];
        $values = [];

        $longestKey   = 0;
        $longestValue = 0;

        foreach ($data as $key => $value) {
            $key   = (string)$key;
            $value = $this->makeString($value);

            $keys[]   = $key;
            $values[] = $value;

            $longestKey   = max($longestKey, $this->longestFinder->getLengthMb($key));
            $longestValue = max($longestValue, $this->longestFinder->getLengthMb($value));
        }

        $count = count($keys);
        $body  = sprintf(
            '╭%s┬%s╮',
            str_repeat('─', $longestKey + 2),
            str_repeat('─', $longestValue + 2),
        ) . PHP_EOL;

        for ($i = 0; $i < $count; $i++) {
            $key   = $keys[$i];
            $value = $values[$i];

            $body .= sprintf(
                '│ %s ├ %s │',
                $this->mbStrPad($key, $longestKey),
                $this->mbStrPad($value, $longestValue),
            ) . PHP_EOL;
        }

        $body .= sprintf(
            '╰%s┴%s╯',
            str_repeat('─', $longestKey + 2),
            str_repeat('─', $longestValue + 2),
        );

        return $body;
    }

    private function mbStrPad(string $input, int $padLength): string
    {
        $length  = $this->longestFinder->getLengthMb($input);
        $missing = $padLength - $length;

        return $input . str_repeat(' ', $missing);
    }

    private function makeString($in): string
    {
        if (is_bool($in)) {
            return $this->boolToString($in);
        }

        if (is_iterable($in)) {
            return '*multi-value*';
        }

        if (is_object($in) && !method_exists($in, '__toString')) {
            return '*object*';
        }

        if (is_object($in) && method_exists($in, 'toString')) {
            return $in->toString();
        }

        return (string)$in;
    }

    private function boolToString(bool $bool): string
    {
        return $bool
            ? 'true'
            : 'false';
    }
}
