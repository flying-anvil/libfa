<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Benchmark;

use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;

class Benchmark
{
    public function __construct(
        private int $blockSize,
        private int $blockCount,
        private ?ProgressBar $progressBar = null,
    ) {}

    public function benchmark(callable $testSubject, string $description = ''): Report
    {
        $times = [];

        $this->writeProgress(0, 1);

        for ($i = 0; $i < $this->blockCount; $i++) {
            $blockStart = hrtime(true);

            for ($b = 0; $b < $this->blockSize; $b++) {
                $testSubject();
            }

            $blockEnd  = hrtime(true);
            $times[$i] = ($blockEnd - $blockStart) * .001;

            $this->writeProgress($i, $this->blockCount);
        }

        $this->writeFinish();
        return new Report($this->blockSize, $this->blockCount, $times, $description);
    }

    private function writeProgress(float $current, float $total): void
    {
        if ($this->progressBar !== null) {
            $this->progressBar->writeProgress($current, $total);
        }
    }

    private function writeFinish(): void
    {
        if ($this->progressBar !== null) {
            echo "\r\e[K";
        }
    }
}
