<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Benchmark;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Utils\Formatter\KeyValueFormatter;
use FlyingAnvil\Libfa\Utils\Formatter\TableFormatter;

class Report implements DataObject
{
    private float $fastest = PHP_INT_MAX;
    private float $slowest = 0;
    private float $average;
    private float $total = 0;
    private int $averageSingle;
    private float $runsPerMs;

    /**
     * @param int $blockSize
     * @param int $blockCount
     * @param float[] $times Block times in µs (micro seconds)
     * @param string $description
     */
    public function __construct(
        private int $blockSize,
        private int $blockCount,
        private array $times,
        private string $description,
    ) {
        foreach ($times as $time) {
            if ($time < $this->fastest) {
                $this->fastest = $time;
            }

            if ($time > $this->slowest) {
                $this->slowest = $time;
            }

            $this->total += $time;
        }

        $this->average       = $this->total / count($times);
        $this->averageSingle = (int)(($this->total * 1000) / ($blockSize * $blockCount));
        $this->runsPerMs     = ($blockSize * $blockCount) / ($this->total * .001);
    }

    public function buildTextReport(): string
    {
        $format =
            'Description: %s' . PHP_EOL .
            'Block size:  %s' . PHP_EOL .
            'Block count: %s' . PHP_EOL .
            'Total runs:  %s' . PHP_EOL .
            'Fastest:     %.2f µs' . PHP_EOL .
            'Slowest:     %.2f µs' . PHP_EOL .
            'Average:     %.2f µs' . PHP_EOL .
            'Total:       %.2f ms' . PHP_EOL .
            'Average (single): %d ns' . PHP_EOL .
            'Runs per ms:      %.2f' . PHP_EOL;

        $totalRuns = $this->blockSize * $this->blockCount;
        $padLength = strlen((string)$totalRuns);

        return sprintf(
            $format,
            $this->description,
            str_pad((string)$this->blockSize, $padLength, ' ', STR_PAD_LEFT),
            str_pad((string)$this->blockCount, $padLength, ' ', STR_PAD_LEFT),
            $totalRuns,
            $this->fastest,
            $this->slowest,
            $this->average,
            $this->total * .001,
            $this->averageSingle,
            $this->runsPerMs,
        );
    }

    public function buildKeyValueReport(): string
    {
        $formatter = new KeyValueFormatter();
        $totalRuns = $this->blockSize * $this->blockCount;

        return $formatter->formatKeyValueTable([
            'Description'      => $this->description,
            'Block size'       => $this->blockSize,
            'Block count'      => $this->blockCount,
            'Total runs'       => $totalRuns,
            'Fastest'          => $this->fastest . ' µs',
            'Slowest'          => $this->slowest . ' µs',
            'Average'          => $this->average . ' µs',
            'Total'            => $this->total * .001 . ' ms',
            'Average (single)' => $this->averageSingle . ' ns',
            'Runs per ms'      => $this->runsPerMs,
        ]);
    }

    public function buildTableReport(): string
    {
        $totalRuns = $this->blockSize * $this->blockCount;
        $table     = Table::createEmpty(['Description', $this->description]);
        $table->addRows(
            ['Block size'      , $this->blockSize],
            ['Block count'     , $this->blockCount],
            ['Total runs'      , $totalRuns],
            ['Fastest'         , $this->fastest . ' µs'],
            ['Slowest'         , $this->slowest . ' µs'],
            ['Average'         , $this->average . ' µs'],
            ['Total'           , $this->total * .001 . ' ms'],
            ['Average (single)', $this->averageSingle . ' ns'],
            ['Runs per ms'     , $this->runsPerMs],
        );

        $formatter = new TableFormatter();
        $formatter->setPadding();
        return implode(PHP_EOL, iterator_to_array($formatter->generateFormattedRows($table))) . PHP_EOL;
    }

    //region Getters

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getBlockSize(): float
    {
        return $this->blockSize;
    }

    public function getBlockCount(): float
    {
        return $this->blockCount;
    }

    public function getTimes(): array
    {
        return $this->times;
    }

    public function getFastest(): float
    {
        return $this->fastest;
    }

    public function getSlowest(): float
    {
        return $this->slowest;
    }

    public function getAverage(): float
    {
        return $this->average;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return int Average time, a single execution took in ns
     */
    public function getAverageSingle(): int
    {
        return $this->averageSingle;
    }

    public function getRunsPerMs(): float
    {
        return $this->runsPerMs;
    }

    //endregion

    public function jsonSerialize(): array
    {
        return [
            'description'   => $this->description,
            'blockSize'     => $this->blockSize,
            'blockCount'    => $this->blockCount,
            'totalRuns'     => $this->blockSize * $this->blockCount,
            'fastest'       => $this->fastest . ' µs',
            'slowest'       => $this->slowest . ' µs',
            'average'       => $this->average . ' µs',
            'total'         => $this->total * .001 . ' ms',
            'averageSingle' => $this->averageSingle . ' ns',
            'runsPerMs'     => $this->runsPerMs,
        ];
    }
}
