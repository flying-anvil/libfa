<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Repository;

use FlyingAnvil\Libfa\Exception\UnsetEnvironmentVariableException;

class EnvironmentRepository
{
    public function getEnvironmentVariable(string $name, mixed $default = null, bool $defaultIsNull = false): mixed
    {
        $value = getenv($name);

        if ($value === false) {
            if ($default !== null || $defaultIsNull) {
                return $default;
            }

            throw new UnsetEnvironmentVariableException(sprintf(
                'Environment variable "%s" is not set',
                $name,
            ));
        }

        return $value;
    }
}

