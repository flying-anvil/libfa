<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use Stringable;
use FlyingAnvil\Libfa\DataObject\Exception\InvalidExtensionException;

class FileExtension implements DataObject, Stringable, StringValue
{
    /**
     * @param string $extension
     *
     * @throws InvalidExtensionException
     */
    private function __construct(protected string $extension)
    {
        if (!ctype_alnum($extension)) {
            throw new InvalidExtensionException(sprintf(
                'extension \'%s\' contains invalid characters',
                $extension,
            ));
        }
    }

    public static function createFromString(string $extension): self
    {
        return new self(ltrim($extension, '.'));
    }

    public static function createFromFilePath(string $filePath): self
    {
        $fileName = basename($filePath);
        $lastDot  = strrpos($fileName, '.');

        if ($lastDot === false) {
            throw new InvalidExtensionException(sprintf(
                'The file (%s) has no dot and thus no guessable extension',
                $fileName,
            ));
        }

        $extension = substr($fileName, $lastDot + 1);

        return new self($extension);
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function __toString(): string
    {
        return $this->extension;
    }

    public function toString(): string
    {
        return $this->extension;
    }

    public function jsonSerialize(): string
    {
        return $this->extension;
    }
}
