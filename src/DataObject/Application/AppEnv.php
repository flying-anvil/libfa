<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Application;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\Repository\EnvironmentRepository;
use JetBrains\PhpStorm\Immutable;
use ReflectionClass;
use ReflectionClassConstant;
use Stringable;

#[Immutable]
final class AppEnv implements DataObject, StringValue, Stringable
{
    public const ENV_TESTING     = 'testing';
    public const ENV_DEVELOPMENT = 'development';
    public const ENV_QA          = 'qa';
    public const ENV_PHPUNIT     = 'phpunit';
    public const ENV_PRODUCTION  = 'production';

    private function __construct(private string $appEnv)
    {
        $valid = self::getValidTypes();
        if (!in_array($appEnv, $valid)) {
            throw new ValueException(sprintf(
                'AppEnv "%s" is invalid',
                $appEnv,
            ));
        }
    }

    public static function create(string $appEnv): self
    {
        return new self($appEnv);
    }

    public static function createFromEnvironmentVariable(string $variable = 'APP_ENV'): self
    {
        $envRepo = new EnvironmentRepository();

        return new self($envRepo->getEnvironmentVariable($variable));
    }

    /**
     * @return string[]
     */
    public static function getValidTypes(): array
    {
        $reflection = new ReflectionClass(self::class);
        return $reflection->getConstants(ReflectionClassConstant::IS_PUBLIC);
    }

    /**
     * @param string $envToCheck One of AppEnv::ENV_*
     * @return bool
     */
    public function is(string $envToCheck): bool
    {
        return $this->appEnv === $envToCheck;
    }

    public function __toString(): string
    {
        return $this->appEnv;
    }

    public function toString(): string
    {
        return $this->appEnv;
    }

    public function jsonSerialize(): string
    {
        return $this->appEnv;
    }
}
