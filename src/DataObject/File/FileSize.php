<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\File;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use Stringable;

class FileSize implements DataObject, StringValue, Stringable
{
    private const BASE_1024 = 1024;
    private const BASE_1000 = 1000;

    protected function __construct(private int $size)
    {
        if ($size < 0 && PHP_INT_SIZE >= 8) {
            throw new ValueException(sprintf(
                'Filesize must be positive, %d given',
                $size,
            ));
        }
    }

    public static function create(int $size): self
    {
        return new self($size);
    }

    public function formatSuffixBase1024(): string
    {
        $prefixIndex = (int)(log($this->size, self::BASE_1024));
        $value       = $this->size / (self::BASE_1024 ** $prefixIndex);

        $prefix = SiPrefix::createFromIndex($prefixIndex);
        return sprintf(
            '%.2f %siB',
            $value,
            $prefix,
        );
    }

    public function formatSuffixBase1000(): string
    {
        $prefixIndex = (int)(log($this->size, self::BASE_1000));
        $value       = $this->size / (self::BASE_1000 ** $prefixIndex);

        $prefix = SiPrefix::createFromIndex($prefixIndex);
        return sprintf(
            '%.2f %sB',
            $value,
            $prefix,
        );
    }

    public function getValue(): int
    {
        return $this->size;
    }

    public function toString(): string
    {
        return (string)$this->size;
    }

    public function __toString(): string
    {
        return (string)$this->size;
    }

    public function jsonSerialize(): int
    {
        return $this->size;
    }
}
