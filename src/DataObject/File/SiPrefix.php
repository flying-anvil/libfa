<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\File;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class SiPrefix implements DataObject, StringValue, Stringable
{
    public const PREFIX_YOCTO  = 'y';
    public const PREFIX_ZEPTO  = 'z';
    public const PREFIX_ATTO   = 'a';
    public const PREFIX_FEMTO  = 'f';
    public const PREFIX_PICO   = 'p';
    public const PREFIX_NANO   = 'n';
    public const PREFIX_MICRO  = 'µ';
    public const PREFIX_MILLI  = 'm';
    public const PREFIX_NONE   = '';
    public const PREFIX_KILO   = 'k';
    public const PREFIX_MEGA   = 'M';
    public const PREFIX_GIGA   = 'G';
    public const PREFIX_TERA   = 'T';
    public const PREFIX_PETA   = 'P';
    public const PREFIX_EXA    = 'E';
    public const PREFIX_ZETTA  = 'Z';
    public const PREFIX_YOTTA  = 'Y';

    public const INDICES = [
        -8 => self::PREFIX_YOCTO,
        -7 => self::PREFIX_ZEPTO,
        -6 => self::PREFIX_ATTO,
        -5 => self::PREFIX_FEMTO,
        -4 => self::PREFIX_PICO,
        -3 => self::PREFIX_NANO,
        -2 => self::PREFIX_MICRO,
        -1 => self::PREFIX_MILLI,
        0  => self::PREFIX_NONE,
        1  => self::PREFIX_KILO,
        2  => self::PREFIX_MEGA,
        3  => self::PREFIX_GIGA,
        4  => self::PREFIX_TERA,
        5  => self::PREFIX_PETA,
        6  => self::PREFIX_EXA,
        7  => self::PREFIX_ZETTA,
        8  => self::PREFIX_YOTTA,
    ];

    public function __construct(private int $index) {}

    public static function createFromIndex(int $index): self
    {
        return new self($index);
    }

    public function toString(): string
    {
        return self::INDICES[$this->index] ?? '';
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function jsonSerialize(): string
    {
        return $this->toString();
    }
}
