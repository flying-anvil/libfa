<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Id;

use FlyingAnvil\Libfa\DataObject\Exception\FileException;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;

final class UUID implements ID
{
    public const UUID_LENGTH  = 36;
    private const SYSTEM_UUID = '/proc/sys/kernel/random/uuid';

    public function __construct(private string $uuid)
    {
        if (strlen($uuid) !== self::UUID_LENGTH) {
            throw new ValueException(sprintf(
                'UUID must be %d characters long, %d given',
                self::UUID_LENGTH,
                strlen($uuid),
            ));
        }

        if (!preg_match('/[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}/i', $uuid)) {
            throw new ValueException(sprintf(
                'UUID "%s" is malformed',
                $uuid,
            ));
        }
    }

    public static function createFromString(string $uuid): self
    {
        return new self($uuid);
    }

    public static function createFromSystemUuid(): self
    {
        if (!file_exists(self::SYSTEM_UUID)) {
            throw new FileException(sprintf(
                'File "%s" does not exist',
                self::SYSTEM_UUID,
            ));
        }

        $uuid = file_get_contents(self::SYSTEM_UUID);
        return new self($uuid);
    }

    public function toString(): string
    {
        return $this->uuid;
    }

    public function jsonSerialize(): string
    {
        return $this->uuid;
    }

    public function __toString(): string
    {
        return $this->uuid;
    }
}
