<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Id;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\RandomNumberGenerator;

final class CustomId extends AbstractGeneratable implements ID
{
    public const ALPHABET = 'abcdefghijklmnopqrstuvwxyz' .
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' .
        '0123456789' .
        '-_';

    private function __construct(private string $id, int $length)
    {
        if (strlen($id) !== $length) {
            throw new ValueException(sprintf(
                'Id must be exactly %d characters long, %d given',
                $length,
                strlen($id),
            ));
        }
    }

    public static function generate(int $length, string $alphabet = self::ALPHABET, RandomNumberGenerator $rng = null): self
    {
        return new self(parent::generateString($length, $alphabet, $rng), $length);
    }

    public static function createFromString(string $id, int $length): self
    {
        return new self($id, $length);
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function jsonSerialize(): string
    {
        return $this->id;
    }
}
