<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Id;

use \Stringable;
use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;

interface ID extends DataObject, StringValue, Stringable
{
}
