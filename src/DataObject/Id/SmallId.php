<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Id;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\RandomNumberGenerator;

final class SmallId extends AbstractGeneratable implements ID
{
    public const LENGTH   = 8;
    public const ALPHABET = 'abcdefghijklmnopqrstuvwxyz' .
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' .
        '0123456789' .
        '-_';

    private function __construct(private string $id)
    {
        if (strlen($id) !== self::LENGTH) {
            throw new ValueException(sprintf(
                'Id must be exactly %d characters long, %d given',
                self::LENGTH,
                strlen($id),
            ));
        }
    }

    public static function generate(RandomNumberGenerator $rng = null): self
    {
        return new self(parent::generateString(self::LENGTH, self::ALPHABET, $rng));
    }

    public static function createFromString(string $id): self
    {
        return new self($id);
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function jsonSerialize(): string
    {
        return $this->id;
    }
}
