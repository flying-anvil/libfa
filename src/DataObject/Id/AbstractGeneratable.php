<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Id;

use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\RandomNumberGenerator;

abstract class AbstractGeneratable
{
    protected static function generateString(int $length, string $alphabet, RandomNumberGenerator $rng = null): string
    {
        $rng = $rng ?? new BuiltinRandomNumberGenerator();

        $id = '';
        $alphabetLength = strlen($alphabet);

        for ($i = 0; $i < $length; $i++) {
            $index = (int)$rng->generateRange(0, $alphabetLength - 1);
            $char = $alphabet[$index];

            $id .= $char;
        }

        return $id;
    }

}
