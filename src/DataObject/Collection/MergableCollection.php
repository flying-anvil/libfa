<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Collection;

interface MergableCollection extends Collection
{
    public function merge(iterable $other);
}
