<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Collection;

use FlyingAnvil\Libfa\DataObject\Exception\MissingKeyException;
use Generator;

class UniversalCollection implements RecursiveCollection, MergableCollection
{
    private function __construct(private array $storage = []) {}

    public static function createFrom(array $data): self
    {
        return new self($data);
    }

    public static function createFromRecursive(array $data): self
    {
        $collection = new self();

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = self::createFromRecursive($value);
            }

            $collection->add((string)$key, $value);
        }

        return $collection;
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public function add(string|int $key, $value): self
    {
        $this->storage[$key] = $value;
        return $this;
    }

    public function remove(string|int $key): self
    {
        unset($this->storage[$key]);
        return $this;
    }

    /**
     * @param string|int $key
     * @param null $default
     * @param bool $defaultIsNull
     * @return mixed
     * @throws MissingKeyException
     */
    public function get(string|int $key, $default = null, $defaultIsNull = false): mixed
    {
        if (isset($this->storage[$key])) {
            return $this->storage[$key];
        }

        if ($default === null && !$defaultIsNull) {
            throw new MissingKeyException(sprintf(
                'key \'%s\' does not exist',
                $key,
            ));
        }

        return $default;
    }

    public function has(string|int $key): bool
    {
        return isset($this->storage[$key]);
    }

    public function merge(iterable $other, bool $allowOverride = false): void
    {
        foreach ($other as $key => $value) {
            if (!$allowOverride && isset($this->storage[$key])) {
                continue;
            }

            $this->add((string)$key, $value);
        }
    }

    public function count(): int
    {
        return count($this->storage);
    }

    public function jsonSerialize(): array
    {
        return $this->storage;
    }

    public function toArray(): array
    {
        return $this->storage;
    }

    public function toArrayRecursive(): array
    {
        $array = [];

        foreach ($this->storage as $key => $value) {
            if ($value instanceof self) {
                $value = $value->toArrayRecursive();
            }

            $array[$key] = $value;
        }

        return $array;
    }

    public function getIterator(): Generator
    {
        yield from $this->storage;
    }
}
