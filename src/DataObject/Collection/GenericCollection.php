<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Collection;

use BadMethodCallException;
use FlyingAnvil\Libfa\DataObject\Exception\InvalidTypeException;
use FlyingAnvil\Libfa\DataObject\Exception\MissingKeyException;
use Generator;
use TypeError;

class GenericCollection implements MergableCollection
{
    public const TYPE_BOOL            = 'boolean';
    public const TYPE_INT             = 'integer';
    public const TYPE_FLOAT           = 'double';
    public const TYPE_DOUBLE          = 'double';
    public const TYPE_STRING          = 'string';
    public const TYPE_ARRAY           = 'array';
    public const TYPE_OBJECT          = 'object';
    public const TYPE_RESOURCE        = 'resource';
    public const TYPE_RESOURCE_CLOSED = 'resource (closed)';
    public const TYPE_NULL            = 'NULL';
    public const TYPE_UNKNOWN         = 'unknown type';

    private const PRIMITIVE_TYPES = [
        self::TYPE_BOOL,
        self::TYPE_INT,
        self::TYPE_FLOAT,
        self::TYPE_DOUBLE,
        self::TYPE_STRING,
        self::TYPE_ARRAY,
        self::TYPE_OBJECT,
        self::TYPE_RESOURCE,
        self::TYPE_RESOURCE_CLOSED,
        self::TYPE_NULL,
        self::TYPE_UNKNOWN,
    ];

    private array $storage;

    private function __construct(
        private string $type,
        private bool $primitiveType,
    ) {
        if ($type === '') {
            throw new BadMethodCallException('Missing argument \'type\'');
        }

        if ($primitiveType && !in_array($type, self::PRIMITIVE_TYPES, true)) {
            throw new InvalidTypeException(sprintf(
                'Type \'%s\' is not a primitive type',
                $type,
            ));
        }

        $this->storage = [];
    }

    public static function createFrom(array $data, string $type = '', bool $primitiveType = false): self
    {
        $collection = new self($type, $primitiveType);

        foreach ($data as $key => $value) {
            $collection->add((string)$key, $value);
        }

        return $collection;
    }

    public static function createEmpty(string $type = '', bool $primitiveType = false): self
    {
        return new self($type, $primitiveType);
    }

    public function add(string $key, $value): self
    {
        if (!$this->validateType($value)) {
            throw new TypeError(sprintf(
                'Value is expected to be %s, %s given',
                $this->type,
                $this->primitiveType ? gettype($value) : get_class($value),
            ));
        }

        $this->storage[$key] = $value;
        return $this;
    }

    public function remove(string $key): self
    {
        unset($this->storage[$key]);
        return $this;
    }

    public function get(string $key, $default = null, $defaultIsNull = false): mixed
    {
        if (isset($this->storage[$key])) {
            return $this->storage[$key];
        }

        if ($default === null && !$defaultIsNull) {
            throw new MissingKeyException(sprintf(
                'key \'%s\' does not exist',
                $key,
            ));
        }

        return $default;
    }

    public function has(string $key): bool
    {
        return isset($this->storage[$key]);
    }

    public function toArray(): array
    {
        return $this->storage;
    }

    public function getIterator(): Generator
    {
        yield from $this->storage;
    }

    public function count(): int
    {
        return count($this->storage);
    }

    public function jsonSerialize(): array
    {
        return $this->storage;
    }

    public function merge(iterable $other, bool $allowOverride = false): void
    {
        foreach ($other as $key => $value) {
            if (!$allowOverride && isset($this->storage[$key])) {
                continue;
            }

            $this->add((string)$key, $value);
        }
    }

    public function validateType(mixed $value): bool
    {
        if ($this->primitiveType) {
            return gettype($value) === $this->type;
        }

        return $value instanceof $this->type;
    }
}
