<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Collection;

interface RecursiveCollection extends Collection
{
    public static function createFromRecursive(array $data);

    public function toArrayRecursive(): array;
}
