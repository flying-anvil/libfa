<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use IteratorAggregate;

interface Collection extends DataObject, Countable, IteratorAggregate
{
    public static function createFrom(array $data);

    public static function createEmpty();

    public function add(string $key, $value);

    public function remove(string $key);

    public function get(string $key, $default = null, $defaultIsNull = false);

    public function has(string $key): bool;

    public function toArray(): array;
}
