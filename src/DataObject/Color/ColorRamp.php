<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color;

use FlyingAnvil\Libfa\Exception\LibfaException;
use FlyingAnvil\Libfa\Math\Math;
use InvalidArgumentException;

class ColorRamp
{
    /**
     * @param ColorRampStep[] $steps
     */
    private function __construct(
        private array $steps,
    ) {
        if (empty($this->steps)) {
            throw new InvalidArgumentException('Steps must not be empty');
        }

        $this->steps = array_values($this->steps);
        usort(
            $this->steps,
            static fn(ColorRampStep $left, ColorRampStep $right) => $left->getPercentage() <=> $right->getPercentage(),
        );
    }

    public static function create(ColorRampStep ...$steps): self
    {
        return new self($steps);
    }

    public function evaluate(float $percentage): Color
    {
        if (count($this->steps) === 1 || $percentage < $this->steps[0]->getPercentage()) {
            return $this->steps[0]->getColor();
        }

        $lastStep = $this->steps[array_key_last($this->steps)];
        if ($percentage > $lastStep->getPercentage()) {
            return $lastStep->getColor();
        }

        $index = $this->findIndexForPercentage($percentage);

        $from = $this->steps[$index];
        $to   = $this->steps[$index + 1];

        return Color::lerp(
            $from->getColor(),
            $to->getColor(),
            Math::map(
                $from->getPercentage(),
                $to->getPercentage(),
                0.,
                1.,
                $percentage,
            ),
        );
    }

    private function findIndexForPercentage(float $percentage): int
    {
        foreach ($this->steps as $index => $step) {
            if ($step->getPercentage() > $percentage) {
                return $index - 1;
            }
        }

        return array_key_last($this->steps) - 1;
//        throw new LibfaException('Could not find index for color ramp step');
    }
}
