<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color;

use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
class ColorRampStep implements DataObject
{
    private function __construct(
        private float $percentage,
        private Color $color,
    ) {}

    public static function create(float $percentage, Color $color): self
    {
        return new self($percentage, $color);
    }

    public function getPercentage(): float
    {
        return $this->percentage;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function jsonSerialize(): array
    {
        return [
            'percentage' => $this->percentage,
            'color'      => $this->color,
        ];
    }
}
