<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\AbstractColorPalette;
use FlyingAnvil\Libfa\DataObject\Exception\RangeException;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\Math\Math;
use JetBrains\PhpStorm\Deprecated;
use Stringable;

class Color implements DataObject, Stringable, StringValue
{
    private const MIN_VALUE = 0;
    private const MAX_VALUE = 255;

    private static Range $validRange;

    public function __construct(
        private int $red,
        private int $green,
        private int $blue,
        private int $alpha,
    ) {
        if (!isset(self::$validRange)) {
            self::$validRange = Range::create(self::MIN_VALUE, self::MAX_VALUE);
        }

        $this->validateRange($red);
        $this->validateRange($green);
        $this->validateRange($blue);
        $this->validateRange($alpha);
    }

    public static function create(int $red, int $green, int $blue, int $alpha = self::MAX_VALUE): self
    {
        return new self($red, $green, $blue, $alpha);
    }

//    public static function createFromDefinition(array $definition): self
//    {
//        if (!isset($definition[0], $definition[1], $definition[2])) {
//            throw new ValueException(
//                'Color definition is malformed. Expected array with 3 or 4 values at keys 0 upwards'
//            );
//        }
//
//        return new self(...$definition);
//    }

    public static function createFromPalette(AbstractColorPalette $palette, string $colorName): self
    {
        return $palette->resolveName($colorName);
    }

    public static function createFromHexChannels(string $red, string $green, string $blue, string $alpha = 'FF'): self
    {
        return new self(
            hexdec($red),
            hexdec($green),
            hexdec($blue),
            hexdec($alpha),
        );
    }

    public static function createFromHexString(string $hex): self
    {
        if ($hex[0] === '#') {
            $hex = substr($hex, 1);
        }

        $red   = '00';
        $green = '00';
        $blue  = '00';
        $alpha = 'FF';

        switch (strlen($hex)) {
            case 1:
                // C => #CCCCCC
                $red   = $hex . $hex;
                $green = $hex . $hex;
                $blue  = $hex . $hex;
                break;
            case 2:
                // CA => #CACACA
                $red   = $hex[0] . $hex[1];
                $green = $hex[0] . $hex[1];
                $blue  = $hex[0] . $hex[1];
                break;
            case 3:
                // CAB => #CCAABB
                $red   = $hex[0] . $hex[0];
                $green = $hex[1] . $hex[1];
                $blue  = $hex[2] . $hex[2];
                break;
            case 4:
                // CABD => #CCAABBDD
                $red   = $hex[0] . $hex[0];
                $green = $hex[1] . $hex[1];
                $blue  = $hex[2] . $hex[2];
                $alpha = $hex[3] . $hex[3];
                break;
            case 6:
                // 00FF00 => #00FF00
                $red   = $hex[0] . $hex[1];
                $green = $hex[2] . $hex[3];
                $blue  = $hex[4] . $hex[5];
                break;
            case 8:
                // 00FF00CC => #00FF00CC
                $red   = $hex[0] . $hex[1];
                $green = $hex[2] . $hex[3];
                $blue  = $hex[4] . $hex[5];
                $alpha = $hex[6] . $hex[7];
                break;
            default:
                throw new ValueException(sprintf(
                    'Hex string "%s" is not valid',
                    $hex,
                ));
        }

        return new self(
            hexdec($red),
            hexdec($green),
            hexdec($blue),
            hexdec($alpha),
        );
    }

    /**
     * @param int $alpha set to < 0 to generate random alpha
     *
     * @return Color
     * @throws \Exception
     */
    public static function createRandom(int $alpha = self::MAX_VALUE): self
    {
        return new self(
            random_int(self::MIN_VALUE, self::MAX_VALUE),
            random_int(self::MIN_VALUE, self::MAX_VALUE),
            random_int(self::MIN_VALUE, self::MAX_VALUE),
            $alpha < self::MIN_VALUE ? random_int(self::MIN_VALUE, self::MAX_VALUE) : $alpha,
        );
    }

    public static function createRestrictedRandom(?Range $restrictRed = null, ?Range $restrictGreen = null, ?Range $restrictBlue = null, ?Range $restrictAlpha = null): self
    {
        $redRangeMin = self::MIN_VALUE;
        $redRangeMax = self::MAX_VALUE;

        $greenRangeMin = self::MIN_VALUE;
        $greenRangeMax = self::MAX_VALUE;

        $blueRangeMin = self::MIN_VALUE;
        $blueRangeMax = self::MAX_VALUE;

        $alphaRangeMin = self::MAX_VALUE; // Not a typo, default to max alpha like in ::createRandom
        $alphaRangeMax = self::MAX_VALUE;

        if ($restrictRed !== null) {
            $redRangeMin = $restrictRed->getLowerBound();
            $redRangeMax = $restrictRed->getUpperBound();
        }

        if ($restrictGreen !== null) {
            $greenRangeMin = $restrictGreen->getLowerBound();
            $greenRangeMax = $restrictGreen->getUpperBound();
        }

        if ($restrictBlue !== null) {
            $blueRangeMin = $restrictBlue->getLowerBound();
            $blueRangeMax = $restrictBlue->getUpperBound();
        }

        if ($restrictAlpha !== null) {
            $alphaRangeMin = $restrictAlpha->getLowerBound();
            $alphaRangeMax = $restrictAlpha->getUpperBound();
        }

        return new self(
            random_int($redRangeMin, $redRangeMax),
            random_int($greenRangeMin, $greenRangeMax),
            random_int($blueRangeMin, $blueRangeMax),
            random_int($alphaRangeMin, $alphaRangeMax),
        );
    }

    #[Deprecated('With ColorRamp there\'s a more flexible solution', ColorRamp::class)]
    public static function triLerp(Color $low, Color $mid, Color $high, float $percentage): Color
    {
        if ($percentage > .4999 && $percentage < .5001) {
            return $mid;
        }

        if ($percentage > .5) {
            return self::lerp($mid, $high, ($percentage * 2) - 1);
        }

        return self::lerp($low, $mid, $percentage * 2);
    }

    public static function lerp(Color $from, Color $to, float $percentage): Color
    {
        $newR = (int)Math::lerp($from->getRed(), $to->getRed(), $percentage);
        $newG = (int)Math::lerp($from->getGreen(), $to->getGreen(), $percentage);
        $newB = (int)Math::lerp($from->getBlue(), $to->getBlue(), $percentage);

        return self::create($newR, $newG, $newB);
    }

    public function getHexRepresentation(): string
    {
        $red   = str_pad(dechex($this->red), 2, '0', STR_PAD_LEFT);
        $green = str_pad(dechex($this->green), 2, '0', STR_PAD_LEFT);
        $blue  = str_pad(dechex($this->blue), 2, '0', STR_PAD_LEFT);

        return strtoupper($red . $green . $blue);
    }
    public function getHexRepresentationWithAlpha(): string
    {
        $alpha = str_pad(dechex($this->alpha), 2, '0', STR_PAD_LEFT);

        return $this->getHexRepresentation() . strtoupper($alpha);
    }

    public function getRed(): int
    {
        return $this->red;
    }

    public function getGreen(): int
    {
        return $this->green;
    }

    public function getBlue(): int
    {
        return $this->blue;
    }

    public function getAlpha(): int
    {
        return $this->alpha;
    }

    private function validateRange(int $channel): void
    {
        if (!self::$validRange->check($channel)) {
            throw new RangeException(sprintf(
                'Channel must be in the range %s, %d given',
                self::$validRange,
                $channel,
            ));
        }
    }

    public function toHsvColor(): ColorHsv
    {
        return ColorHsv::fromColorRgb($this);
    }

    public function toString(): string
    {
        return $this->getHexRepresentation();
    }

    public function __toString(): string
    {
        return $this->getHexRepresentation();
    }

    public function jsonSerialize(): array
    {
        return [
            'red'   => $this->red,
            'green' => $this->green,
            'blue'  => $this->blue,
            'alpha' => $this->alpha,
        ];
    }
}
