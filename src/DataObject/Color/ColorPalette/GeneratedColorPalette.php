<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Exception\RangeException;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\DataObject\Math\Vector3;
use FlyingAnvil\Libfa\Math\Math;
use FlyingAnvil\Libfa\Random\BuiltinRandomNumberGenerator;
use FlyingAnvil\Libfa\Random\RandomNumberGenerator;

class GeneratedColorPalette extends IndexedColorPalette
{
    public function __construct(
        private Vector3 $bias,
        private Vector3 $scale,
        private Vector3 $cosineOscillations,
        private Vector3 $cosinePhase,
        private bool $clampSaturation,
    ) {
        parent::__construct();
    }

    /**
     * @param int $count
     * @param Vector3 $bias X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $scale X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $cosineOscillations X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $cosinePhase X/Y/Z represents each color channel (r/g/b)
     * @param bool $clampSaturation If allowed, color values under 0 and over 255 are clamped
     * @return static
     */
    public static function generate(int $count, Vector3 $bias, Vector3 $scale, Vector3 $cosineOscillations, Vector3 $cosinePhase, bool $clampSaturation = false): self
    {
        $palette = new self($bias, $scale, $cosineOscillations, $cosinePhase, $clampSaturation);

        for ($i = 0; $i < $count; $i++) {
            // $time never reaches 1, and that's ok because it prevents the same color at 0 and 1
            $time = $i / $count;

            // $time = $i / ($count - 1); // Causes same color at 0 and 1

            $rawColor = self::calculateChannels($time, $bias, $scale, $cosineOscillations, $cosinePhase);

            $red   = (int)($rawColor->getX() * 255);
            $green = (int)($rawColor->getY() * 255);
            $blue  = (int)($rawColor->getZ() * 255);

            if ($clampSaturation) {
                $red   = Math::clamp($red, 0, 255);
                $green = Math::clamp($green, 0, 255);
                $blue  = Math::clamp($blue, 0, 255);
            }

            try {
                $palette->addColor(Color::create(
                    $red,
                    $green,
                    $blue,
                ));
            } catch (RangeException $exception) {
                throw new ValueException(
                    'Sum of bias and scale is > 1.0, which oversaturates color channel',
                    previous: $exception,
                );
            }
        }

        return $palette;
    }

    public static function generateRandom(
        int $count,
        ?Range $biasRange = null,
        ?Range $scaleRange = null,
        ?Range $cosineOscillationsRange = null,
        ?Range $cosinePhaseRange = null,
        ?RandomNumberGenerator $rng = null,
        bool $clampSaturation = false,
    ): self {
        $biasRange               = $biasRange               ?? Range::create(0.0, 1.0);
        $scaleRange              = $scaleRange              ?? Range::create(0.0, 1.0);
        $cosineOscillationsRange = $cosineOscillationsRange ?? Range::create(0.0, 2.0);
        $cosinePhaseRange        = $cosinePhaseRange        ?? Range::create(0.0, 1.0);
        $rng = $rng ?? new BuiltinRandomNumberGenerator();

        $bias               = self::generateVector3FromRange($biasRange, $rng);
        $scale              = self::generateVector3FromRange($scaleRange, $rng);
        $cosineOscillations = self::generateVector3FromRange($cosineOscillationsRange, $rng);
        $cosinePhase        = self::generateVector3FromRange($cosinePhaseRange, $rng);

        if (!$clampSaturation) {
            [$bias, $scale] = self::restrictBiasAndScale($bias, $scale);
        }

        // TODO Try to pass false as last Parameter if bias/scale have been restricted
        return self::generate($count, $bias, $scale, $cosineOscillations, $cosinePhase, !$clampSaturation);
    }

    private static function generateVector3FromRange(Range $range, RandomNumberGenerator $rng): Vector3
    {
        $x = $rng->generateRange($range->getLowerBound(), $range->getUpperBound());
        $y = $rng->generateRange($range->getLowerBound(), $range->getUpperBound());
        $z = $rng->generateRange($range->getLowerBound(), $range->getUpperBound());

        return Vector3::create($x, $y, $z);
    }

    private static function restrictBiasAndScale(Vector3 $bias, Vector3 $scale): array
    {
        [$biasX, $scaleX] = self::restrict($bias->getX(), $scale->getX());
        [$biasY, $scaleY] = self::restrict($bias->getY(), $scale->getY());
        [$biasZ, $scaleZ] = self::restrict($bias->getZ(), $scale->getZ());

        $newBias  = Vector3::create($biasX, $biasY, $biasZ);
        $newScale = Vector3::create($scaleX, $scaleY, $scaleZ);

        return [$newBias, $newScale];
    }

    private static function restrict(int|float $a, int|float $b): array
    {
        $sum = $a + $b;

        // No restriction needed
        if ($sum >= 0.0 && $sum <= 1.0) {
            return [$a, $b];
        }

        return [
            $a / $sum,
            $b / $sum,
        ];
    }

    /**
     * Based on https://iquilezles.org/www/articles/palettes/palettes.htm
     *
     * @param float $time
     * @param Vector3 $bias X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $scale X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $cosineOscillations X/Y/Z represents each color channel (r/g/b)
     * @param Vector3 $cosinePhase X/Y/Z represents each color channel (r/g/b)
     * @return Vector3
     */
    public static function calculateChannels(float $time, Vector3 $bias, Vector3 $scale, Vector3 $cosineOscillations, Vector3 $cosinePhase): Vector3
    {
        return Vector3::create(
            self::calculateChannel($time, $bias->getX(), $scale->getX(), $cosineOscillations->getX(), $cosinePhase->getX()),
            self::calculateChannel($time, $bias->getY(), $scale->getY(), $cosineOscillations->getY(), $cosinePhase->getY()),
            self::calculateChannel($time, $bias->getZ(), $scale->getZ(), $cosineOscillations->getZ(), $cosinePhase->getZ()),
        );
    }

    public static function calculateChannel(int|float $time, int|float $bias, int|float $scale, int|float $cosineOscillations, int|float $cosinePhase): float
    {
        return $bias + $scale * cos(Math::TAU * ($cosineOscillations * $time + $cosinePhase));
    }

    public function getBias(): Vector3
    {
        return $this->bias;
    }

    public function getScale(): Vector3
    {
        return $this->scale;
    }

    public function getCosineOscillations(): Vector3
    {
        return $this->cosineOscillations;
    }

    public function getCosinePhase(): Vector3
    {
        return $this->cosinePhase;
    }

    public function isClampSaturation(): bool
    {
        return $this->clampSaturation;
    }
}
