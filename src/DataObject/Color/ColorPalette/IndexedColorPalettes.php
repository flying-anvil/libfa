<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Wrapper\File;
use Generator;
use IteratorAggregate;
use OutOfRangeException;
use Stringable;

class IndexedColorPalettes implements DataObject, IteratorAggregate, Countable, Stringable
{
    /** @var IndexedColorPalette[] */
    private array $palettes = [];

    private function __construct(
        private int $colorsPerRow,
    ) {}

    /**
     * Assumes each Palette has an equal amount of colors
     * @param IndexedColorPalette $palette
     */
    private function addPalette(IndexedColorPalette $palette): void
    {
        $this->palettes[] = $palette;
    }

    public static function loadFromPalFile(File $file, int $colorsPerRow = 0): self
    {
        $file->open();
        $fileSize = $file->getFileSize();

        if ($colorsPerRow === 0) {
            $colorsPerRow = self::guessColorsPerRow($file);
        }

        $self            = new self($colorsPerRow);
        $currentPalette  = IndexedColorPalette::createEmpty();
        $colorsInPalette = 0;
        $self->addPalette($currentPalette);

        for ($i = 0; $i < $fileSize; $i += 3) {
            $r = ord($file->readChar());
            $g = ord($file->readChar());
            $b = ord($file->readChar());

            $currentPalette->addColor(Color::create($r, $g, $b));

            if ($colorsPerRow > 0 && ++$colorsInPalette % $colorsPerRow === 0 && $i < $fileSize - 3) {
                $currentPalette  = IndexedColorPalette::createEmpty();
                $colorsInPalette = 0;
                $self->addPalette($currentPalette);
            }
        }

        return $self;
    }

    private static function guessColorsPerRow(File $file): int
    {
        $root = ($file->getFileSize() / 3) ** .5;

        if (floor($root) === $root) {
            return (int)$root;
        }

        return 0;
    }

    public function getPaletteByIndex(int $index): IndexedColorPalette
    {
        return $this->palettes[$index];
    }

    public function getColorByCoordinate(int $x, int $y): Color
    {
        return $this->palettes[$y]->getColorByIndex($x);
    }

    public function getColorByIndex(int $index): Color
    {
        $paletteIndex = (int)floor($index / $this->colorsPerRow);
        $colorIndex   = $index % $this->colorsPerRow;

        $palette = $this->palettes[$paletteIndex] ?? null;

        if ($palette === null) {
            throw new OutOfRangeException(sprintf(
                'Cannot get color %d, palette has fewer colors',
                $index,
            ));
        }

        return $palette->getColorByIndex($colorIndex);
    }

    public function saveAsFile(File $target): void
    {
        $target->open('wb');

        foreach ($this->palettes as $index => $palette) {
            foreach ($palette as $color) {
                $target->write(sprintf(
                    '%s%s%s',
                    chr($color->getRed()),
                    chr($color->getGreen()),
                    chr($color->getBlue()),
                ));
            }
        }
    }

    /**
     * @return Generator<IndexedColorPalette> | IndexedColorPalette[]
     */
    public function getIterator(): Generator
    {
        yield from $this->palettes;
    }

    public function count(): int
    {
        return $this->countPalettes();
    }

    public function countPalettes(): int
    {
        return count($this->palettes);
    }

    public function countColors(): int
    {
        $count = 0;

        foreach ($this->palettes as $palette) {
            $count += count($palette);
        }

        return $count;
    }

    public function jsonSerialize(): array
    {
        return $this->palettes;
    }

    public function __toString(): string
    {
        return implode(PHP_EOL, $this->palettes);
    }
}
