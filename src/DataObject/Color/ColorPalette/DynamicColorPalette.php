<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Exception\MissingKeyException;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Collection\GenericCollection;

class DynamicColorPalette extends AbstractColorPalette
{
    private static DynamicColorPalette $latest;

    private GenericCollection $colors;

    protected function __construct()
    {
        $this->colors = GenericCollection::createEmpty(Color::class);
        self::$latest = $this;

        parent::__construct();
    }

    public static function getPalette(): AbstractColorPalette
    {
        if (!isset(self::$latest)) {
            self::$latest = self::create();
        }

        return self::$latest;
    }

    public static function create(): self
    {
        return new self();
    }

    public function addColor(string $name, Color $color): self
    {
        $this->colors->add($name, $color);

        return $this;
    }

    public function resolveName(string $name): Color
    {
        try {
            return $this->colors->get($name);
        } catch (MissingKeyException $e) {
            throw new ValueException(sprintf(
                'Name \'%s\' is not in this collection',
                $name,
            ));
        }
    }

    public function getColors(): array
    {
        return $this->colors->toArray();
    }
}
