<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;

class HtmlColorPalette extends AbstractColorPalette
{
    public const COLOR_WHITE   = 'white';
    public const COLOR_SILVER  = 'silver';
    public const COLOR_GRAY    = 'gray';
    public const COLOR_BLACK   = 'black';
    public const COLOR_RED     = 'red';
    public const COLOR_MAROON  = 'maroon';
    public const COLOR_YELLOW  = 'yellow';
    public const COLOR_OLIVE   = 'olive';
    public const COLOR_LIME    = 'lime';
    public const COLOR_GREEN   = 'green';
    public const COLOR_AQUA    = 'aqua';
    public const COLOR_TEAL    = 'teal';
    public const COLOR_BLUE    = 'blue';
    public const COLOR_NAVY    = 'navy';
    public const COLOR_FUCHSIA = 'fuchsia';
    public const COLOR_PURPLE  = 'purple';

    private const COLORS = [
        self::COLOR_WHITE   => [255, 255, 255],
        self::COLOR_SILVER  => [192, 192, 192],
        self::COLOR_GRAY    => [128, 128, 128],
        self::COLOR_BLACK   => [  0,   0,   0],
        self::COLOR_RED     => [255,   0,   0],
        self::COLOR_MAROON  => [128,   0,   0],
        self::COLOR_YELLOW  => [255, 255,   0],
        self::COLOR_OLIVE   => [128, 128,   0],
        self::COLOR_LIME    => [  0, 255,   0],
        self::COLOR_GREEN   => [  0, 128,   0],
        self::COLOR_AQUA    => [  0, 255, 255],
        self::COLOR_TEAL    => [  0, 128, 128],
        self::COLOR_BLUE    => [  0,   0, 255],
        self::COLOR_NAVY    => [  0,   0, 128],
        self::COLOR_FUCHSIA => [ 255,  0, 255],
        self::COLOR_PURPLE  => [ 128,  0, 128],
    ];

    public function resolveName(string $name): Color
    {
        if (!isset(self::COLORS[$name])) {
            throw new ValueException(sprintf(
                'Name \'%s\' is not a valid HTML color name',
                $name,
            ));
        }

        return Color::create(...self::COLORS[$name]);
    }

    public function getColors(): array
    {
        return self::COLORS;
    }
}
