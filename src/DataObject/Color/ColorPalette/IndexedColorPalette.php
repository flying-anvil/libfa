<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;
use FlyingAnvil\Libfa\Wrapper\File;
use Generator;
use IteratorAggregate;
use OutOfRangeException;
use Stringable;

class IndexedColorPalette extends AbstractColorPalette implements DataObject, IteratorAggregate, Countable, Stringable
{
    /** @var Color[] */
    private array $colors = [];

    protected function __construct()
    {
        parent::__construct();
    }

    public function addColor(Color ...$colors): void
    {
        foreach ($colors as $color) {
            $this->colors[] = $color;
        }
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public static function loadFromPalFile(File $file): self
    {
        $file->open();
        $self = new self();

        $fileSize = $file->getFileSize();
        for ($i = 0; $i < $fileSize; $i += 3) {
            $r = ord($file->readChar());
            $g = ord($file->readChar());
            $b = ord($file->readChar());

            $self->addColor(Color::create($r, $g, $b));
        }

        return $self;
    }

    public function getColorByIndex(int $index): Color
    {
        $color = $this->colors[$index] ?? null;
        if ($color === null) {
            throw new OutOfRangeException(sprintf(
                'Cannot get color %d, palette has fewer colors',
                $index,
            ));
        }

        return $color;
    }

    public function resolveName(string $name): Color
    {
        return $this->getColorByIndex((int)$name);
    }

    public function saveAsFile(File $target): void
    {
        $target->open('wb');

        foreach ($this->colors as $color) {
            $target->write(sprintf(
                '%s%s%s',
                chr($color->getRed()),
                chr($color->getGreen()),
                chr($color->getBlue()),
            ));
        }
    }

    public function getColors(): array
    {
        return $this->colors;
    }

    /**
     * @return Generator<Color> | Color[]
     */
    public function getIterator(): Generator
    {
        yield from $this->colors;
    }

    public function count(): int
    {
        return count($this->colors);
    }

    public function jsonSerialize(): array
    {
        return $this->colors;
    }

    public function __toString(): string
    {
        $output = '';

        foreach ($this->colors as $index => $color) {
            $output .= ColorBlockOutput::create($color);
        }

        return $output;
    }
}
