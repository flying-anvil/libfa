<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color\ColorPalette;

use FlyingAnvil\Libfa\DataObject\Color\Color;

abstract class AbstractColorPalette
{
    /** @var self[] */
    private static array $instances = [];

    protected function __construct() {}

    public static function getPalette(): static
    {
        $paletteClass = static::class;

        if (!isset(self::$instances[$paletteClass])) {
            self::$instances[$paletteClass] = new static();
        }

        return self::$instances[$paletteClass];
    }

    public function getRandomColor(): Color
    {
        $colors = $this->getColors();
        $key    = array_rand($colors);

        return $this->resolveName($key);
    }

    /**
     * @return Color[]
     */
    // TODO: Make Colors Collection or name it "toArray" because the palette already is the collection
    abstract public function getColors(): array;

    abstract public function resolveName(string $name): Color;
}
