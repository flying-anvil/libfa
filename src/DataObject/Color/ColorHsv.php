<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Color;

use FlyingAnvil\Libfa\DataObject\DataObject;

class ColorHsv implements DataObject
{
    private function __construct(
        private float $hue,
        private float $saturation,
        private float $value,
    ) {}

    public static function create(float $hue, float $saturation, float $value): self
    {
        return new self($hue, $saturation, $value);
    }

    public static function fromColorRgb(Color $color): self
    {
        // RGB Values:  Number 0-255
        // HSV Results: Number 0-1

        $red01   = ($color->getRed() / 255);
        $green01 = ($color->getGreen() / 255);
        $blue01  = ($color->getBlue() / 255);

        $min  = min($red01, $green01, $blue01);
        $max  = max($red01, $green01, $blue01);
        $diff = $max - $min;

        $value = $max;

        if ($diff <= 0)
        {
            return new self(0, 0, $value);
        }

        $saturation = $diff / $max;

        $diffRed   = ((($max - $red01) / 6) + ($diff / 2)) / $diff;
        $diffGreen = ((($max - $green01) / 6) + ($diff / 2)) / $diff;
        $diffBlue  = ((($max - $blue01) / 6) + ($diff / 2)) / $diff;

        $hue = 0;

        if ($red01 === $max) {
            $hue = $diffBlue - $diffGreen;
        } else if ($green01 === $max) {
            $hue = (1 / 3) + $diffRed - $diffBlue;
        } else if ($blue01 === $max) {
            $hue = (2 / 3) + $diffGreen - $diffRed;
        }

        // hue %= 1
        if ($hue > 1) {
            $hue--;
        }

        // hue %= 1
        if ($hue < 0) {
            $hue++;
        }

        return new self($hue, $saturation, $value);
    }

    public function getHue(): float
    {
        return $this->hue;
    }

    public function getSaturation(): float
    {
        return $this->saturation;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function jsonSerialize(): array
    {
        return [
            'h' => $this->hue,
            's' => $this->saturation,
            'v' => $this->value,
        ];
    }
}
