<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject;

use BadMethodCallException;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Generator;
use IteratorAggregate;

class Table implements DataObject, IteratorAggregate
{
    /** @var string[] */
    private array $headers;

    /** @var string[][] */
    private array $rows;

    private int $columnCount;

    private function __construct(array $headers, array $rows)
    {
        $this->headers = $headers;
        $this->rows    = $rows;

        $this->columnCount = count($headers);
    }

    public static function createEmpty(array $headers): self
    {
        return new self($headers, []);
    }

    public function addRows(array ...$rows): self
    {
        foreach ($rows as $row) {
            $rowColumnCount = count($row);
            if ($rowColumnCount !== $this->columnCount) {
                throw new BadMethodCallException(sprintf(
                    'Headers contain %d columns, but row has %d',
                    $this->columnCount,
                    $rowColumnCount,
                ));
            }

            $this->rows[] = $row;
        }

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return Generator<array>
     */
    public function getRows(): Generator
    {
        yield from $this->rows;
    }

    public function getColumns(): iterable
    {
        $headerCount = count($this->headers);
        for ($i = 0; $i < $headerCount; $i++) {
            $column = [];

            foreach ($this->rows as $row) {
                $column[] = $row[$i];
            }

            yield $this->headers[$i] => $column;
        }
    }

    public function getIterator(): Generator
    {
        yield from $this->getRows();
    }

    public function jsonSerialize(): array
    {
        return [
            'headers' => $this->headers,
            'rows'    => $this->rows,
        ];
    }
}
