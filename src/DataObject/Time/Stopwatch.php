<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Time;

class Stopwatch
{
    // TODO: Check if started/stopped

    /** @var int[] */
    private array $splits = [];

    private int $start;
    private int $end;

    public function start(): void
    {
        $this->start = hrtime(true);
    }

    public function stop(): void
    {
        $this->end = hrtime(true);
        $this->split();
    }

    public function split(): void
    {
        $this->splits[] = hrtime(true) - $this->start;
    }

    public function getDurationAsNanoSeconds(): float
    {
        return $this->end - $this->start;
    }

    public function getDurationAsMicroSeconds(): float
    {
        return ($this->end - $this->start) * 1e-3;
    }

    public function getDurationAsMilliSeconds(): float
    {
        return ($this->end - $this->start) * 1e-6;
    }

    public function getDurationAsSeconds(): float
    {
        return ($this->end - $this->start) * 1e-9;
    }

    public function getDurationAsMinutes(): float
    {
        return $this->getDurationAsSeconds() / 60;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function getEnd(): int
    {
        return $this->end;
    }

    public function getSplits(): array
    {
        return $this->splits;
    }
}
