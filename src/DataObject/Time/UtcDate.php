<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Time;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class UtcDate implements DataObject, StringValue, Stringable
{
    public const FORMAT_HUMAN = 'Y-m-d H:i:s';
    public const FORMAT_ATOM  = DATE_ATOM;

    private function __construct(private DateTimeImmutable $date) {}

    public static function now(): self
    {
        $utc  = new DateTimeZone('UTC');
        $date = new DateTimeImmutable();
        $date = $date->setTimezone($utc);

        return new self($date);
    }

    public static function createFromDateTime(DateTimeInterface $dateTime): self
    {
        $utc  = new DateTimeZone('UTC');
        $date = DateTimeImmutable::createFromInterface($dateTime);
        $date = $date->setTimezone($utc);

        return new self($date);
    }

    /**
     * @param string $format
     * @param string $dateTime
     * @param DateTimeZone|null $timeZone Timezone, the dateTime given is in. null assumes the given date is in UTC
     * @return static
     */
    public static function createFromFormat(string $format, string $dateTime, ?DateTimeZone $timeZone = null): self
    {
        $utc  = new DateTimeZone('UTC');

        if ($timeZone === null) {
            $timeZone = $utc;
        }

        $date = DateTimeImmutable::createFromFormat($format, $dateTime, $timeZone);
        $date = $date->setTimezone($utc);

        return new self($date);
    }

    /**
     * @param string $dateTime
     * @param DateTimeZone|null $timeZone Timezone, the dateTime given is in. null assumes the given date is in UTC
     * @return static
     */
    public static function parse(string $dateTime, ?DateTimeZone $timeZone = null): self
    {
        $utc  = new DateTimeZone('UTC');

        if ($timeZone === null) {
            $timeZone = $utc;
        }

        $date = new DateTimeImmutable($dateTime, $timeZone);
        $date = $date->setTimezone($utc);

        return new self($date);
    }

    public function format(string $format = self::FORMAT_HUMAN): string
    {
        return $this->date->format($format);
    }

    public function getTimestamp(): int
    {
        return $this->date->getTimestamp();
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function isToday(): bool
    {
        $today = self::now()->format('Y-m-d');
        return $this->format('Y-m-d') === $today;
    }

    public function toString(): string
    {
        return $this->format(DATE_ATOM);
    }

    public function __toString(): string
    {
        return $this->format(DATE_ATOM);
    }

    public function jsonSerialize(): string
    {
        return $this->format(DATE_ATOM);
    }
}
