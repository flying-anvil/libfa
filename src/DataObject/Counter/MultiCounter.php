<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Counter;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Generator;
use IteratorAggregate;

class MultiCounter implements DataObject, Countable, IteratorAggregate
{
    /** @var int[] */
    private array $counts;

    private function __construct(array $counts)
    {
        $this->counts = $counts;
    }

    public static function createEmpty(): self
    {
        return new self([]);
    }

    public static function createWithInitialCounts(array $counts): self
    {
        return new self($counts);
    }

    public function increment($key, int $amount = 1): void
    {
        if (!isset($this->counts[$key])) {
            $this->counts[$key] = 0;
        }

        $this->counts[$key] += $amount;
    }

    public function decrement($key, int $amount = 1): void
    {
        if (!isset($this->counts[$key])) {
            $this->counts[$key] = 0;
        }

        $this->counts[$key] -= $amount;
    }

    public function reset($key): void
    {
        $this->counts[$key] = 0;
    }

    public function get($key): int
    {
        return $this->counts[$key] ?? 0;
    }

    public function toArray(): array
    {
        return $this->counts;
    }

    public function jsonSerialize(): array
    {
        return $this->counts;
    }

    public function count(): int
    {
        return array_sum($this->counts);
    }

    public function getIterator(): Generator
    {
        yield from $this->counts;
    }
}
