<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Counter;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class Counter implements Stringable, DataObject, Countable
{
    private function __construct(private int $count) {}

    public static function create(int $initialCount = 0): self
    {
        return new self($initialCount);
    }

    public function increment(int $amount = 1): void
    {
        $this->count += $amount;
    }

    public function decrement(int $amount = 1): void
    {
        $this->count -= $amount;
    }

    public function __toString(): string
    {
        return (string)$this->count;
    }

    public function jsonSerialize(): int
    {
        return $this->count;
    }

    public function count(): int
    {
        return $this->count;
    }
}
