<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\String;

use BadMethodCallException;
use FlyingAnvil\Libfa\Conversion\StringValue;
use Stringable;
use FlyingAnvil\Libfa\DataObject\DataObject;

final class TemplateString implements DataObject, Stringable, StringValue
{
    private function __construct(protected string $template) {}

    public static function create(string $template): self
    {
        return new self($template);
    }

    public function render(array $variables): string
    {
        $output = $this->renderPartial($variables);

        if (preg_match('/{([a-zA-Z_0-9]+)}/', $output, $matches)) {
            throw new BadMethodCallException(sprintf(
                'Named parameter "%s" has no value',
                $matches[1],
            ));
        }

        return $output;
    }

    public function renderPartial(array $variables): string
    {
        $search  = [];
        $replace = [];

        foreach ($variables as $key => $value) {
            $search[]  = '{' . $key . '}';
            $replace[] = (string)$value;
        }

        return str_replace($search, $replace, $this->template);
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function concat(string $more): self
    {
        return new self($this->template . $more);
    }

    public function __toString(): string
    {
        return $this->template;
    }

    public function toString(): string
    {
        return $this->template;
    }

    public function jsonSerialize(): string
    {
        return $this->template;
    }
}
