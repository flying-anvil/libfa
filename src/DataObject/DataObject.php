<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject;

use JsonSerializable;

interface DataObject extends JsonSerializable
{
}
