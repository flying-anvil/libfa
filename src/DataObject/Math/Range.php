<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use FlyingAnvil\Libfa\Conversion\StringValue;
use Stringable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use TypeError;

class Range implements DataObject, Stringable, StringValue
{
    protected function __construct(
        protected float|int $lowerBound,
        protected float|int $upperBound,
        protected bool $lowerInclusive = true,
        protected bool $upperInclusive = true
    ) {
        if (!is_int($lowerBound) && !is_float($lowerBound)) {
            throw new TypeError(sprintf(
                'Type of parameter $lowerBound must be float or int, %s given',
                gettype($lowerBound),
            ));
        }

        if (!is_int($upperBound) && !is_float($upperBound)) {
            throw new TypeError(sprintf(
                'Type of parameter $upperBound must be float or int, %s given',
                gettype($upperBound),
            ));
        }

        if ($lowerBound > $upperBound) {
            throw new ValueException(sprintf(
                'Lower bound (%s) must not be above upper bound (%s)',
                $lowerBound,
                $upperBound,
            ));
        }
    }

    public static function create(
        float|int $lowerBound,
        float|int $upperBound,
        bool $lowerInclusive = true,
        bool $upperInclusive = true
    ): self {
        return new self($lowerBound, $upperBound, $lowerInclusive, $upperInclusive);
    }

    public function check(float $value): bool
    {
        $matchesLower = $this->lowerInclusive
            ? $value >= $this->lowerBound
            : $value > $this->lowerBound;

        $matchesUpper = $this->upperInclusive
            ? $value <= $this->upperBound
            : $value < $this->upperBound;

        return $matchesLower && $matchesUpper;
    }

    public function getLowerBound(): float|int
    {
        return $this->lowerBound;
    }

    public function getUpperBound(): float|int
    {
        return $this->upperBound;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return sprintf(
            '%s%s,%s%s',
            $this->lowerInclusive ? '[' : '(',
            $this->lowerBound,
            $this->upperBound,
            $this->upperInclusive ? ']' : ')',
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'lowerBound' => $this->lowerBound,
            'upperBound' => $this->upperBound,
        ];
    }
}
