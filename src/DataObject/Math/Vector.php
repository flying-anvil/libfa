<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

abstract class Vector
{
    abstract public function getMagnitude(): float;
    abstract public function getMagnitudeSquare(): float;
    abstract public function setMagnitude(float $newMagnitude): static;

    abstract public function normalized(): static;

//    abstract public static function angle(Vector $left, Vector $right): float;
}
