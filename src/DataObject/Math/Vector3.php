<?php /** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\Math\Math;
use Stringable;

class Vector3 extends Vector implements DataObject, StringValue, Stringable
{
    private function __construct(
        protected int|float $x,
        protected int|float $y,
        protected int|float $z,
    ) {}

    public static function create(int|float $x, int|float $y, int|float $z): self
    {
        return new self($x, $y, $z);
    }

    public function getMagnitude(): float
    {
        return $this->getMagnitudeSquare() ** .5;
    }

    public function getMagnitudeSquare(): float
    {
        return $this->x * $this->x + $this->y * $this->y + $this->z * $this->z;
    }

    public function setMagnitude(float $newMagnitude): static
    {
        $magnitude = $this->getMagnitude();

        return new self(
            ($this->x / $magnitude) * $newMagnitude,
            ($this->y / $magnitude) * $newMagnitude,
            ($this->z / $magnitude) * $newMagnitude,
        );
    }

    public function normalized(): static
    {
        $magnitude = $this->getMagnitude();

        return self::create(
            $this->x / $magnitude,
            $this->y / $magnitude,
            $this->z / $magnitude,
        );
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public function getZ(): float
    {
        return $this->z;
    }

    // TODO unfinished
    public static function angle(Vector3 $left, Vector3 $right): float
    {
        $num = ($left->getMagnitudeSquare() * $right->getMagnitudeSquare()) ** .5;

        return acos(Math::clamp(self::dot($left, $right) / $num, -1, 1)) * 57.29578;
    }

    public static function distance(Vector3 $left, Vector3 $right): float
    {
        $leftToRight = self::sub($right, $left);
        return $leftToRight->getMagnitude();
    }

    public static function dot(Vector3 $left, Vector3 $right): float
    {
        return $left->x * $right->x + $left->y * $right->y + $left->z * $right->z;
    }

    public static function lerp(Vector3 $left, Vector3 $right, float $time): self
    {
        return self::create(
            $left->x + ($right->x - $left->x) * $time,
            $left->y + ($right->y - $left->y) * $time,
            $left->z + ($right->z - $left->z) * $time,
        );
    }

    /**
     * Returns a vector that is made from the largest components of two vectors
     */
    public static function max(Vector3 $left, Vector3 $right): self
    {
        return self::create(
            max($left->x, $right->x),
            max($left->y, $right->y),
            max($left->z, $right->z),
        );
    }

    /**
     * Returns a vector that is made from the smallest components of two vectors
     */
    public static function min(Vector3 $left, Vector3 $right): self
    {
        return self::create(
            min($left->x, $right->x),
            min($left->y, $right->y),
            min($left->z, $right->z),
        );
    }

    public static function moveTowards(Vector3 $current, Vector3 $target, float $maxDelta): Vector3
    {
        $diffX = $target->x - $current->x;
        $diffY = $target->y - $current->y;
        $diffZ = $target->z - $current->z;
        $squareDistance = $diffX * $diffX + $diffY * $diffY + $diffZ * $diffZ;

        if ($squareDistance === 0.0 || ($maxDelta >= 0.0 && $squareDistance <= $maxDelta * $maxDelta)) {
            return $target;
        }

        $distance = sqrt($squareDistance);
        return self::create(
            $current->x + $diffX / $distance * $maxDelta,
            $current->y + $diffY / $distance * $maxDelta,
            $current->z + $diffZ / $distance * $maxDelta,
        );
    }

    // TODO return plane instead
//    public static function perpendicular(Vector2 $vector): Vector2
//    {
//        return self::create(
//            -$vector->y,
//            $vector->x,
//        );
//    }

    public static function reflect(Vector3 $direction, Vector3 $reflectionNormal): self
    {
        $num = -2 * self::dot($reflectionNormal, $direction);

        return self::create(
            $num * $reflectionNormal->x + $direction->x,
            $num * $reflectionNormal->y + $direction->y,
            $num * $reflectionNormal->z + $direction->z,
        );
    }

    public static function add(Vector3 $left, Vector3 $right): self
    {
        return self::create(
            $left->x + $right->x,
            $left->y + $right->y,
            $left->z + $right->z,
        );
    }

    public static function sub(Vector3 $left, Vector3 $right): self
    {
        return self::create(
            $left->x - $right->x,
            $left->y - $right->y,
            $left->z - $right->z,
        );
    }

    public static function scale(Vector3 $vector, float $factor): self
    {
        return self::create(
            $vector->x * $factor,
            $vector->y * $factor,
            $vector->z * $factor,
        );
    }

    public static function shorten(Vector3 $vector, float $factor): self
    {
        return self::create(
            $vector->x / $factor,
            $vector->y / $factor,
            $vector->z / $factor,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'x' => $this->x,
            'y' => $this->y,
            'z' => $this->z,
        ];
    }

    public function toString(int $decimalPlaces = -1): string
    {
        if ($decimalPlaces < 0) {
            return sprintf(
                '(%s, %s, %s)',
                $this->x,
                $this->y,
                $this->z,
            );
        }

        $format = sprintf(
            '(%%.%1$df, %%.%1$df, %%.%1$df)',
            $decimalPlaces,
        );

        return sprintf(
            $format,
            $this->x,
            $this->y,
            $this->z,
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
