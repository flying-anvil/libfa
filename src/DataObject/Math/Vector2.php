<?php /** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\Math\Math;

class Vector2 extends Vector implements DataObject
{
    private function __construct(
        protected float $x,
        protected float $y,
    ) {}

    public static function create(float $x, float $y): self
    {
        return new self($x, $y);
    }

    public function getMagnitude(): float
    {
        return $this->getMagnitudeSquare() ** .5;
    }

    public function getMagnitudeSquare(): float
    {
        return $this->x * $this->x + $this->y * $this->y;
    }

    public function setMagnitude(float $newMagnitude): static
    {
        $magnitude = $this->getMagnitude();

        return new self(
            ($this->x / $magnitude) * $newMagnitude,
            ($this->y / $magnitude) * $newMagnitude,
        );
    }

    public function normalize(): void
    {
        $magnitude = $this->getMagnitude();

        $this->x /= $magnitude;
        $this->y /= $magnitude;
    }

    public function normalized(): static
    {
        $magnitude = $this->getMagnitude();

        return self::create(
            $this->x / $magnitude,
            $this->y / $magnitude,
        );
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public static function angle(Vector2 $left, Vector2 $right): float
    {
        $num = ($left->getMagnitudeSquare() * $right->getMagnitudeSquare()) ** .5;

        return acos(Math::clamp(self::dot($left, $right) / $num, -1, 1)) * 57.29578;
    }

    public static function distance(Vector2 $left, Vector2 $right): float
    {
        $leftToRight = self::sub($right, $left);
        return $leftToRight->getMagnitude();
    }

    public static function dot(Vector2 $left, Vector2 $right): float
    {
        return $left->x * $right->x + $left->y * $right->y;
    }

    public static function lerp(Vector2 $left, Vector2 $right, float $time): self
    {
        return self::create(
            $left->x + ($right->x - $left->x) * $time,
            $left->y + ($right->y - $left->y) * $time,
        );
    }

    /**
     * Returns a vector that is made from the largest components of two vectors
     */
    public static function max(Vector2 $left, Vector2 $right): self
    {
        return self::create(
            max($left->x, $right->x),
            max($left->y, $right->y),
        );
    }

    /**
     * Returns a vector that is made from the smallest components of two vectors
     */
    public static function min(Vector2 $left, Vector2 $right): self
    {
        return self::create(
            min($left->x, $right->x),
            min($left->y, $right->y),
        );
    }

    public static function moveTowards(Vector2 $current, Vector2 $target, float $maxDelta): Vector2
    {
        $diffX = $target->x - $current->x;
        $diffY = $target->y - $current->y;
        $squareDistance = $diffX * $diffX + $diffY * $diffY;

        if ($squareDistance === 0.0 || ($maxDelta >= 0.0 && $squareDistance <= $maxDelta * $maxDelta)) {
            return $target;
        }

        $distance = sqrt($squareDistance);
        return self::create(
            $current->x + $diffX / $distance * $maxDelta,
            $current->y + $diffY / $distance * $maxDelta,
        );
    }

    public static function perpendicular(Vector2 $vector): Vector2
    {
        return self::create(
            -$vector->y,
            $vector->x,
        );
    }

    public static function reflect(Vector2 $direction, Vector2 $reflectionNormal): Vector2
    {
        $num = -2 * self::dot($reflectionNormal, $direction);

        return self::create(
            $num * $reflectionNormal->x + $direction->x,
            $num * $reflectionNormal->y + $direction->y,
        );
    }

    public static function add(Vector2 $left, Vector2 $right): Vector2
    {
        return self::create(
            $left->x + $right->x,
            $left->y + $right->y,
        );
    }

    public static function sub(Vector2 $left, Vector2 $right): Vector2
    {
        return self::create(
            $left->x - $right->x,
            $left->y - $right->y,
        );
    }

    public static function scale(Vector2 $vector, float $factor): Vector2
    {
        return self::create(
            $vector->x * $factor,
            $vector->y * $factor,
        );
    }

    public static function shorten(Vector2 $vector, float $factor): Vector2
    {
        return self::create(
            $vector->x / $factor,
            $vector->y / $factor,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'x' => $this->x,
            'y' => $this->y,
        ];
    }
}
