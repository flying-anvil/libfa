<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math\Physic;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class Frequency implements DataObject, StringValue, Stringable
{
    protected function __construct(protected float $frequency) {}

    public static function create(float $frequency): self
    {
        return new self($frequency);
    }

    public function getValue(): float
    {
        return $this->frequency;
    }

    public function toPeriod(): Period
    {
        return Period::create(1 / $this->frequency);
    }

    public function jsonSerialize(): float
    {
        return $this->frequency;
    }

    public function toString(): string
    {
        return (string)$this->frequency;
    }

    public function __toString(): string
    {
        return (string)$this->frequency;
    }
}
