<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math\Physic;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class Amplitude implements DataObject, StringValue, Stringable
{
    protected function __construct(protected float $amplitude) {}

    public static function create(float $amplitude): self
    {
        return new self($amplitude);
    }

    public function getValue(): float
    {
        return $this->amplitude;
    }

    public function jsonSerialize(): float
    {
        return $this->amplitude;
    }

    public function toString(): string
    {
        return (string)$this->amplitude;
    }

    public function __toString(): string
    {
        return (string)$this->amplitude;
    }
}
