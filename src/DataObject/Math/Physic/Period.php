<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math\Physic;

use Stringable;
use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;

class Period implements DataObject, StringValue, Stringable
{
    protected function __construct(protected float $period) {}

    public static function create(float $period): self
    {
        return new self($period);
    }

    public function getPeriod(): float
    {
        return $this->period;
    }

    public function toFrequency(): Frequency
    {
        return Frequency::create(1 / $this->period);
    }

    public function jsonSerialize(): float
    {
        return $this->period;
    }

    public function toString(): string
    {
        return (string)$this->period;
    }

    public function __toString(): string
    {
        return (string)$this->period;
    }
}
