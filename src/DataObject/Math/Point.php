<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class Point implements DataObject, StringValue, Stringable
{
    protected function __construct(
        protected float $x,
        protected float $y,
    ) {}

    public static function create(float $x, float $y): self
    {
        return new self($x, $y);
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public function toString(): string
    {
        return sprintf(
            '(%s,%s)',
            $this->x,
            $this->y,
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function jsonSerialize(): array
    {
        return [
            'x' => $this->x,
            'y' => $this->y,
        ];
    }
}
