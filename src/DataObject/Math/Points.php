<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Generator;
use IteratorAggregate;

class Points implements DataObject, Countable, IteratorAggregate
{
    /** @var Point[] */
    private array $points = [];
    private int $count = 0;

    protected function __construct() {}

    public static function createEmpty(): self
    {
        return new self();
    }

    public function addPoint(Point $point): self
    {
        $id = spl_object_id($point);

        if (!isset($this->points[$id])) {
            $this->points[$id] = $point;
            $this->count++;
        }

        return $this;
    }

    public function removePoint(Point $point): self
    {
        $id = spl_object_id($point);

        if (isset($this->points[$id])) {
            unset($this->points[$id]);
            $this->count--;
        }

        return $this;
    }

    public function sumX(): float
    {
        $sum = 0.0;

        foreach ($this->points as $point) {
            $sum += $point->getX();
        }

        return $sum;
    }

    public function sumY(): float
    {
        $sum = 0.0;

        foreach ($this->points as $point) {
            $sum += $point->getY();
        }

        return $sum;
    }

    public function averageX(): float
    {
        return $this->sumX() / $this->count;
    }

    public function averageY(): float
    {
        return $this->sumY() / $this->count;
    }

    /**
     * @return Generator<Point> | Point[]
     */
    public function getIterator(): Generator
    {
        yield from $this->points;
    }

    public function count(): int
    {
        return $this->count;
    }

    public function jsonSerialize(): array
    {
        return $this->points;
    }
}
