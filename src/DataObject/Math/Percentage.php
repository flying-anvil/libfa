<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Math;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

class Percentage implements DataObject, StringValue, Stringable
{
    private function __construct(private float $percentage) {}

    public static function createFromFloat(float $percentage): self
    {
        return new self($percentage);
    }

    /**
     * @param string $rawPercentage Example: 64.32 % | 13.3% | 100%
     * @return static
     */
    public static function createFromString(string $rawPercentage): self
    {
        preg_match('#(\d+\.?\d*) ?%#', $rawPercentage, $matches);

        [, $progress] = $matches;
        return new self((float)$progress * .01);
    }

    public function getValue(): float
    {
        return $this->percentage;
    }

    public function __toString(): string
    {
        return sprintf(
            '%.2f',
            $this->percentage,
        );
    }

    public function toString(): string
    {
        return sprintf(
            '%.2f',
            $this->percentage,
        );
    }

    public function jsonSerialize(): float
    {
        return $this->percentage;
    }
}
