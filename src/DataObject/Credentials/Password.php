<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Credentials;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

final class Password implements DataObject, StringValue, Stringable
{
    private function __construct(
        private string $hash,
        private string $pepper = '',
    ) {}

    public static function createFromHash(string $hash, string $pepper = ''): self
    {
        return new self($hash, $pepper);
    }

    public static function createFromRaw(string $rawPassword, string $algorithm = PASSWORD_DEFAULT, string $pepper = '', array $options = []): self
    {
        return new self(
            password_hash($rawPassword . $pepper, $algorithm, $options),
            $pepper,
        );
    }

    public function isValid(string $password): bool
    {
        return password_verify($password . $this->pepper, $this->hash);
    }

    public function getInfo(): array
    {
        return password_get_info($this->hash);
    }

    public function needsRehash(string $algorithm, array $options = []): bool
    {
        return password_needs_rehash($this->hash, $algorithm, $options);
    }

    public function __toString(): string
    {
        return $this->hash;
    }

    public function toString(): string
    {
        return $this->hash;
    }

    public function jsonSerialize(): string
    {
        return $this->hash;
    }
}
