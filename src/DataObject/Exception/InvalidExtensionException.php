<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Exception;

use FlyingAnvil\Libfa\Exception\LibfaException;

class InvalidExtensionException extends LibfaException
{
}
