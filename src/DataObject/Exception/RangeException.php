<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\DataObject\Exception;

class RangeException extends ValueException
{
}
