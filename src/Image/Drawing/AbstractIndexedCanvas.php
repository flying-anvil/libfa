<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing;

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalette;
use FlyingAnvil\Libfa\DataObject\Math\Range;

abstract class AbstractIndexedCanvas extends AbstractBaseCanvas
{
    public function setPixel(int $x, int $y, int $colorIndex): void
    {
        $index = $this->get1DIndex($x, $y);
        $this->pixels[$index] = $colorIndex;
    }

    public function getPixel(int $x, int $y): int
    {
        $index = $this->get1DIndex($x, $y);
        return $this->pixels[$index] ?? 0;
    }

    # region drawing functions

    public function fill(int $colorIndex): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = $colorIndex;
            }
        }
    }

    public function fillRandom(Range $restrictIndices): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = random_int($restrictIndices->getLowerBound(), $restrictIndices->getUpperBound());
            }
        }
    }

    public function fillRandomFromPalette(IndexedColorPalette $palette): void
    {
        $colorCount = $palette->count() - 1;

        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = random_int(0, $colorCount);
            }
        }
    }

    /**
     * @param callable $generator Callable ist given $x, $y, $resolutionX and $resolutionY
     * <br/> It serves a a "shader" and returns a int as the color index
     */
    public function fillPattern(callable $generator): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = $generator($x, $y, $this->resolutionX, $this->resolutionY);
            }
        }
    }

    public function drawRect(
        int $fromX,
        int $fromY,
        int $toX,
        int $toY,
        ?int $borderColorIndex = null,
        ?int $fillColorIndex = null,
        bool $fillWithTransparency = false
    ): void {
        for ($y = $fromY; $y <= $toY; $y++) {
            for ($x = $fromX; $x <= $toX; $x++) {
                if ($x === $fromX || $x === $toX || $y === $fromY || $y === $toY) {
                    $this->setPixel($x, $y, $borderColorIndex);
                    continue;
                }

                if ($fillWithTransparency) {
                    $this->setPixel($x, $y, $fillColorIndex);
                }
            }
        }
    }

    # endregion

    abstract public function draw(IndexedColorPalette $palette): void;
    abstract public function drawSmall(IndexedColorPalette $palette): void;

    public function toString(IndexedColorPalette $palette): string
    {
        ob_start();
        $this->draw($palette);
        return ob_get_clean();
    }

    public function __toString(): string
    {
        $map = '';
        $longestIndex = 1;

        foreach ($this->pixels as $colorIndex) {
            $length = strlen((string)$colorIndex);
            if ($length > $longestIndex) {
                $longestIndex = $length;
            }
        }

        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $pixelIndex = $this->get1DIndex($x, $y);
                $colorIndex = $this->pixels[$pixelIndex] ?? 0;

                $map .= str_pad((string)$colorIndex, $longestIndex, ' ', STR_PAD_LEFT);
                if ($x !== $this->resolutionX) {
                    $map .= ' ';
                }
            }

            $map .= PHP_EOL;
        }

        return $map;
    }
}
