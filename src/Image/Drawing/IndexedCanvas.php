<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing;

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalette;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\SmallColorBlockOutput;

class IndexedCanvas extends AbstractIndexedCanvas
{
    public function draw(IndexedColorPalette $palette): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            echo $this->getDrawnLine($y, $palette);
            echo PHP_EOL;
        }
    }

    public function getDrawnLine(int $line, IndexedColorPalette $palette, bool $indexZeroAsTransparency = false): string
    {
        $drawnLine = '';

        for ($x = 0; $x < $this->resolutionX; $x++) {
            $pixelIndex = $this->get1DIndex($x, $line);
            $colorIndex = $this->pixels[$pixelIndex] ?? 0;

            if ($colorIndex === 0 && $indexZeroAsTransparency) {
                $drawnLine .= '  ';
                continue;
            }

            $color      = $palette->getColorByIndex($colorIndex);
            $drawnLine .= ColorBlockOutput::create($color);
        }

        return $drawnLine;
    }

    public function drawSmall(IndexedColorPalette $palette): void
    {
        for ($y = 0; $y < $this->resolutionY; $y += 2) {
            echo $this->getDrawnLineSmall($y, $palette);
            echo PHP_EOL;
        }
    }

    public function getDrawnLineSmall(int $line, IndexedColorPalette $palette, bool $indexZeroAsTransparency = false): string
    {
        $drawnLine = '';

        for ($x = 0; $x < $this->resolutionX; $x++) {
            $colorTopIndex    = $this->pixels[$this->get1DIndex($x, $line)] ?? 0;
            $colorBottomIndex = $this->getBottomColor($x, $line);

            $colorTop    = $palette->getColorByIndex($colorTopIndex);
            $colorBottom = $colorBottomIndex !== null
                ? $palette->getColorByIndex($colorBottomIndex)
                : null;

            if ($colorTopIndex === 0 && $indexZeroAsTransparency) {
                $colorTop = null;
            }

            if ($colorBottomIndex === 0 && $indexZeroAsTransparency) {
                $colorBottom = null;
            }

            $drawnLine .= SmallColorBlockOutput::create($colorTop, $colorBottom);
        }

        return $drawnLine;
    }

    private function getBottomColor(int $x, int $y): ?int
    {
        if ($y + 1 >= $this->resolutionY) {
            return null;
        }

        return $this->pixels[$this->get1DIndex($x, $y + 1)] ?? 0;
    }
}
