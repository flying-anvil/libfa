<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing;

use Stringable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use SplFixedArray;

abstract class AbstractBaseCanvas implements DataObject, Stringable
{
    protected SplFixedArray $pixels;

    protected function __construct(
        protected int $resolutionX,
        protected int $resolutionY)
    {
        $this->pixels = new SplFixedArray($resolutionX * $resolutionY);
    }

    /**
     * @param int $resolutionX
     * @param int $resolutionY
     * @return static
     */
    public static function create(int $resolutionX, int $resolutionY): static
    {
        return new static($resolutionX, $resolutionY);
    }

    public function clear(): void
    {
        $this->pixels = new SplFixedArray($this->resolutionX * $this->resolutionY);
    }

    protected function get1DIndex(int $x, int $y): int
    {
        return $this->resolutionX * $y + $x;
    }

    public function getResolutionX(): int
    {
        return $this->resolutionX;
    }

    public function getResolutionY(): int
    {
        return $this->resolutionY;
    }

    public function jsonSerialize(): SplFixedArray
    {
        return $this->pixels;
    }
}
