<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing\Tile;

use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalette;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;

class TileMap
{
    /** @var Tile[] */
    private array $tiles = [];

    private Tile $blankTile;

    private function __construct(
        private int $mapSizeX,
        private int $mapSizeY,
        private int $tileSizeX,
        private int $tileSizeY,
    ) {
        $this->blankTile = $this->createBlankTile();
    }

    public static function create(int $mapSizeX, int $mapSizeY, int $tileSizeX, int $tileSizeY): self
    {
        return new self($mapSizeX, $mapSizeY, $tileSizeX, $tileSizeY);
    }

    public static function createFromTiles(Tiles $tiles, int $mapSizeX, int $mapSizeY, int $tileSizeX, int $tileSizeY): self
    {
        $instance = new self($mapSizeX, $mapSizeY, $tileSizeX, $tileSizeY);

        foreach ($tiles as $tile) {
            $instance->addTile($tile);
        }

        return $instance;
    }

    public static function createFromTilesGuessSize(Tiles $tiles, int $tileSizeX, int $tileSizeY): self
    {
        $guessedSize = self::guessMapSize($tiles);
        $instance = new self($guessedSize, $guessedSize, $tileSizeX, $tileSizeY);

        foreach ($tiles as $tile) {
            $instance->addTile($tile);
        }

        return $instance;
    }

    public function addTile(Tile $tile): void
    {
        if ($tile->getResolutionX() !== $this->tileSizeX || $tile->getResolutionY() !== $this->tileSizeY) {
            throw new ValueException(sprintf(
                'All tiles in this map must be of same size (%dx%d)',
                $this->tileSizeX,
                $this->tileSizeY,
            ));
        }

        $this->tiles[] = $tile;
    }

    public function draw(IndexedColorPalette $palette, bool $indexZeroAsTransparency = false): void
    {
        echo implode(PHP_EOL, $this->getLargeDrawingLines($palette, $indexZeroAsTransparency));
    }

    public function drawSmall(IndexedColorPalette $palette, bool $indexZeroAsTransparency = false): void
    {
        echo implode(PHP_EOL, $this->getSmallDrawingLines($palette, $indexZeroAsTransparency));
    }

    protected function getLargeDrawingLines(IndexedColorPalette $palette, bool $indexZeroAsTransparency): array
    {
        $lines = [];

        for ($verticalTile = 0; $verticalTile < $this->mapSizeY; $verticalTile++) {
            for ($lineInTile = 0; $lineInTile < $this->tileSizeY; $lineInTile++) {
                $line = '';
                for ($horizontalTile = 0; $horizontalTile < $this->mapSizeX; $horizontalTile++) {
                    $tile  = $this->tiles[$this->get1DIndexTile($horizontalTile, $verticalTile)] ?? $this->blankTile;
                    $line .= $tile->getDrawnLine($lineInTile, $palette, $indexZeroAsTransparency);
                }

                $lines[] = $line;
            }
        }

        return $lines;
    }

    protected function getSmallDrawingLines(IndexedColorPalette $palette, bool $indexZeroAsTransparency): array
    {
        $lines = [];

        for ($verticalTile = 0; $verticalTile < $this->mapSizeY; $verticalTile++) {
            for ($lineInTile = 0; $lineInTile < $this->tileSizeY; $lineInTile += 2) {
                $line = '';
                for ($horizontalTile = 0; $horizontalTile < $this->mapSizeX; $horizontalTile++) {
                    $tile  = $this->tiles[$this->get1DIndexTile($horizontalTile, $verticalTile)] ?? $this->blankTile;
                    $line .= $tile->getDrawnLineSmall($lineInTile, $palette, $indexZeroAsTransparency);
                }

                $lines[] = $line;
            }
        }

        return $lines;
  }

    private function get1DIndexTile(int $x, int $y): int
    {
        return $this->mapSizeX * $y + $x;
    }

    private function createBlankTile(): Tile
    {
        return Tile::create($this->tileSizeX, $this->tileSizeY);
    }

    private static function guessMapSize(Tiles $tiles): int
    {
        $root = $tiles->count() ** .5;

        if (floor($root) === $root) {
            return (int)$root;
        }

        throw new \Exception(sprintf(
            "Could not guess dimension for %d tiles",
            $tiles->count(),
        ));
    }
}
