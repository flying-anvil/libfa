<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing\Tile;

use Countable;
use Generator;
use IteratorAggregate;

class Tiles implements IteratorAggregate, Countable
{
    /** @var Tile[] */
    private $tiles = [];

    /** @var int */
    private $count = 0;

    private function __construct()
    {
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public function addTile(Tile $tile): void
    {
        $this->tiles[] = $tile;
        $this->count++;
    }

    /**
     * @return Tile[]
     */
    public function getTiles(): array
    {
        return $this->tiles;
    }

    /**
     * @return Generator | Tile[]
     */
    public function getIterator(): Generator
    {
        yield from $this->tiles;
    }

    public function count(): int
    {
        return $this->count;
    }
}
