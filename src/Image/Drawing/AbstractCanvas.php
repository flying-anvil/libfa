<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing;

use Stringable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use SplFixedArray;

abstract class AbstractCanvas implements DataObject, Stringable
{
    protected SplFixedArray $pixels;

    protected function __construct(
        protected int $resolutionX,
        protected int $resolutionY,
    ) {
        $this->pixels = new SplFixedArray($resolutionX * $resolutionY);
    }

    public static function create(int $resolutionX, int $resolutionY): static
    {
        return new static($resolutionX, $resolutionY);
    }

    public function clear(): void
    {
        $this->pixels = new SplFixedArray($this->resolutionX * $this->resolutionY);
    }

    public function setPixel(int $x, int $y, ?Color $color = null): void
    {
        $index = $this->get1DIndex($x, $y);
        $this->pixels[$index] = $color;
    }

    public function getPixel(int $x, int $y): ?Color
    {
        $index = $this->get1DIndex($x, $y);
        return $this->pixels[$index];
    }

    # region drawing functions

    public function fill(?Color $color): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = $color;
            }
        }
    }

    public function fillRandom(?Range $restrictRed = null, ?Range $restrictGreen = null, ?Range $restrictBlue = null): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = Color::createRestrictedRandom($restrictRed, $restrictGreen, $restrictBlue);
            }
        }
    }

    /**
     * @param callable $generator Callable ist given $x, $y, $resolutionX and $resolutionY
     * <br/> It serves a a "shader" and returns a Color
     */
    public function fillPattern(callable $generator): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $this->pixels[$index] = $generator($x, $y, $this->resolutionX, $this->resolutionY);
            }
        }
    }

    public function drawRect(
        int $fromX,
        int $fromY,
        int $toX,
        int $toY,
        ?Color $borderColor = null,
        ?Color $fillColor = null,
        bool $fillWithTransparency = false
    ): void {
        for ($y = $fromY; $y <= $toY; $y++) {
            for ($x = $fromX; $x <= $toX; $x++) {
                if ($x === $fromX || $x === $toX || $y === $fromY || $y === $toY) {
                    $this->setPixel($x, $y, $borderColor);
                    continue;
                }

                if ($fillWithTransparency) {
                    $this->setPixel($x, $y, $fillColor);
                }
            }
        }
    }

    # endregion

    protected function get1DIndex(int $x, int $y): int
    {
        return $this->resolutionX * $y + $x;
    }

    abstract public function draw(): void;
    abstract public function drawSmall(): void;

    public function __toString(): string
    {
        ob_start();
        $this->draw();
        return ob_get_clean();
    }

    public function jsonSerialize(): SplFixedArray
    {
        return $this->pixels;
    }
}
