<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Image\Drawing;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\SmallColorBlockOutput;

class Canvas extends AbstractCanvas
{
    public function draw(): void
    {
        for ($y = 0; $y < $this->resolutionY; $y++) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $index = $this->get1DIndex($x, $y);
                $color = $this->pixels[$index];

                if ($color === null) {
                    echo '  ';
                    continue;
                }

                echo ColorBlockOutput::create($color);
            }

            echo PHP_EOL;
        }
    }

    public function drawSmall(): void
    {
        for ($y = 0; $y < $this->resolutionY; $y += 2) {
            for ($x = 0; $x < $this->resolutionX; $x++) {
                $colorTop    = $this->pixels[$this->get1DIndex($x, $y)];
                $colorBottom = $this->getBottomColor($x, $y);

                echo SmallColorBlockOutput::create($colorTop, $colorBottom);
            }

            echo PHP_EOL;
        }
    }

    private function getBottomColor(int $x, int $y): ?Color
    {
        if ($y + 1 >= $this->resolutionY) {
            return null;
        }

        return $this->pixels[$this->get1DIndex($x, $y + 1)];
    }
}
