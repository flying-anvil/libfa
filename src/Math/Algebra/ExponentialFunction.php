<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math\Algebra;

class ExponentialFunction implements MathFunction
{
    protected function __construct(
        protected float|int $exponent,
    ) {}

    public static function create(float|int $exponent): self
    {
        return new self($exponent);
    }

    public function evaluate(float|int $x): float|int
    {
        // x ^ y
        return $x ** $this->exponent;
    }
}
