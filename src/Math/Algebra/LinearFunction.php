<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math\Algebra;

class LinearFunction implements MathFunction
{
    protected function __construct(
        protected float|int $slope,
        protected float|int $intercept,
    ) {}

    public static function create(float|int $slope, float|int $intercept): self
    {
        return new self($slope, $intercept);
    }

    public function evaluate(float|int $x): float|int
    {
        // 1.5x + 2
        return $this->slope * $x + $this->intercept;
    }
}
