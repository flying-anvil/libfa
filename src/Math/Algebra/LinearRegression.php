<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math\Algebra;

use FlyingAnvil\Libfa\DataObject\Math\Points;

class LinearRegression
{
    public function calculateTrend(Points $points): LinearFunction
    {
        return $this->calculateLinearRegression($points);
    }

    public function calculateNextValue(Points $points, float|int $nextX): float|int
    {
        return $this->calculateLinearRegression($points)->evaluate($nextX);
    }

    private function calculateLinearRegression(Points $points): LinearFunction
    {
        $xAverage = $points->averageX();
        $yAverage = $points->averageY();

        $numeratorSum   = 0;
        $denominatorSum = 0;

        foreach ($points as $point) {
            $xMinusAverage = $point->getX() - $xAverage;
            $yMinusAverage = $point->getY() - $yAverage;

            $numeratorSum   += $xMinusAverage * $yMinusAverage;
            $denominatorSum += $xMinusAverage ** 2;
        }

        $slope     = $numeratorSum / $denominatorSum;
        $intercept = $yAverage - $slope * $xAverage;

        return LinearFunction::create($slope, $intercept);
    }
}
