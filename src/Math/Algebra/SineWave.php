<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math\Algebra;

use FlyingAnvil\Libfa\DataObject\Math\Physic\Amplitude;
use FlyingAnvil\Libfa\DataObject\Math\Physic\Frequency;

class SineWave implements MathFunction
{
    /**
     * @param Frequency|null $frequency defaults to 1
     * @param Amplitude|null $amplitude defaults to 1
     */
    public function __construct(
         protected ?Frequency $frequency,
         protected ?Amplitude $amplitude
    ) {
        $this->frequency = $frequency ?? Frequency::create(1);
        $this->amplitude = $amplitude ?? Amplitude::create(1);
    }

    public function evaluate(float|int $x): float
    {
        return sin($x * $this->frequency->getValue()) * $this->amplitude->getValue();
    }
}
