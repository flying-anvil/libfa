<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math\Algebra;

interface MathFunction
{
    public function evaluate(float|int $x);
}
