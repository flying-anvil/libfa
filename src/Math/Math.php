<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Math;

class Math
{
    public const PI  = M_PI;
    public const TAU = M_PI * 2;

    public static function clamp(float|int $value, float|int $min, float|int $max): float|int
    {
        if ($value < $min) {
            return $min;
        }

        if ($value > $max) {
            return $max;
        }

        return $value;
    }

    public static function clamp01(float|int $value): float|int
    {
        return self::clamp($value, 0, 1);
    }

    public static function lerp(float|int $from, float|int $to, float $percentage): float|int
    {
        return (($to - $from) * $percentage) + $from;
    }

    public static function lerpInt(int $from, int $to, float $percentage): int
    {
        return (int)self::lerp($from, $to, $percentage);
    }

    public static function map(float|int $inFrom, float|int $inTo, float|int $outFrom, float|int $outTo, float|int $value): float|int
    {
        return $outFrom + ((($value - $inFrom) * ($outTo - $outFrom)) / ($inTo - $inFrom));
    }
}
