<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Debug;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\AbstractColorPalette;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalette;
use FlyingAnvil\Libfa\DataObject\Table;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorableOutput;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorBlockOutput;
use FlyingAnvil\Libfa\Utils\Formatter\TableFormatter;

class PalettePrinter
{
    private const BACKGROUND_BLOCK_LENGTH = 12;
    private const DEFAULT_COLORS_PER_ROW  = 16;

    public static function printPalette(AbstractColorPalette $palette): void
    {
        $colors = $palette->getColors();
        $writer = new ConsoleWriter();
        $writer->writeln(sprintf(
                "Palette: %s\nMethod:  %s\nColors:  %d",
                get_class($palette),
                'Foreground + Background',
                count($colors),
            ) . PHP_EOL);

        foreach ($colors as $name => $colorComponents) {
            $color = Color::create(...$colorComponents);
            $writer->writeln(
                ColorableOutput::create(
                    str_repeat(' ', self::BACKGROUND_BLOCK_LENGTH),
                    null,
                    $color,
                ),
                '  ',
                ColorableOutput::create(
                    $name,
                    $color,
                )
            );
        }
    }

    public static function printPaletteTable(AbstractColorPalette $palette): void
    {
        $colors = $palette->getColors();
        $writer = new ConsoleWriter();
        $writer->writeln(sprintf(
            "Palette: %s\nMethod:  %s\nColors:  %d",
            get_class($palette),
            'Table',
            count($colors),
        ) . PHP_EOL);

        $padding = str_repeat(' ', self::BACKGROUND_BLOCK_LENGTH);
        $table   = Table::createEmpty([
            'background',
            'foreground',
            'cleanName',
        ]);

        foreach ($colors as $name => $colorComponents) {
            $table->addRows([
                ColorableOutput::create($padding, null, Color::create(...$colorComponents)),
                ColorableOutput::create($name, Color::create(...$colorComponents)),
                $name,
            ]);
        }

        $formatter = new TableFormatter();
        $formatter->setPadding();
        $formatter->printTable($table);
    }

    public static function printPaletteForeground(AbstractColorPalette $palette): void
    {
        $colors = $palette->getColors();
        $writer = new ConsoleWriter();
        $writer->writeln(sprintf(
            "Palette: %s\nMethod:  %s\nColors:  %d",
            get_class($palette),
            'Foreground',
            count($colors),
        ) . PHP_EOL);

        foreach ($colors as $name => $colorComponents) {
            $writer->writeln(ColorableOutput::create(
                $name,
                Color::create(...$colorComponents),
            ));
        }
    }

    public static function printPaletteBackground(AbstractColorPalette $palette): void
    {
        $colors = $palette->getColors();
        $writer = new ConsoleWriter();
        $writer->writeln(sprintf(
            "Palette: %s\nMethod:  %s\nColors:  %d",
            get_class($palette),
            'Background',
            count($colors),
        ) . PHP_EOL);

        $padding = str_repeat(' ', self::BACKGROUND_BLOCK_LENGTH);
        foreach ($colors as $name => $colorComponents) {
            $writer->writeln(ColorableOutput::create(
                $padding,
                null,
                Color::create(...$colorComponents),
            ));
        }
    }

    public static function printPaletteBlock(AbstractColorPalette $palette, int $colorsPerRow = 0): void
    {
        $writer = new ConsoleWriter();
        $colors = $palette->getColors();

        if ($palette instanceof IndexedColorPalette) {
            $colorsPerRow = $palette->count();
        }

        if ($colorsPerRow === 0) {
            $colorsPerRow = self::guessColorsPerRow(count($colors));
        }

        $writer->writeln(sprintf(
            "Palette: %s\nMethod:  %s\nPer Row: %d\nColors:  %d",
            get_class($palette),
            'Block',
            $colorsPerRow,
            count($colors),
        ) . PHP_EOL);

        $i = 0;
        foreach ($colors as $color) {
            if (is_array($color)) {
                $color = Color::create(...$color);
            }

            $writer->write(ColorBlockOutput::create($color));

            if (++$i >= $colorsPerRow) {
                $writer->writeln('');
                $i = 0;
            }
        }
    }

    private static function guessColorsPerRow(int $totalColors): int
    {
        $root = $totalColors ** .5;

        if (floor($root) === $root) {
            return (int)$root;
        }

        return self::DEFAULT_COLORS_PER_ROW;
    }
}
