<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Debug;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorableOutput;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;

class VarDumper
{
    public const TYPE_STRING = 'string';
    public const TYPE_BOOL   = 'bool';
    public const TYPE_INT    = 'int';
    public const TYPE_FLOAT  = 'float';
    public const TYPE_ARRAY  = 'array';
    public const TYPE_OBJECT = 'object';

    public const TYPES = [
        self::TYPE_STRING,
        self::TYPE_BOOL,
        self::TYPE_INT,
        self::TYPE_FLOAT,
        self::TYPE_ARRAY,
        self::TYPE_OBJECT,
    ];

    public const DEFAULT_COLOR_STRING      = 'C00';
    public const DEFAULT_COLOR_BOOL        = '75507B';
    public const DEFAULT_COLOR_INT         = '4E9A06';
    public const DEFAULT_COLOR_FLOAT       = 'F57900';
    public const DEFAULT_COLOR_ARRAY_ARROW = '888A85';

    public static function dump(mixed $value): void
    {
        echo PHP_EOL;
        var_dump($value);

        if (!extension_loaded('xdebug')) {
            self::dumpVanilla($value);
            return;
        }

        $dump = self::getDump($value);
        echo implode(PHP_EOL, $dump);
    }

    private static function dumpVanilla(mixed $var): void
    {
        $dump  = self::getDump($var);

        foreach ($dump as $line) {
            if ($line === '') {
                continue;
            }

            $pattern = '/\s*(%s)\(([^\)]+)\)/';
            $pattern = sprintf(
                $pattern,
                implode('|', self::TYPES),
            );

            $matched = preg_match($pattern, $line, $matches);

            if ($matched) {
                [, $type, $value] = $matches;
                switch ($type) {
                    case self::TYPE_STRING:
                        self::formatString($line, (int)$value);
                        continue 2;
                    case self::TYPE_BOOL:
                        self::formatBool($value);
                        continue 2;
                    case self::TYPE_INT:
                        self::formatInt($value);
                        continue 2;
                    case self::TYPE_FLOAT:
                        self::formatFloat($value);
                        continue 2;
                    case self::TYPE_ARRAY:
                        self::formatArrayInitializer($value);
                        continue 2;
                    case self::TYPE_OBJECT:
                        self::formatObjectInitializer($line, $value);
                        continue 2;
                }

                var_dump($var);
            }

            if (self::probeArrayValue($line)) {
                continue;
            }

            if (ltrim($line) === '}') {
                continue;
            }
//            var_dump($match, $type, $value);

            echo $line, PHP_EOL;
            die;
        }

//        die;
    }

    private static function getDump(mixed $value): array
    {
        ob_start();
        var_dump($value);
        return explode(PHP_EOL, ob_get_clean());
    }

    private static function formatString(string $line, int $length): void
    {
        $value = substr($line, strpos($line, '"') + 1, -1);
        $value = DecorableOutput::create('\'' . $value . '\'')
            ->setForegroundColor(Color::createFromHexString(self::DEFAULT_COLOR_STRING));

        echo sprintf(
            '%s %s (length=%d)',
            self::TYPE_STRING,
            $value,
            $length,
        ), PHP_EOL;
    }

    private static function formatBool(string $value): void
    {
        $output = DecorableOutput::create($value)
            ->setForegroundColor(Color::createFromHexString(self::DEFAULT_COLOR_BOOL));

        echo self::TYPE_BOOL, ' ', $output, PHP_EOL;
    }

    private static function formatInt(string $value): void
    {
        $output = DecorableOutput::create($value)
            ->setForegroundColor(Color::createFromHexString(self::DEFAULT_COLOR_INT));

        echo self::TYPE_INT, ' ', $output, PHP_EOL;
    }

    private static function formatFloat($value): void
    {
        $output = DecorableOutput::create($value)
            ->setForegroundColor(Color::createFromHexString(self::DEFAULT_COLOR_FLOAT));

        echo self::TYPE_FLOAT, ' ', $output, PHP_EOL;
    }

    private static function formatArrayInitializer(string $size): void
    {
        $rawSize = sprintf(
            '(size=%d)',
            $size,
        );

        $type = DecorableOutput::create(self::TYPE_ARRAY)
            ->setBold();

        $formattedSize = DecorableOutput::create($rawSize)
            ->setItalic();

        echo $type, ' ', $formattedSize, PHP_EOL;
    }

    private static function formatObjectInitializer(string $line, string $value): void
    {
        $type = DecorableOutput::create(self::TYPE_OBJECT)
            ->setBold();

        $class = DecorableOutput::create($value)
            ->setItalic();

        $idPos    = strpos($line, '#');
        $idEndPos = strpos($line, ' ', $idPos);

        $objectId = substr($line, $idPos + 1, $idEndPos - $idPos - 1);

        $formatted = sprintf(
            '%s(%s)[%d]',
            $type,
            $class,
            $objectId,
        );

        echo $formatted, PHP_EOL;
    }

    private static function probeArrayValue(string $line): bool
    {
        $pattern = '/(\s+)\[(.+)\]=>/';
        $matched = preg_match($pattern, $line, $matches);

        if (!$matched) {
            return false;
        }

        [, $spaces, $key] = $matches;
        $key = str_replace('"', '\'', $key);

        $arrow = DecorableOutput::create('=>')
            ->setForegroundColor(Color::createFromHexString(self::DEFAULT_COLOR_ARRAY_ARROW))
            ->setBold();

        echo sprintf(
            '%s%s %s ',
            $spaces,
            $key,
            $arrow,
        );

        return true;
    }
}
