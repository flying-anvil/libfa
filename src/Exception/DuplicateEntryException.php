<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Exception;

class DuplicateEntryException extends LibfaException
{
}
