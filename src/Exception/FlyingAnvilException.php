<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Exception;

use Exception;

class FlyingAnvilException extends Exception
{
}
