<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Conversion;

interface StringValue
{
    public function toString(): string;
}
