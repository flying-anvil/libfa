<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Conversion;

interface Describable
{
    /**
     * @return string A summary of the object as a sentence
     */
    public function describe(): string;
}
