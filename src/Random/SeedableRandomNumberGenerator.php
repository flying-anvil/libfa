<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

interface SeedableRandomNumberGenerator extends RandomNumberGenerator
{
    public function setSeed($seed): void;
    public function reset(): void;
}
