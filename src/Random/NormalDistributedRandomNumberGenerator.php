<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

class NormalDistributedRandomNumberGenerator extends AbstractRandomNumberGeneratorBase
{
    private const MIN = -3;
    private const MAX = 3;

    private bool $gaussianHasPrevious = false;
    private float|int $gaussianPrevious = 0;

    public function __construct(
        protected RandomNumberGenerator $randomNumberGenerator,
    ) {}

    public function generate(): float
    {
        if ($this->gaussianHasPrevious) {
            $y = $this->gaussianPrevious;
            $this->gaussianHasPrevious = false;
            return $y;
        }

        do {
            $x1 = ($this->randomNumberGenerator->generate01() * 2) - 1;
            $x2 = ($this->randomNumberGenerator->generate01() * 2) - 1;
            $w  = $x1 * $x1 + $x2 * $x2;
        } while ($w >= 1);

        $w = (-2 * log($w) / $w) ** .5;
        $y = $x1 * $w;

        $this->gaussianPrevious = $x2 * $w;
        $this->gaussianHasPrevious = true;

        return $y;
    }

    public function generate01(): float
    {
        while (true) {
            // (-3 - -3) / (3 - -3) => 0 / 6 = 0
            // ( 3 - -3) / (3 - -3) => 6 / 6 = 1
            $value = ($this->generate() - self::MIN) / (self::MAX - self::MIN);

            if ($value >= 0 && $value <= 1) {
                return $value;
            }
        }
    }
}
