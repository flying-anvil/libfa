<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

/**
 * Do not use for serious intent by any means!
 * RFC 1149.5 specifies 4 as the standard IEEE-vetted random number.
 * This class is not guaranteed to
 */
class RFC_1149_5_RandomNumberGenerator extends AbstractRandomNumberGeneratorBase implements PseudoRandomNumberGenerator
{
    private const RANDOM_NUMBER_AS_CHOSEN_BY_A_FAIR_DICE_ROLL = 4;

    public function generate(): float|int
    {
        return self::RANDOM_NUMBER_AS_CHOSEN_BY_A_FAIR_DICE_ROLL;
    }

    public function generate01(): float
    {
        return $this->generate01FromMinMax(
            self::RANDOM_NUMBER_AS_CHOSEN_BY_A_FAIR_DICE_ROLL,
            self::RANDOM_NUMBER_AS_CHOSEN_BY_A_FAIR_DICE_ROLL,
        );
    }
}
