<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

interface PseudoRandomNumberGenerator extends RandomNumberGenerator
{
}
