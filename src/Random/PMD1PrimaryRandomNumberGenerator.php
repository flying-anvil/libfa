<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

/**
 * Used for most stuff, like AI or dungeon generation
 */
class PMD1PrimaryRandomNumberGenerator extends AbstractRandomNumberGeneratorBase implements PseudoRandomNumberGenerator, SeedableRandomNumberGenerator
{
    public const MIN = 0;
    public const MAX = 0xFFFF;

    private const MAGIC_NUMBER = 1566083941;

    public function __construct(private int $seed = 0)
    {
        $this->setSeed($seed);
    }

    public function generate(): float|int
    {
        $this->seed = (self::MAGIC_NUMBER * $this->seed + 1) & 0xFFFF_FFFF;
        return $this->seed >> 16;
    }

    public function generate01(): float
    {
        return $this->generate01FromMinMax(self::MIN, self::MAX);
    }

    public function setSeed($seed): void
    {
        // Prevent seed from being 0
        $this->seed = $seed | 1;
    }

    public function reset(): void
    {
        $this->setSeed(0);
    }
}
