<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

use JetBrains\PhpStorm\Deprecated;

/**
 * @deprecated NormalDistributedRandomNumberGenerator
 */
#[Deprecated('Improved naming', NormalDistributedRandomNumberGenerator::class)]
class StandardNormalRandomNumberGenerator extends NormalDistributedRandomNumberGenerator
{
}
