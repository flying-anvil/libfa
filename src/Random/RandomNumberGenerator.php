<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

interface RandomNumberGenerator
{
    public function generate(): float|int;

    public function generateRange(float|int $min, float|int $max): float|int;
    public function generateRangeInt(int $min, int $max): int;

    public function generate01(): float;
}
