<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

/**
 * Used to seed the primary RNG
 */
class PMD1DungeonRandomNumberGenerator extends AbstractRandomNumberGeneratorBase implements PseudoRandomNumberGenerator, SeedableRandomNumberGenerator
{
    public const MIN = 0;
    public const MAX = 0xFF_FFFF;

    private const MAGIC_NUMBER = 1566083941;

    public function __construct(private int $seed = 0)
    {
        $this->setSeed($seed);
    }

    public function generate(): float|int
    {
        $temp = (self::MAGIC_NUMBER * $this->seed + 1) & 0xFFFF_FFFF;
        $this->seed = (self::MAGIC_NUMBER * $temp + 1) & 0xFFFF_FFFF;

        // Why or-ing with 1? That causes every number to be odd… (but it's correct)
        return (($temp >> 16) | $this->seed & 0xFFFF_0000) & 0xFF_FFFF | 1;
    }

    public function generate01(): float
    {
        return $this->generate01FromMinMax(self::MIN, self::MAX);
    }

    public function setSeed($seed): void
    {
        // Prevent seed from being 0 + cut it off
        $this->seed = ($seed | 1) & 0xFF_FFFF;
    }

    public function reset(): void
    {
        $this->setSeed(0);
    }
}
