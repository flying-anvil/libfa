<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

class BuiltinRandomNumberGenerator extends AbstractRandomNumberGeneratorBase implements TrueRandomNumberGenerator
{
    public function generate(): float|int
    {
        return random_int(PHP_INT_MIN, PHP_INT_MAX);
    }

    public function generate01(): float
    {
        return random_int(0, PHP_INT_MAX) / PHP_INT_MAX;
    }
}
