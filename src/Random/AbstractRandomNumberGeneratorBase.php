<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

abstract class AbstractRandomNumberGeneratorBase implements RandomNumberGenerator
{
    public function generateRange(float|int $min, float|int $max): float|int
    {
        // 0, 100 => $diff = 100; $value =  0; 0 + (100 *  0) =>   0
        // 0, 100 => $diff = 100; $value =  1; 0 + (100 *  1) => 100
        // 2,  10 => $diff =   8; $value =  0; 2 + (  8 *  0) =>   2
        // 2,  10 => $diff =   8; $value =  1; 2 + (  8 *  1) =>  10
        // 2,  10 => $diff =   8; $value = .5; 2 + (  8 * .5) =>   6
        $diff  = $max - $min;
        $value = $this->generate01();
        return $min + ($diff * $value);
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     * @note Use with care: min and max are less likely than other values
     */
    public function generateRangeInt(int $min, int $max): int
    {
        // TODO: This causes min and max to be less likely
        return (int)round($this->generateRange($min, $max));

        // TODO: What if returned max is already max, no +1 necessary?
//        return (int)$this->generateRange($min, $max + 1);
    }

    protected function generate01FromMinMax(float|int $min, float|int $max): float|int
    {
        // -10 to 10 | +10 => 0 to 20 | /20 => 01
        //  10 to 20 | -10 => 0 to 10 | /10 => 01
        $value = $this->generate();
        $denominator = $max - $min;

        return ($value - $min) / $denominator;
    }
}
