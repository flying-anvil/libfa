<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random\Exception;

class SeedException extends RandomNumberGenerationException
{
}
