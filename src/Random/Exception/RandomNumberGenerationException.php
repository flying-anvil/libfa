<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random\Exception;

use FlyingAnvil\Libfa\Exception\LibfaException;

class RandomNumberGenerationException extends LibfaException
{
}
