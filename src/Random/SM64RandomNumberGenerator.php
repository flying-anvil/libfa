<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Random;

class SM64RandomNumberGenerator extends AbstractRandomNumberGeneratorBase implements PseudoRandomNumberGenerator, SeedableRandomNumberGenerator
{
    public const MIN = 0;
    public const MAX = 0xFFFF;

    /*
     * If the rng is 0x560A or 0xE550, the cycle is stuck in a two value loop
     * 0x560A creates 0xE550 and vice versa
     */
    private const FAILSAFE = 0x560A;

    private const MASK_LOWER_BYTE = 0x00FF;
    private const MASK_UPPER_BYTE = 0xFF00;

//    /** @var int Default value causes first vale to be 0 */
//    private $seed = 0x54AA;

    private int $seed = 0;

    /**
     * @param bool $prematurelyRepeat The algorithm contains an unnecessary failsafe
     * that repeats the cycle prematurely. Setting this to false works fine,
     * but is no longer true to the original.
     */
    public function __construct(protected bool $prematurelyRepeat = true) {}

    public function generate(): int
    {
        $input = $this->seed;

        if ($input === self::FAILSAFE) {
            $input = 0;
        }

        // Move $input's lower Byte to it's upper and XOR it with $input
        $temp1 = (($input & self::MASK_LOWER_BYTE) << 8) ^ $input;

        // Byte swap
        $input = (($temp1 & self::MASK_LOWER_BYTE) << 8) + (($temp1 & self::MASK_UPPER_BYTE) >> 8);
        $temp1 = (($temp1 & self::MASK_LOWER_BYTE) << 1) ^ $input;
        $temp2 = ($temp1 >> 1) ^ 0xFF80;

        $lowestBit = $temp1 & 1;
        $isEven    = $lowestBit === 0;
        $xorValue  = $isEven
            ? 0x1FF4
            : 0x8180;

        // Prematurely repeat cycle (unnecessary failsafe)
        if ($isEven && $temp2 === 0xAA55 && $this->prematurelyRepeat) {
            $this->seed = 0;
            return 0;
        }

        $input = $temp2 ^ $xorValue;

        $this->seed = $input;
        return $input;
    }

    public function generate01(): float
    {
        $value = $this->generate();
        return $value / self::MAX;
    }

    public function reset(): void
    {
        $this->seed = 0;
    }

    public function setSeed($seed): void
    {
        $this->seed = (int)$seed;
    }
}
