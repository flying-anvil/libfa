<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Test\Wrapper;

use FlyingAnvil\Libfa\Wrapper\File;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Libfa\Wrapper\File
 */
class FileTest extends TestCase
{
    private $memoryStream;

    protected function setUp(): void
    {
        $this->memoryStream = File::loadMemory();
        $this->memoryStream->open('wb');
    }

    public function testWriteSignedBytePositive(): void
    {
        $data = 123;
        $this->memoryStream->writeSignedByte($data);
        $this->memoryStream->rewind();
        $data = $this->memoryStream->read(10);
        self::assertSame(1, strlen($data));
        self::assertSame("\x7B", $data);
        $this->memoryStream->rewind();
    }

    public function testWriteSignedByteNegative(): void
    {
        $negativeData = -10;
        $this->memoryStream->writeSignedByte($negativeData);
        $this->memoryStream->rewind();
        $negativeContent = $this->memoryStream->read(10);
        self::assertSame(1, strlen($negativeContent));
        self::assertSame("\xF6", $negativeContent);
        $this->memoryStream->rewind();
    }

    public function testReadSignedByte(): void
    {
        $this->memoryStream->write("\xF6");
        $this->memoryStream->rewind();
        $read = $this->memoryStream->readSignedByte();

        self::assertSame(-10, $read);
    }

    public function testReadUnsignedByte(): void
    {
        $this->memoryStream->write("\xF6");
        $this->memoryStream->rewind();
        $read = $this->memoryStream->readUnsignedByte();

        self::assertSame(0xF6, $read);
    }
}
