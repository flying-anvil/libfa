<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Test\DataObject;

use FlyingAnvil\Libfa\DataObject\Exception\InvalidExtensionException;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Libfa\DataObject\FileExtension
 */
class FileExtensionTest extends TestCase
{
    public function testCanCreateFromExtension(): void
    {
        $rawExtension = 'exe';
        $extension = FileExtension::createFromString($rawExtension);

        self::assertSame($rawExtension, $extension->getExtension());
    }

    public function testCanCreateFromExtensionWithLeadingDot(): void
    {
        $extension = FileExtension::createFromString('.lib');

        self::assertSame('lib', $extension->getExtension());
    }

    public function testCanExtractExtensionFromPath(): void
    {
        $path = 'path/.to/file.txt';

        $extension = FileExtension::createFromFilePath($path);
        self::assertSame('txt', $extension->getExtension());
    }

    public function testCanCastToString(): void
    {
        $rawExtension = 'asm';
        $extension = FileExtension::createFromString($rawExtension);

        self::assertSame($rawExtension, (string)$extension);
    }

    public function testCanEncodeToJson(): void
    {
        $rawExtension = 'asm';
        $extension = FileExtension::createFromString($rawExtension);

        self::assertSame('"' . $rawExtension . '"', json_encode($extension));
    }

    public function testCannotCreateWithInvalidCharacters(): void
    {
        $this->expectException(InvalidExtensionException::class);
        $this->expectExceptionMessage('extension \'s p a c e s\' contains invalid characters');

        FileExtension::createFromString('s p a c e s');
    }
}
