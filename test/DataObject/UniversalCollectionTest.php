<?php

declare(strict_types=1);

namespace FlyingAnvil\Libfa\Test\DataObject;

use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Libfa\DataObject\UniversalCollection
 */
class UniversalCollectionTest extends TestCase
{
    public function testDummy(): void
    {
        $collection = UniversalCollection::createFrom([1, 2, 3]);

        self::assertCount(3, $collection);

        $collection->add('key', 5);

        self::assertCount(4, $collection);
    }

    public function testCanCreateAndConvertRecursively(): void
    {
        $in = [
            'root' => [
                'nested' => [
                    'deeply' => 'disturbed',
                ],
            ],
        ];

        $collection = UniversalCollection::createFromRecursive($in);
        $out = $collection->toArrayRecursive();

        self::assertSame($in, $out);
    }
}
